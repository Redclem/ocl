#include "graphics.h"


namespace Graphics
{

Window::Window() : m_window(nullptr)
{
}


Window::~Window()
{
    if(m_window)
    {
        SDL_DestroyWindow(m_window);
        m_window = nullptr;
    }
}

int Window::create_window(const char* name, int width, int height)
{
    m_window = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN);

    if(!m_window)
    {
        log_message("Error SDL_CreateWindow :", SDL_GetError());
        return -1;
    }

    return 0;
}

void Window::wait_quit()
{
    SDL_Event evnt;

    do
    {
        SDL_WaitEvent(&evnt);
    }
    while(evnt.type != SDL_QUIT);
}

Surface::Surface() : m_surface(nullptr)
{

}

Surface::~Surface()
{
    if(m_surface)
    {
        SDL_FreeSurface(m_surface);
        m_surface = nullptr;
    }
}

int Surface::create(int w, int h, int depth, Uint32 format)
{
    if(m_surface)
        SDL_FreeSurface(m_surface);

    m_surface = SDL_CreateRGBSurfaceWithFormat(0, w, h, depth, format);
    return (m_surface == nullptr ? -1 : 0);
}

int Surface::create(const char* path)
{
    int w, h;
    std::unique_ptr<stbi_uc> img_dat(stbi_load(path, &w, &h, nullptr, 4));

    if(img_dat == nullptr)
    {
        SDL_SetError("File %s not found", path);
        return -1;
    }

    CHECK_SDL(create(w, h, 32, SDL_PIXELFORMAT_RGBA32))

    if(SDL_MUSTLOCK(m_surface))
        CHECK_SDL(SDL_LockSurface(m_surface))

    uint8_t * dst_data = reinterpret_cast<uint8_t*>(m_surface->pixels);

    for(int line(0); line != h;line++)
        for(int col(0);col != w * 4; col++)
        {
            dst_data[line * m_surface->pitch + col] = img_dat.get()[line * w * 4 + col];
        }

    if(SDL_MUSTLOCK(m_surface))
        SDL_UnlockSurface(m_surface);

    return 0;
}

int Surface::create(const char* text, const stbtt_fontinfo* font, float scale, SDL_Color color)
{
    #define SCALE(x) static_cast<int>(static_cast<float>(x) * scale)

    if(text == nullptr)
    {
        CHECK_SDL(create(0, 0, 32, SDL_PIXELFORMAT_RGBA32))
        return 0;
    }

    int scaled_asc;
    int pen_pos(0);
    {
        int asc, dsc;
        stbtt_GetFontVMetrics(font, &asc, &dsc, nullptr);

        scaled_asc = SCALE(asc);

        int width(0);

        int bearing;

        stbtt_GetCodepointHMetrics(font, *text, nullptr, &bearing);


        if(bearing < 0)
        {
            pen_pos = -SCALE(bearing);
            width = pen_pos;
        }

        const char * cpy = text;

        while(*cpy)
        {
            int adv;
            stbtt_GetCodepointHMetrics(font, *cpy, &adv, nullptr);

            width += SCALE(adv);

            cpy++;
        }

        CHECK_SDL(create(width, SCALE(asc - dsc), 8, SDL_PIXELFORMAT_INDEX8))

        for(size_t clr_index(0); clr_index != 256; clr_index++)
        {
            m_surface->format->palette->colors[clr_index].r = color.r;
            m_surface->format->palette->colors[clr_index].g = color.g;
            m_surface->format->palette->colors[clr_index].b = color.b;
            m_surface->format->palette->colors[clr_index].a = uint8_t(clr_index);
        }
    }

    while(*text)
    {
        int y, x;

        stbtt_GetCodepointBitmapBox(font, *text, scale, scale, &x, &y, nullptr, nullptr);

        stbtt_MakeCodepointBitmap(font, reinterpret_cast<uint8_t*>(m_surface->pixels) + m_surface->pitch * (y + scaled_asc) + pen_pos + x, m_surface->w - pen_pos - x, m_surface->h - y - scaled_asc, m_surface->pitch, scale, scale, *text);

        int adv;
        stbtt_GetCodepointHMetrics(font, *text, &adv, nullptr);

        pen_pos += SCALE(adv);

        text++;
    }
    return 0;
    #undef SCALE
}

void Surface::measure(const char* text, const stbtt_fontinfo* font, float scale, int& w, int& h)
{
    #define SCALE(x) static_cast<int>(static_cast<float>(x) * scale)

    if(text == nullptr)
        return;

    {
        int asc, dsc;
        stbtt_GetFontVMetrics(font, &asc, &dsc, nullptr);

        h = SCALE(asc - dsc);

        int bearing;

        stbtt_GetCodepointHMetrics(font, *text, nullptr, &bearing);

        w = (bearing < 0 ? -SCALE(bearing) : 0);

        while(*text)
        {
            int adv;
            stbtt_GetCodepointHMetrics(font, *text, &adv, nullptr);

            w += SCALE(adv);

            text++;
        }
    }

    #undef SCALE
}


bool point_in_rect(const SDL_Point& point, const SDL_Rect& rect)
{
    return ((point.x >= rect.x) && (point.x < (rect.x + rect.w))) && ((point.y >= rect.y) && (point.y < (rect.y + rect.h)));
}

Surface::Surface(Surface&& from) : INIT_EXCHANGE(m_surface, nullptr)
{
}

Surface& Surface::operator=(Surface&& rhs)
{
    CHECK_SELF_ASSIGN;

    ASSIGN_SWAP(m_surface);
    return *this;
}

int Window::toggle_fullscreen()
{
    if(SDL_GetWindowFlags(m_window) & SDL_WINDOW_FULLSCREEN_DESKTOP)
    {
        CHECK_SDL(SDL_SetWindowFullscreen(m_window, 0))
        SDL_SetWindowPosition(m_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    }
    else
    {
        CHECK_SDL(SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN_DESKTOP))
    }

    return 0;
}

int Window::set_resolution(int w, int h)
{
    if(SDL_GetWindowFlags(m_window) & SDL_WINDOW_FULLSCREEN_DESKTOP)
        CHECK_SDL(SDL_SetWindowFullscreen(m_window, 0))
    SDL_SetWindowSize(m_window, w, h);
    SDL_SetWindowPosition(m_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    return 0;
}

Window::Window(Window&& from) : INIT_EXCHANGE(m_window, nullptr)
{

}

Window & Window::operator=(Window && rhs)
{
    CHECK_SELF_ASSIGN;

    ASSIGN_SWAP(m_window);
    return *this;
}

}
