#include "VulkanClasses.h"

namespace Vulkan
{

GraphicObject::GraphicObject(GraphicObject && from) : INIT_EXCHANGE(m_base, nullptr)
{
}

GraphicObject& GraphicObject::operator=(GraphicObject&& rhs)
{
    CHECK_SELF_ASSIGN;

    ASSIGN_SWAP(m_base);
    return *this;
}

GraphicObject::GraphicObject(VulkanBase* base) : m_base(base)
{

}

GraphicObject::~GraphicObject()
{
    cleanup();
}

MemoryObject::MemoryObject(VulkanBase* base) : GraphicObject::GraphicObject(base), INIT_VK_DEF(m_memory)
{

}

void MemoryObject::cleanup()
{
    if(m_memory)
    {
        vkFreeMemory(get_device(), m_memory, nullptr);
        ASSIGN_VK_DEF(m_memory);
    }

    GraphicObject::cleanup();
}

MemoryObject::MemoryObject(MemoryObject&& from) : GraphicObject::GraphicObject(std::move(from)), INIT_VK_EXCH(m_memory)
{

}

MemoryObject & MemoryObject::operator=(MemoryObject && rhs)
{
    GraphicObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_memory);
    return *this;
}

VkResult MemoryObject::allocate_memory(VkMemoryPropertyFlags props)
{
    VkMemoryRequirements mem_req = get_memory_requirements();

    VkMemoryAllocateInfo alloc_info = {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        nullptr,
        mem_req.size,
        get_base()->choose_memory_type(mem_req, props)
    };

    CHECK_VULKAN(vkAllocateMemory(get_device(), &alloc_info, nullptr, &m_memory))

    CHECK_VULKAN(bind_memory())

    return VK_SUCCESS;
}

Buffer::Buffer(VulkanBase* base) : MemoryObject::MemoryObject(base), INIT_VK_DEF(m_buffer)
{

}

void Buffer::cleanup()
{
    if(m_buffer)
    {
        vkDestroyBuffer(get_device(), m_buffer, nullptr);
        ASSIGN_VK_DEF(m_buffer);
    }
    MemoryObject::cleanup();
}

Buffer::Buffer(Buffer&& from) : MemoryObject::MemoryObject(std::move(from)), INIT_VK_EXCH(m_buffer)
{

}

Buffer & Buffer::operator=(Buffer && rhs)
{
    MemoryObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_buffer);
    return *this;
}

VkResult Buffer::create(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memory_properties)
{
    VkBufferCreateInfo buffer_info = {};
    buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_info.size = size;
    buffer_info.usage = usage;
    buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    CHECK_VULKAN(vkCreateBuffer(get_device(), &buffer_info, nullptr, &m_buffer))

    CHECK_VULKAN(allocate_memory(memory_properties))

    return VK_SUCCESS;
}

VkMemoryRequirements Buffer::get_memory_requirements() const
{
    VkMemoryRequirements mem_req;
    vkGetBufferMemoryRequirements(get_device(), m_buffer, &mem_req);
    return mem_req;
}

VkResult Buffer::bind_memory()
{
    CHECK_VULKAN(vkBindBufferMemory(get_device(), m_buffer, get_memory(), 0))
    return VK_SUCCESS;
}

VkResult MemoryObject::map_memory(void*& data)
{
    CHECK_VULKAN(vkMapMemory(get_device(), m_memory, 0, VK_WHOLE_SIZE, 0, &data))
    return VK_SUCCESS;
}

VkResult MemoryObject::flush_and_unmap_memory()
{
    VkMappedMemoryRange range = {
        VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        nullptr,
        m_memory,
        0,
        VK_WHOLE_SIZE
    };
    CHECK_VULKAN(vkFlushMappedMemoryRanges(get_device(), 1, &range))
    vkUnmapMemory(get_device(), m_memory);
    return VK_SUCCESS;
}

CommandBuffer::CommandBuffer(VulkanBase* base) : GraphicObject::GraphicObject(base), INIT_VK_DEF(m_command_buffer), INIT_VK_DEF(m_command_pool)
{

}

void CommandBuffer::cleanup()
{
    if(m_command_buffer)
    {
        vkFreeCommandBuffers(get_device(), m_command_pool, 1, &m_command_buffer);
        ASSIGN_VK_DEF(m_command_buffer);
        ASSIGN_VK_DEF(m_command_pool);
    }
}

CommandBuffer::CommandBuffer(CommandBuffer&& from) : GraphicObject::GraphicObject(std::move(from)), INIT_VK_EXCH(m_command_buffer), INIT_VK_DEF(m_command_pool)
{

}

CommandBuffer & CommandBuffer::operator=(CommandBuffer && rhs)
{
    GraphicObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_command_buffer);
    ASSIGN_SWAP(m_command_pool);
    return *this;
}

VkResult CommandBuffer::allocate(VkCommandBufferLevel level, VkCommandPool pool)
{
    VkCommandBufferAllocateInfo alloc_info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        nullptr,
        pool,
        level,
        1
    };
    m_command_pool = pool;

    CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_command_buffer))
    return VK_SUCCESS;
}

VkResult CommandBuffer::begin(VkCommandBufferUsageFlags usage)
{
    VkCommandBufferBeginInfo begin_info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        usage,
        nullptr
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_command_buffer, &begin_info))

    return VK_SUCCESS;
}

VkResult CommandBuffer::end()
{
    CHECK_VULKAN(vkEndCommandBuffer(m_command_buffer));

    return VK_SUCCESS;
}

Fence::Fence(VulkanBase* base) : GraphicObject::GraphicObject(base), INIT_VK_DEF(m_fence)
{

}

Fence::Fence(Fence && from) : GraphicObject::GraphicObject(std::move(from)), INIT_VK_EXCH(m_fence)
{

}

Fence & Fence::operator=(Fence && rhs)
{
    GraphicObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_fence);
    return *this;
}

void Fence::cleanup()
{
    if(m_fence)
    {
        vkDestroyFence(get_device(), m_fence, nullptr);
        ASSIGN_VK_DEF(m_fence);
    }
}

VkResult Fence::create()
{
    VkFenceCreateInfo fence_info = {
        VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        nullptr,
        0
    };

    CHECK_VULKAN(vkCreateFence(get_device(), &fence_info, nullptr, &m_fence))

    return VK_SUCCESS;
}

VkResult Fence::wait()
{
    CHECK_VULKAN(vkWaitForFences(get_device(), 1, &m_fence, VK_TRUE, uint64_t(~0)))
    return VK_SUCCESS;
}

void MemoryObject::add_memory_requirements(VkMemoryRequirements& req0, const VkMemoryRequirements& req1)
{
    if(req1.alignment > req0.alignment)
        req0.alignment = req1.alignment;

    VkDeviceSize rem(req0.size % req1.alignment);

    if(rem)
    {
        req0.size += req1.alignment - rem;
    }

    req0.size += req1.size;

    req0.memoryTypeBits &= req1.memoryTypeBits;
}

void MemoryObject::offset_match_alignment(VkDeviceSize& offset, VkDeviceSize alignment)
{
    VkDeviceSize rem(offset % alignment);
    if(rem)
        offset += alignment - rem;
}


}
