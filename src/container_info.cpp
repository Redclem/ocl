#include "container_info.h"

ContainerInfo::ContainerInfo() : m_vertices(), m_outside_indexes(), m_inside_indexes(), m_volumes(), m_height(0.0f), m_bottom(0.0f), m_top(0.0f), m_maximums{0.0f, 0.0f}, m_minimums{0.0f, 0.0f}
{

}

ContainerInfo::~ContainerInfo()
{

}

bool ContainerInfo::load(std::istream& stream)
{
    std::string keyword;
    float scale_x(1.0f), scale_y(1.0f);
    Custom::Vector<index_t> outline;
    while(stream >> keyword && keyword != "end")
    {
        if(keyword == "scale")
        {
            if(m_vertices.get_size())
            {
                log_message("error : scale declaration after vertex loading");
                return false;
            }

            stream >> scale_x >> scale_y;
        }

        else if(keyword == "vertices")
        {
            /** Check if vertices have already been loaded */

            if(m_vertices.get_size())
            {
                log_message("error : multiple vertex declaration");
                return false;
            }

            /** Load vertices */
            size_t vertex_count(0);
            stream >> vertex_count;
            if(vertex_count == 0)
            {
                log_message("error : failure to read vertex count or vertex count is 0 (prohibited)");
                return false;
            }

            m_vertices.resize(vertex_count);

            {
                vertex_t & vertex(m_vertices[0]);
                stream >> vertex.x >> vertex.y;
                vertex.x *= scale_x;
                vertex.y *= scale_y;

                m_maximums = {vertex.x, vertex.y};
                m_minimums = {vertex.x, vertex.y};
            }

            for(size_t vertex_index(1); vertex_index != vertex_count; vertex_index++)
            {
                vertex_t & vertex(m_vertices[vertex_index]);
                stream >> vertex.x >> vertex.y;
                vertex.x *= scale_x;
                vertex.y *= scale_y;

                m_maximums.x = (vertex.x > m_maximums.x ? vertex.x : m_maximums.x);
                m_maximums.y = (vertex.y > m_maximums.y ? vertex.y : m_maximums.y);

                m_minimums.x = (vertex.x < m_minimums.x ? vertex.x : m_minimums.x);
                m_minimums.y = (vertex.y < m_minimums.y ? vertex.y : m_minimums.y);
            }
        }

        else if(keyword == "indexes")
        {
            std::string side;
            stream >> side;

            Custom::Vector<index_t> * index_vector_ptr;

            if (side == "outside")
                index_vector_ptr = &m_outside_indexes;
            else if(side == "inside")
                index_vector_ptr = &m_inside_indexes;
            else
            {
                log_message("Unknown index side :", side);
                return false;
            }

            if(index_vector_ptr->get_size())
            {
                log_message("error : multiple index declaration for side", side);
                return false;
            }

            size_t index_count(0);
            stream >> index_count;
            if(index_count == 0)
            {
                log_message("error : failed to read index count or index count is 0 (prohibited)");
                return false;
            }

            index_vector_ptr->resize(index_count);

            for(size_t index_index(0); index_index != index_count; index_index++)
            {
                std::string index_str;
                stream >> index_str;
                index_vector_ptr->at(index_index) = (index_str == "PR" ? primitive_restart : index_t(strtoul(index_str.c_str(), nullptr, 10)));
            }
        }

        else if(keyword == "autogen")
        {

            std::string target;
            stream >> target;
            if(target == "indexes")
            {
                if(m_inside_indexes.get_size() || m_outside_indexes.get_size())
                {
                    log_message("error : demanding autogen for inside when indexes have been loaded or generated");
                    return false;
                }
                float width(0.0f);
                stream >> width;
                if(width <= 0.0f)
                {
                    log_message("error : unable to read width or width is 0 or negative (prohibited)");
                    return false;
                }
                autogen_inside(outline, width);
            }
            else
            {
                log_message("error : unknown autogen target :", target);
                return false;
            }
        }

        else if(keyword == "volumes")
        {
            if(m_volumes.get_size())
            {
                log_message("error : multiple volume declaration");
                return false;
            }

            size_t volume_count;
            stream >> volume_count;

            if(volume_count == 0)
            {
                log_message("error : unable to read volume count or volume count is 0 (prohibited)");
                return false;
            }

            m_volumes.resize(volume_count);

            for(VolumePtr & volume : m_volumes)
            {
                height_t height;
                stream >> height;

                if(height <= 0.0f)
                {
                    log_message("error : unable to read height or height is negative or 0 (prohibited)");
                    return false;
                }

                m_height += height;

                std::string volume_type;
                stream >> volume_type;

                if(volume_type == "linear")
                {
                    surface_t section;
                    stream >> section;

                    if(section <= 0.0f)
                    {
                        log_message("error : unable to read section or section is negative or 0 (prohibited)");
                        return false;
                    }

                    volume = new Linear(height, section);
                }
                else
                {
                    log_message("error : unknown volume type :", volume_type);
                    return false;
                }
            }
        }

        else if(keyword == "outline")
        {
            size_t index_count;
            stream >> index_count;
            if(index_count == 0)
            {
                log_message("error : cannot read index count or index count is 0 (prohibited)");
                return false;
            }

            if(outline.get_size())
            {
                log_message("error : multiple outline declaration");
                return false;
            }

            outline.resize(index_count);

            for(size_t index(0); index != index_count; index++)
            {
                std::string index_str;
                stream >> index_str;
                outline[index] = (index_str == "PR" ? primitive_restart : index_t(strtoul(index_str.c_str(), nullptr, 10)));
            }
        }

        else
        {
            log_message("error : unknown keyword :", keyword);
            return false;
        }

    }
    if(!stream)
    {
        log_message("error : unexpected failure to read from stream");
        return false;
    }

    if(m_outside_indexes.get_size())
        compute_extremums();

    return true;
}

ContainerInfo::ContainerInfo(ContainerInfo&& from) : INIT_MOVE(m_vertices), INIT_MOVE(m_outside_indexes), INIT_MOVE(m_inside_indexes), INIT_MOVE(m_volumes), INIT_COPY(m_height), INIT_COPY(m_bottom), INIT_COPY(m_top),
    INIT_COPY(m_maximums), INIT_COPY(m_minimums)
{

}

ContainerInfo::ContainerInfo(const ContainerInfo& from) : INIT_COPY(m_vertices), INIT_COPY(m_outside_indexes), INIT_COPY(m_inside_indexes), INIT_COPY(m_volumes), INIT_COPY(m_height), INIT_COPY(m_bottom), INIT_COPY(m_top),
    INIT_COPY(m_maximums), INIT_COPY(m_minimums)
{
}

ContainerInfo& ContainerInfo::operator=(ContainerInfo&& rhs)
{
    CHECK_SELF_ASSIGN;
    ASSIGN_MOVE(m_vertices);
    ASSIGN_MOVE(m_outside_indexes);
    ASSIGN_MOVE(m_inside_indexes);
    ASSIGN_MOVE(m_volumes);
    ASSIGN_COPY(m_height);
    ASSIGN_COPY(m_bottom);
    ASSIGN_COPY(m_top);
    ASSIGN_COPY(m_maximums);
    ASSIGN_COPY(m_minimums);
    return *this;
}

ContainerInfo& ContainerInfo::operator=(const ContainerInfo& rhs)
{
    CHECK_SELF_ASSIGN;
    ASSIGN_COPY(m_vertices);
    ASSIGN_COPY(m_outside_indexes);
    ASSIGN_COPY(m_inside_indexes);
    ASSIGN_COPY(m_volumes);
    ASSIGN_COPY(m_height);
    ASSIGN_COPY(m_bottom);
    ASSIGN_COPY(m_top);
    ASSIGN_COPY(m_maximums);
    ASSIGN_COPY(m_minimums);
    return *this;
}

void ContainerInfo::clear()
{
    m_vertices.clear();
    m_outside_indexes.clear();
    m_inside_indexes.clear();
    m_volumes.clear();
    m_height = 0.0f;
    m_bottom = 0.0f;
    m_top = 0.0f;
}

void ContainerInfo::autogen_inside(const Custom::Vector<index_t> & outline, float width)
{
    /** Compute normals */

    Custom::Vector<Vec::Vec> vertices_normals(m_vertices.get_size());

    std::map<index_t, index_t> outside_inside_map;

    for(size_t index(1); index != outline.get_size(); index++)
    {
        if(outline[index - 1] == primitive_restart)
            continue;

        outside_inside_map.insert({outline[index - 1], outside_inside_map.size()});

        if(outline[index] == primitive_restart)
            continue;

        vertex_t & first_vert = m_vertices[outline[index - 1]];
        vertex_t & second_vert = m_vertices[outline[index]];

        Vec::Vec segment_normal(second_vert.y - first_vert.y, first_vert.x - second_vert.x);
        segment_normal.normalize();

        vertices_normals[outline[index - 1]] = (vertices_normals[outline[index - 1]] + segment_normal) / (1 + vertices_normals[outline[index - 1]].dot(segment_normal));
        vertices_normals[outline[index]] = (vertices_normals[outline[index]] + segment_normal) / (1 + vertices_normals[outline[index]].dot(segment_normal));
    }

    outside_inside_map.insert({outline.back(), outside_inside_map.size()});

    /** Normalize and scale normals by width */

    for(Vec::Vec & normal : vertices_normals)
        normal *= width;

    /** Generate new vertices */

    size_t generated_vertices_offset(m_vertices.get_size());

    m_vertices.resize(m_vertices.get_size() + outside_inside_map.size());

    for(auto & outside_inside : outside_inside_map)
    {
        m_vertices[outside_inside.second + generated_vertices_offset].x = m_vertices[outside_inside.first].x + vertices_normals[outside_inside.first].x;
        m_vertices[outside_inside.second + generated_vertices_offset].y = m_vertices[outside_inside.first].y + vertices_normals[outside_inside.first].y;
    }

    /** Generate outside indexes */

    m_outside_indexes.resize(outline.get_size() * 2);

    for(size_t idx(0); idx != outline.get_size(); idx++)
    {
        m_outside_indexes[idx * 2] = outline[idx];
        m_outside_indexes[idx * 2 + 1] = outside_inside_map.at(outline[idx]) + generated_vertices_offset;
    }

    /** Generate inside indexes */

    m_inside_indexes.resize(outline.get_size());

    size_t beg(0), end(outline.get_size() - 1);

    for(size_t index(0); index != outline.get_size(); index++)
    {
        index_t outside_index;
        if(beg - end % 2)
        {
            outside_index = outline[beg];
            beg++;
        }
        else
        {
            outside_index = outline[end];
            end--;
        }

        m_inside_indexes[index] = outside_inside_map[outside_index] + generated_vertices_offset;
    }
}

void ContainerInfo::display(std::ostream& stream) const
{
    if(m_vertices.get_size())
    {
        stream << "vertices " << m_vertices.get_size() << '\n';
        for(const vertex_t & vert : m_vertices)
            stream << vert.x << ' ' << vert.y << '\n';
    }

    if(m_outside_indexes.get_size())
    {
        stream << "indexes outside " << m_outside_indexes.get_size() << '\n';
        for(index_t index : m_outside_indexes)
            stream << index << ' ';
        stream << '\n';
    }

    if(m_inside_indexes.get_size())
    {
        stream << "indexes inside " << m_inside_indexes.get_size() << '\n';
        for(index_t index : m_inside_indexes)
            stream << index << ' ';
        stream << '\n';
    }

    if(m_volumes.get_size())
    {
        stream << "volumes " << m_volumes.get_size() << '\n';

        for(const VolumePtr & volume : m_volumes)
        {
            volume->display(stream);
            stream << '\n';
        }
    }

    stream << "end\n";
}

void ContainerInfo::compute_extremums()
{
    m_bottom = m_vertices[m_inside_indexes.front()].y;
    m_top = m_vertices[m_inside_indexes.front()].y;

    for(size_t index(1); index != m_inside_indexes.get_size(); index++)
    {
        m_bottom = m_bottom < m_vertices[m_inside_indexes[index]].y ? m_vertices[m_inside_indexes[index]].y : m_bottom;
        m_top = m_top > m_vertices[m_inside_indexes[index]].y ? m_vertices[m_outside_indexes[index]].y : m_top;
    }
}

volume_t ContainerInfo::get_capacity() const
{
    volume_t cap(0.0f);
    for(auto & vol : m_volumes)
        cap += vol->get_capacity();
    return cap;
}

height_t ContainerInfo::get_fluid_level(volume_t volume) const
{
    height_t hgt(0.0f);

    for(const VolumePtr & vol : m_volumes)
    {
        if(volume >= vol->get_capacity())
        {
            volume -= vol->get_capacity();
            hgt += vol->get_height();
        }
        else
            return hgt + vol->get_fluid_level(volume);
    }

    return hgt;
}

