#include "VulkanBase.h"


VulkanBase::VulkanBase() : m_window(nullptr), INIT_VK_DEF(m_instance),
#ifdef DEBUG
    INIT_VK_DEF(m_debug_utils_messenger),
#endif // DEBUG
    INIT_VK_DEF(m_physical_device), INIT_VK_DEF(m_surface), INIT_VK_DEF(m_device), INIT_VK_DEF(m_graphics_queue), INIT_VK_DEF(m_present_queue), INIT_VK_DEF(m_swapchain), m_swapchain_extent {0, 0}, m_swapchain_image_views(),
    m_framebuffers(), INIT_VK_DEF(m_render_pass), INIT_VK_DEF(m_pipeline_cache), m_swapchain_format(VK_FORMAT_UNDEFINED), m_aspect_ratio(1.0f)
{
    if(!SDL_WasInit(SDL_INIT_VIDEO))
    {
        if(SDL_Init(SDL_INIT_VIDEO))
        {
            log_message("Error init SDL :", SDL_GetError());
            throw std::runtime_error("Erreur init SDL\n");
        }

        atexit(SDL_Quit);
    }

    if(SDL_Vulkan_LoadLibrary(nullptr))
    {
        log_message("Erreur SDL_Vulkan_LoadLibrary :", SDL_GetError());
        throw std::runtime_error("Erreur SDL_Vulkan_LoadLibrary\n");
    }

    atexit(SDL_Vulkan_UnloadLibrary);
}

VulkanBase::~VulkanBase()
{
    cleanup();
}

void VulkanBase::cleanup()
{
    cleanup_swapchain_ressources();

    if(m_pipeline_cache)
    {
        save_pipeline_cache();
        vkDestroyPipelineCache(m_device, m_pipeline_cache, nullptr);
        ASSIGN_VK_DEF(m_pipeline_cache);
    }

    if(m_swapchain)
    {
        vkDestroySwapchainKHR(m_device, m_swapchain, nullptr);
        ASSIGN_VK_DEF(m_swapchain);
    }
    if(m_device)
    {
        vkDestroyDevice(m_device, nullptr);
        ASSIGN_VK_DEF(m_device);
        ASSIGN_VK_DEF(m_graphics_queue);
        ASSIGN_VK_DEF(m_present_queue);
    }
    if(m_surface)
    {
        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
        ASSIGN_VK_DEF(m_surface);
    }
#ifdef DEBUG
    if(m_debug_utils_messenger)
    {
        PFN_vkDestroyDebugUtilsMessengerEXT destroyer = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(m_instance, "vkDestroyDebugUtilsMessengerEXT"));

        if(destroyer)
        {
            destroyer(m_instance, m_debug_utils_messenger, nullptr);
            ASSIGN_VK_DEF(m_debug_utils_messenger);
        }
    }
#endif // DEBUG

    if(m_instance)
    {
        vkDestroyInstance(m_instance, nullptr);
        ASSIGN_VK_DEF(m_instance);
    }
}

VulkanBase::VulkanBase(VulkanBase&& from) : INIT_COPY(m_window), INIT_VK_EXCH(m_instance),
#ifdef DEBUG
    INIT_VK_EXCH(m_debug_utils_messenger),
#endif // DEBUG
    INIT_COPY(m_physical_device), INIT_VK_EXCH(m_surface), INIT_VK_EXCH(m_device), INIT_COPY(m_graphics_queue), INIT_COPY(m_present_queue), INIT_VK_EXCH(m_swapchain), INIT_COPY(m_swapchain_extent),
    INIT_MOVE(m_swapchain_image_views), INIT_MOVE(m_framebuffers), INIT_VK_EXCH(m_render_pass), INIT_VK_EXCH(m_pipeline_cache), INIT_COPY(m_swapchain_format), INIT_COPY(m_aspect_ratio)
{

}

VulkanBase& VulkanBase::operator=(VulkanBase&& rhs)
{
    CHECK_SELF_ASSIGN;

    ASSIGN_COPY(m_window);
    ASSIGN_SWAP(m_instance);
#ifdef DEBUG
    ASSIGN_SWAP(m_debug_utils_messenger);
#endif // DEBUG
    ASSIGN_SWAP(m_physical_device);
    ASSIGN_SWAP(m_surface);
    ASSIGN_SWAP(m_device);
    ASSIGN_COPY(m_graphics_queue);
    ASSIGN_COPY(m_present_queue);
    ASSIGN_SWAP(m_swapchain);
    ASSIGN_COPY(m_swapchain_extent);
    ASSIGN_MOVE(m_swapchain_image_views);
    ASSIGN_COPY(m_swapchain_format);
    ASSIGN_MOVE(m_framebuffers);
    ASSIGN_SWAP(m_render_pass);
    ASSIGN_SWAP(m_pipeline_cache);
    ASSIGN_COPY(m_aspect_ratio);
    return *this;
}


VkResult VulkanBase::init()
{
    CHECK_VULKAN(create_instance())

#ifdef DEBUG
    CHECK_VULKAN(create_debug_utils_messenger())
#endif // DEBUG

    if(!create_surface())
    {
        log_message("Erreur create_surface() :", SDL_GetError());
        return VK_ERROR_UNKNOWN;
    }
    CHECK_VULKAN(choose_physical_device())
    CHECK_VULKAN(create_device())
    CHECK_VULKAN(create_swapchain())
    CHECK_VULKAN(create_render_pass())
    CHECK_VULKAN(create_framebuffers())
    CHECK_VULKAN(create_pipeline_cache())
    return VK_SUCCESS;
}

VkResult VulkanBase::create_instance()
{
    uint32_t layer_count;
    CHECK_VULKAN(vkEnumerateInstanceLayerProperties(&layer_count, nullptr))

    Custom::Vector<VkLayerProperties> props(layer_count);
    CHECK_VULKAN(vkEnumerateInstanceLayerProperties(&layer_count, props.get_data()))

    VkInstanceCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;

    VkApplicationInfo app_info;

    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;

    app_info.pApplicationName = "OCL";
    app_info.applicationVersion = Version::version;

    app_info.pEngineName = "OCL Graphics Engine";
    app_info.engineVersion = Version::version;

    CHECK_VULKAN(vkEnumerateInstanceVersion(&app_info.apiVersion))

    info.ppEnabledLayerNames = layers.data();
    info.enabledLayerCount = layers.size();

    SDL_Vulkan_GetInstanceExtensions(m_window, &info.enabledExtensionCount, nullptr);

    Custom::Vector<const char *> extensions_vec(info.enabledExtensionCount + instance_extensions.size());
    SDL_Vulkan_GetInstanceExtensions(m_window, &info.enabledExtensionCount, extensions_vec.get_data());

    info.ppEnabledExtensionNames = extensions_vec.get_data();

    for(uint32_t extra_exten(0); extra_exten != instance_extensions.size(); extra_exten++)
    {
        extensions_vec[info.enabledExtensionCount + extra_exten] = instance_extensions[extra_exten];
    }

    info.enabledExtensionCount = uint32_t(extensions_vec.get_size());

#ifdef DEBUG

    VkDebugUtilsMessengerCreateInfoEXT msg_info;
    populate_debug_utils_messenger_info(msg_info);

    info.pNext = &msg_info;

#endif

    CHECK_VULKAN(vkCreateInstance(&info, nullptr, &m_instance))
    return VK_SUCCESS;
}

VkResult VulkanBase::choose_physical_device()
{
    /** Prefer discrete GPUs */

    uint32_t physical_device_count;
    CHECK_VULKAN(vkEnumeratePhysicalDevices(m_instance, &physical_device_count, nullptr))

    Custom::Vector<VkPhysicalDevice> devices(physical_device_count, DEF_VK(VkPhysicalDevice));
    CHECK_VULKAN(vkEnumeratePhysicalDevices(m_instance, &physical_device_count, devices.get_data()))

    for(VkPhysicalDevice device : devices)
    {
        VkPhysicalDeviceProperties cur_props;
        vkGetPhysicalDeviceProperties(device, &cur_props);

        bool suitable;
        CHECK_VULKAN(physical_device_suitable(device, suitable))

        if(cur_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && suitable)
        {
            m_physical_device = device;
            break;
        }
        else if(suitable && m_physical_device == VK_NULL_HANDLE)
        {
            m_physical_device = device;
        }
    }

    if(m_physical_device == DEF_VK(VkPhysicalDevice))
        return VK_ERROR_UNKNOWN;

    return VK_SUCCESS;
}

bool VulkanBase::create_surface()
{
    return SDL_Vulkan_CreateSurface(m_window, m_instance, &m_surface) == SDL_TRUE;
}

VkResult VulkanBase::create_device()
{
    uint32_t graphic_index;
    uint32_t present_index;

    CHECK_VULKAN(get_physical_device_indexes(m_physical_device, graphic_index, present_index))

    Custom::Vector<VkDeviceQueueCreateInfo> queue_infos(graphic_index != present_index ? 2 : 1);

    float prio(1.0f);

    queue_infos[0] =
    {
        VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        nullptr,
        0,
        graphic_index,
        1,
        &prio
    };

    if(queue_infos.get_size() != 1)
    {
        queue_infos[1] =
        {
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            nullptr,
            0,
            present_index,
            1,
            &prio
        };
    }

    VkDeviceCreateInfo device_info = {};
    device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_info.queueCreateInfoCount = uint32_t(queue_infos.get_size());
    device_info.pQueueCreateInfos = queue_infos.get_data();

    device_info.enabledExtensionCount = device_extensions.size();
    device_info.ppEnabledExtensionNames = device_extensions.data();

    CHECK_VULKAN(vkCreateDevice(m_physical_device, &device_info, nullptr, &m_device))

    vkGetDeviceQueue(m_device, graphic_index, 0, &m_graphics_queue);
    vkGetDeviceQueue(m_device, present_index, 0, &m_present_queue);

    return VK_SUCCESS;
}

VkResult VulkanBase::physical_device_suitable(VkPhysicalDevice physical_device, bool& suitable) const
{
    uint32_t graphic, present;

    CHECK_VULKAN(get_physical_device_indexes(physical_device, graphic, present))

    suitable = graphic != no_family && present != no_family;

    return VK_SUCCESS;
}

VkResult VulkanBase::get_physical_device_indexes(VkPhysicalDevice physical_device, uint32_t& graphic, uint32_t& present) const
{
    graphic = no_family;
    present = no_family;

    uint32_t family_count;
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &family_count, nullptr);

    Custom::Vector<VkQueueFamilyProperties> properties(family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &family_count, properties.get_data());

    for(uint32_t family(0); family != family_count; family++)
    {
        VkQueueFamilyProperties & cur_properties = properties[family];

        if((cur_properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) && graphic == no_family && cur_properties.queueCount)
            graphic = family;

        VkBool32 can_present;
        CHECK_VULKAN(vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, family, m_surface, &can_present))
        if(can_present && present == no_family)
            present = family;
    }

    return VK_SUCCESS;
}

VkResult VulkanBase::get_graphic_index(uint32_t& graphic) const
{
    uint32_t family_count;
    vkGetPhysicalDeviceQueueFamilyProperties(m_physical_device, &family_count, nullptr);

    Custom::Vector<VkQueueFamilyProperties> properties(family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(m_physical_device, &family_count, properties.get_data());

    for(uint32_t family(0); family != family_count; family++)
    {
        VkQueueFamilyProperties & cur_properties = properties[family];

        if((cur_properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) && cur_properties.queueCount)
        {
            graphic = family;
            return VK_SUCCESS;
        }
    }

    return VK_ERROR_UNKNOWN;
}


VkResult VulkanBase::create_swapchain()
{
    VkSwapchainCreateInfoKHR swap_info = {};
    swap_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swap_info.surface = m_surface;

    /** Get physical device surface capabilities */

    VkSurfaceCapabilitiesKHR surface_cap;

    CHECK_VULKAN(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physical_device, m_surface, &surface_cap))

    /** If surface nonexistent, abort */
    if(surface_cap.maxImageExtent.width == 0 || surface_cap.maxImageExtent.height == 0)
    {
        m_swapchain_extent = {0, 0};
        m_aspect_ratio = 1.0f;
        return VK_SUCCESS;
    }

    swap_info.minImageCount = surface_cap.minImageCount;

    /** Find format and color space */

    {
        VkSurfaceFormatKHR surface_format;
        CHECK_VULKAN(choose_surface_format(surface_format))

        swap_info.imageFormat = surface_format.format;
        swap_info.imageColorSpace = surface_format.colorSpace;

        m_swapchain_format = surface_format.format;
    }

    if(surface_cap.currentExtent.width != uint32_t(~0) && surface_cap.currentExtent.height != uint32_t(~0))
        swap_info.imageExtent = surface_cap.currentExtent;
    else
    {
        int window_width, window_height;

        SDL_GetWindowSize(m_window, &window_width, &window_height);



        swap_info.imageExtent.width = uint32_t(window_width);

        if(swap_info.imageExtent.width < surface_cap.minImageExtent.width)
            swap_info.imageExtent.width = surface_cap.minImageExtent.width;

        else if(swap_info.imageExtent.width > surface_cap.maxImageExtent.width)
            swap_info.imageExtent.width = surface_cap.maxImageExtent.width;

        swap_info.imageExtent.height = uint32_t(window_height);

        if(swap_info.imageExtent.height < surface_cap.minImageExtent.height)
            swap_info.imageExtent.height = surface_cap.minImageExtent.height;

        else if(swap_info.imageExtent.height > surface_cap.maxImageExtent.height)
            swap_info.imageExtent.height = surface_cap.maxImageExtent.height;
    }

    m_swapchain_extent = swap_info.imageExtent;
    m_aspect_ratio = float(m_swapchain_extent.width) / float(m_swapchain_extent.height);

    swap_info.imageArrayLayers = 1;
    swap_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swap_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

    swap_info.preTransform = surface_cap.currentTransform;
    swap_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    CHECK_VULKAN(choose_present_mode(swap_info.presentMode))

    swap_info.clipped = VK_TRUE;
    swap_info.oldSwapchain = m_swapchain;

    CHECK_VULKAN(vkCreateSwapchainKHR(m_device, &swap_info, nullptr, &m_swapchain))

    if(swap_info.oldSwapchain)
    {
        vkDestroySwapchainKHR(m_device, swap_info.oldSwapchain, nullptr);
        ASSIGN_VK_DEF(swap_info.oldSwapchain);
    }

    uint32_t image_count;
    CHECK_VULKAN(vkGetSwapchainImagesKHR(m_device, m_swapchain, &image_count, nullptr))

    Custom::Vector<VkImage> swapchain_images(image_count, DEF_VK(VkImage));
    CHECK_VULKAN(vkGetSwapchainImagesKHR(m_device, m_swapchain, &image_count, swapchain_images.get_data()))

    m_swapchain_image_views.assign(image_count, DEF_VK(VkImageView));
    for(uint32_t image(0); image != image_count; image++)
    {
        VkImageViewCreateInfo view_info = {};
        view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        view_info.image = swapchain_images[image];
        view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        view_info.format = swap_info.imageFormat;
        view_info.components =
        {
            VK_COMPONENT_SWIZZLE_R,
            VK_COMPONENT_SWIZZLE_G,
            VK_COMPONENT_SWIZZLE_B,
            VK_COMPONENT_SWIZZLE_A
        };
        view_info.subresourceRange =
        {
            VK_IMAGE_ASPECT_COLOR_BIT,
            0,
            1,
            0,
            1
        };

        CHECK_VULKAN(vkCreateImageView(m_device, &view_info, nullptr, m_swapchain_image_views.get_data() + image))
    }

    return VK_SUCCESS;
}

VkResult VulkanBase::choose_surface_format(VkSurfaceFormatKHR& format)
{
    /** Prefer RGB or BRG formats in sRGB colorspace */

    uint32_t format_count;

    CHECK_VULKAN(vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device, m_surface, &format_count, nullptr))

    Custom::Vector<VkSurfaceFormatKHR> formats(format_count);
    CHECK_VULKAN(vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device, m_surface, &format_count, formats.get_data()))

    for(VkSurfaceFormatKHR & cur_format : formats)
    {
        if((cur_format.format == VK_FORMAT_B8G8R8_SRGB || cur_format.format == VK_FORMAT_R8G8B8A8_SRGB)
                && cur_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            format = cur_format;
            return VK_SUCCESS;
        }
    }

    format = formats[0];
    return VK_SUCCESS;
}

VkResult VulkanBase::choose_present_mode(VkPresentModeKHR& present_mode)
{
    uint32_t present_mode_count;
    CHECK_VULKAN(vkGetPhysicalDeviceSurfacePresentModesKHR(m_physical_device, m_surface, &present_mode_count, nullptr))

    Custom::Vector<VkPresentModeKHR> present_modes(present_mode_count);
    CHECK_VULKAN(vkGetPhysicalDeviceSurfacePresentModesKHR(m_physical_device, m_surface, &present_mode_count, present_modes.get_data()))

    present_mode = VK_PRESENT_MODE_FIFO_KHR;

    for(VkPresentModeKHR current_present_mode : present_modes)
    {
        if(current_present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
            break;
        }
        else if(present_mode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
        {
            present_mode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
        }
    }

    return VK_SUCCESS;
}

uint32_t VulkanBase::choose_memory_type(const VkMemoryRequirements& requirements, VkMemoryPropertyFlags mem_props)
{
    VkPhysicalDeviceMemoryProperties phys_mem_props;
    vkGetPhysicalDeviceMemoryProperties(m_physical_device, &phys_mem_props);

    for(uint32_t type(0); type != phys_mem_props.memoryTypeCount; type++)
    {
        if(((requirements.memoryTypeBits) & (1 << type)) &&
                (mem_props & phys_mem_props.memoryTypes[type].propertyFlags) == mem_props)
            return type;
    }
    return no_type;
}

VkResult VulkanBase::recreate_swapchain()
{
    cleanup_swapchain_ressources();

    CHECK_VULKAN(create_swapchain())
    CHECK_VULKAN(create_render_pass())
    CHECK_VULKAN(create_framebuffers())

    return VK_SUCCESS;
}

void VulkanBase::cleanup_swapchain_ressources()
{
    if(!m_swapchain_image_views.empty())
    {
        for(VkImageView view : m_swapchain_image_views)
        {
            if(view)
                vkDestroyImageView(m_device, view, nullptr);
        }
    }

    m_swapchain_image_views.clear();

    if(!m_framebuffers.empty())
    {
        for(VkFramebuffer buffer : m_framebuffers)
        {
            if(buffer)
                vkDestroyFramebuffer(get_device(), buffer, nullptr);
        }
    }

    m_framebuffers.clear();

    if(m_render_pass)
    {
        vkDestroyRenderPass(m_device, m_render_pass, nullptr);
        ASSIGN_VK_DEF(m_render_pass);
    }
}

VkResult VulkanBase::create_render_pass()
{
    VkRenderPassCreateInfo render_pass_info = {};
    render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;


    VkAttachmentDescription attachment =
    {
        0,
        m_swapchain_format,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };
    render_pass_info.attachmentCount = 1;
    render_pass_info.pAttachments = &attachment;

    VkAttachmentReference attach =
    {
        0,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription subpass_descr =
    {
        0,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        0,
        nullptr,
        1,
        &attach,
        nullptr,
        nullptr,
        0,
        nullptr
    };

    render_pass_info.subpassCount = 1;
    render_pass_info.pSubpasses = &subpass_descr;

    CHECK_VULKAN(vkCreateRenderPass(m_device, &render_pass_info, nullptr, &m_render_pass))

    return VK_SUCCESS;
}


VkResult VulkanBase::create_framebuffers()
{
    m_framebuffers.assign(m_swapchain_image_views.get_size(), DEF_VK(VkFramebuffer));

    for(uint32_t framebuffer(0); framebuffer != m_framebuffers.get_size(); framebuffer++)
    {
        VkFramebufferCreateInfo frame_info = {};

        frame_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        frame_info.renderPass = m_render_pass;
        frame_info.attachmentCount = 1;
        frame_info.pAttachments = m_swapchain_image_views.get_data() + framebuffer;
        frame_info.width = m_swapchain_extent.width;
        frame_info.height = m_swapchain_extent.height;
        frame_info.layers = 1;

        CHECK_VULKAN(vkCreateFramebuffer(m_device, &frame_info, nullptr, m_framebuffers.get_data() + framebuffer))
    }

    return VK_SUCCESS;
}

#ifdef DEBUG

VkResult VulkanBase::create_debug_utils_messenger()
{
    PFN_vkCreateDebugUtilsMessengerEXT create_func = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(m_instance, "vkCreateDebugUtilsMessengerEXT"));

    if(create_func == nullptr)
    {
        log_message("Error vkGetInstanceProcAddr\n");
        return VK_ERROR_UNKNOWN;
    }

    VkDebugUtilsMessengerCreateInfoEXT msg_info;
    populate_debug_utils_messenger_info(msg_info);



    CHECK_VULKAN(create_func(m_instance, &msg_info, nullptr, &m_debug_utils_messenger))

    return VK_SUCCESS;
}

VkBool32 VulkanBase::debug_utils_messenger_callback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void*)
{
    std::cout << "Layer Message\n";
    std::cout << "Message Severity : ";

    switch(messageSeverity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        std::cout << "Verbose";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
        std::cout << "Info";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        std::cout << "Warning";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        std::cout << "Error";
        break;
    default:
        std::cout << "Unknown";
        break;
    }

    std::cout << "\nMessage Type : ";

    switch(messageTypes)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
        std::cout << "General";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
        std::cout << "Validation";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
        std::cout << "Performance";
        break;
    default:
        std::cout << "Unknown";
        break;
    }

    std::cout << '\n';
    std::cout << pCallbackData->pMessage;
    std::cout << "\n\n";

    return VK_FALSE;
}

void VulkanBase::populate_debug_utils_messenger_info(VkDebugUtilsMessengerCreateInfoEXT& info)
{
    info =
    {
        VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        nullptr,
        0,
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        debug_utils_messenger_callback,
        nullptr
    };
}


#endif // DEBUG

VkResult VulkanBase::create_pipeline_cache()
{
    VkPipelineCacheCreateInfo cache_info = {};
    cache_info.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

    Custom::Vector<char> m_data;
    std::ifstream cache_file(cache_location, std::ios::ate | std::ios::binary);
    if(cache_file.is_open())
    {
        m_data.resize(size_t(cache_file.tellg()));
        cache_file.seekg(0, std::ios::beg);
        cache_file.read(m_data.get_data(), m_data.get_size());
    }

    cache_info.initialDataSize = m_data.get_size();
    cache_info.pInitialData = m_data.get_data();

    CHECK_VULKAN(vkCreatePipelineCache(m_device, &cache_info, nullptr, &m_pipeline_cache))

    return VK_SUCCESS;
}

void VulkanBase::save_pipeline_cache() const
{
    if(!m_pipeline_cache)
        return;

    std::ofstream cache_file(cache_location, std::ios::binary);

    if(!cache_file.is_open())
        return;

    size_t bufsize;

    if(vkGetPipelineCacheData(m_device, m_pipeline_cache, &bufsize, nullptr) != VK_SUCCESS)
        return;

    Custom::Vector<char> cache_data(bufsize);

    if(vkGetPipelineCacheData(m_device, m_pipeline_cache, &bufsize, cache_data.get_data()) != VK_SUCCESS)
        return;

    cache_file.write(cache_data.get_data(), cache_data.get_size());
}



void VulkanBase::set_viewport_scissor(VkCommandBuffer cmd_buf) const
{
    VkViewport viewport =
    {
        0.0f,
        0.0f,
        float(m_swapchain_extent.width),
        float(m_swapchain_extent.height),
        0.0f,
        1.0f
    };

    vkCmdSetViewport(cmd_buf, 0, 1, &viewport);

    VkRect2D scissor =
    {
        {0, 0},
        m_swapchain_extent
    };

    vkCmdSetScissor(cmd_buf, 0, 1, &scissor);
}
