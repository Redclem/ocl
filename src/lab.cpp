#include "lab.h"

Lab::Lab() : VulkanBase::VulkanBase(), Gui::EventProcessor::EventProcessor(), m_gui_root(this, this, &m_render_ressources), m_window(), m_icon(), INIT_VK_DEF(m_semaphore_image_ready),
    INIT_VK_DEF(m_semaphore_render_done), INIT_VK_DEF(m_command_pool), INIT_VK_DEF(m_fence_render), INIT_VK_DEF(m_render_command_buffer),
    INIT_VK_DEF(m_gui_render_buffer), INIT_VK_DEF(m_gui_update_command_buffer), INIT_VK_DEF(m_active_render_command_buffer), INIT_VK_DEF(m_containers_render_buffer), m_render_ressources(this), m_container_graphics(this),
    m_containers(), m_container_pipeline(this), m_active_element(nullptr), m_run(false), m_refresh(false), m_update_gui(false), m_update_active_elem(false), m_paused(false), m_update_render_commands(false),
    m_surface_changed(false)

{
    if(!SDL_WasInit(SDL_INIT_VIDEO))
    {
        if(SDL_Init(SDL_INIT_VIDEO))
        {
            log_message("Erreur init SDL : ", SDL_GetError());
            throw std::runtime_error(std::string("Erreur init SDL") + SDL_GetError());
        }

        atexit(SDL_Quit);
    }
}

Lab::Lab(Lab&& from) : VulkanBase::VulkanBase(std::move(from)), Gui::EventProcessor::EventProcessor(std::move(from)), INIT_MOVE(m_gui_root), INIT_MOVE(m_window), INIT_MOVE(m_icon), INIT_VK_EXCH(m_semaphore_image_ready),
    INIT_VK_EXCH(m_semaphore_render_done), INIT_VK_EXCH(m_command_pool), INIT_VK_EXCH(m_fence_render),INIT_VK_EXCH(m_render_command_buffer), INIT_VK_EXCH(m_gui_render_buffer),
    INIT_VK_EXCH(m_gui_update_command_buffer), INIT_VK_EXCH(m_active_render_command_buffer), INIT_VK_EXCH(m_containers_render_buffer), INIT_MOVE(m_render_ressources), INIT_MOVE(m_container_graphics),
    INIT_MOVE(m_containers), INIT_MOVE(m_container_pipeline), INIT_COPY(m_active_element), INIT_COPY(m_run), INIT_COPY(m_refresh), INIT_COPY(m_update_gui), INIT_COPY(m_update_active_elem), INIT_COPY(m_paused),
    INIT_COPY(m_update_render_commands), INIT_COPY(m_surface_changed)
{

}

Lab& Lab::operator=(Lab&& rhs)
{
    VulkanBase::operator=(std::move(rhs));
    Gui::EventProcessor::operator=(std::move(rhs));
    ASSIGN_MOVE(m_gui_root);
    ASSIGN_MOVE(m_window);
    ASSIGN_MOVE(m_icon);
    ASSIGN_SWAP(m_semaphore_image_ready);
    ASSIGN_SWAP(m_semaphore_render_done);
    ASSIGN_SWAP(m_command_pool);
    ASSIGN_COPY(m_run);
    ASSIGN_COPY(m_refresh);
    ASSIGN_SWAP(m_fence_render);
    ASSIGN_SWAP(m_render_command_buffer);
    ASSIGN_SWAP(m_gui_render_buffer);
    ASSIGN_SWAP(m_gui_update_command_buffer);
    ASSIGN_SWAP(m_active_render_command_buffer);
    ASSIGN_SWAP(m_containers_render_buffer);
    ASSIGN_COPY(m_active_element);
    ASSIGN_MOVE(m_render_ressources);
    ASSIGN_MOVE(m_container_graphics);
    ASSIGN_MOVE(m_containers);
    ASSIGN_MOVE(m_container_pipeline);
    ASSIGN_COPY(m_run);
    ASSIGN_COPY(m_refresh);
    ASSIGN_COPY(m_update_gui);
    ASSIGN_COPY(m_update_active_elem);
    ASSIGN_COPY(m_paused);
    ASSIGN_COPY(m_update_render_commands);
    ASSIGN_COPY(m_surface_changed);
    return *this;
}

Lab::~Lab()
{
    cleanup();
    //dtor
}

bool Lab::init()
{
    SDL_DisplayMode disp_mode;
    CHECK_SDL_BOOL(SDL_GetDisplayMode(0, 0, &disp_mode))
    CHECK_SDL_BOOL(m_window.create_window("OCL", disp_mode.w * 3 / 4, disp_mode.h * 3 / 4))

    CHECK_SDL_BOOL(m_icon.create("gfx/erlenmeyer.png"))
    m_window.set_icon(m_icon.get_surface());

    set_window(m_window.get_window());

    CHECK_VULKAN_BOOL(VulkanBase::init());

    CHECK_VULKAN(m_render_ressources.init_ressources())

    m_gui_root.compute_pixel_sizes();

    add_gui_elements();

    CHECK_VULKAN_BOOL(m_gui_root.init("gfx/consola.ttf", 15.0f))

    CHECK_VULKAN_BOOL(create_semaphores())

    CHECK_VULKAN_BOOL(create_command_pool())

    CHECK_VULKAN_BOOL(create_fence())

    if(!m_container_graphics.load_containers({"containers/beaker_basic.continfo"}))
    {
        log_message("Error load container graphics");
        return false;
    }

    CHECK_VULKAN_BOOL(m_container_graphics.create_buffer(m_render_ressources.get_command_pool()))

    CHECK_VULKAN_BOOL(m_container_pipeline.init())

    CHECK_VULKAN_BOOL(record_secondary())

    CHECK_VULKAN_BOOL(create_active_element_render_command_buffer())

    CHECK_VULKAN_BOOL(record_containers_render_buffer())

#ifdef DEBUG

    if(!exec_tests())
    {
        log_message("Tests failed");
        return false;
    }

#endif

    return true;
}

void Lab::run()
{
    SDL_Event evnt;
    m_run = true;
    m_refresh = true;
    while(m_run)
    {
        SDL_WaitEvent(&evnt);

        switch(evnt.type)
        {
        case SDL_QUIT:
            m_run = false;
            break;
        case SDL_WINDOWEVENT:
            switch(evnt.window.event)
            {
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                m_surface_changed = true;
                break;
            }
            break;
        case SDL_MOUSEMOTION:
            m_gui_root.update_active_element({evnt.motion.x, evnt.motion.y});
            if(m_paused)
                update_active_element();
            break;
        case SDL_MOUSEBUTTONUP:
            if(evnt.button.button == SDL_BUTTON_LEFT)
                if(m_paused)
                    m_gui_root.on_click();
            break;
        case SDL_KEYUP:
            switch(evnt.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                if(switch_pause())
                {
                    log_message("Error switch_pause()");
                    m_run = false;
                }
                break;
            case SDLK_F11:
            {
                int fullscreen = SDL_GetWindowFlags(m_window.get_window()) & SDL_WINDOW_FULLSCREEN;

                if(SDL_SetWindowFullscreen(m_window.get_window(), fullscreen ^ SDL_WINDOW_FULLSCREEN))
                {

                    log_message("Error SDL_SetWindowFullscreen(m_window.get_window(), fullscreen ^ SDL_WINDOW_FULLSCREEN) :", SDL_GetError());

                    m_run = false;
                    return;
                }

                m_surface_changed = true;
            }
            break;
            }
            break;

        }

        while((m_refresh || m_surface_changed) && m_run)
        {
            if(m_surface_changed)
            {
                if(on_surface_change() != VK_SUCCESS)
                {
                    log_message("Error on_size_change()");
                    m_run = false;
                }

                m_surface_changed = false;
            }

            VkResult render_code = render();
            if(render_code != VK_SUCCESS)
            {
                log_message("Error render : ", render_code);
                m_run = false;
            }

//            int w, h;
//            SDL_GetWindowSize(m_window.get_window(), &w, &h);
//            if(uint32_t(w) != get_swapchain_extent().width || uint32_t(h) != get_swapchain_extent().height)
//            {
//                m_surface_changed = true;
//            }
        }

    }
}

VkResult Lab::render()
{
    {
        VkResult res(vkWaitForFences(get_device(), 1, &m_fence_render, VK_TRUE, 0));

        if(res == VK_TIMEOUT)
        {
            return VK_SUCCESS;
        }
        else if (res != VK_SUCCESS)
        {
            log_message("Error vkWaitForFences(get_device(), 1, &m_fence_render, VK_TRUE, 0) :", res);

            return res;
        }
    }

    if(m_update_render_commands)
    {
        CHECK_VULKAN(record_secondary())
        m_update_render_commands = false;
    }

    uint32_t frame_index;

    {
        VkResult result = vkAcquireNextImageKHR(get_device(), get_swapchain(), ~uint64_t(0), m_semaphore_image_ready, VK_NULL_HANDLE, &frame_index);
        if(result == VK_ERROR_OUT_OF_DATE_KHR)
        {
            m_surface_changed = true;
            return VK_SUCCESS;
        }
        else if(result == VK_SUBOPTIMAL_KHR)
        {
            m_surface_changed = true;
        }
        else if (result != VK_SUCCESS)
        {
            log_message("Error vkAcquireNextImageKHR(get_device(), get_swapchain(), ~uint64_t(0), m_semaphore_image_ready, VK_NULL_HANDLE, &frame_index) :", result);
            return result;
        }
    }

    if(m_render_command_buffer == VK_NULL_HANDLE)
    {
        VkCommandBufferAllocateInfo alloc_info =
        {
            VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            nullptr,
            m_command_pool,
            VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            1
        };

        CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_render_command_buffer))
    }
    else
        vkResetCommandBuffer(m_render_command_buffer, 0);

    VkCommandBufferBeginInfo beg_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        nullptr
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_render_command_buffer, &beg_info))

    if(m_update_gui)
    {
        vkCmdExecuteCommands(m_render_command_buffer, 1, &m_gui_update_command_buffer);
        m_update_gui = false;
    }

    if(m_update_active_elem)
    {
        m_gui_root.update_active_element_buffer(m_active_element, m_render_command_buffer);
        m_update_active_elem = false;
    }

    VkClearValue clr_val;
    clr_val.color.float32[0] = 1.0f;
    clr_val.color.float32[1] = 1.0f;
    clr_val.color.float32[2] = 1.0f;
    clr_val.color.float32[3] = 0.0f;

    VkRenderPassBeginInfo render_pass_info =
    {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        nullptr,
        get_render_pass(),
        get_framebuffers()[frame_index],
        {{0, 0}, get_swapchain_extent()},
        1,
        &clr_val
    };

    vkCmdBeginRenderPass(m_render_command_buffer, &render_pass_info, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

    vkCmdExecuteCommands(m_render_command_buffer, 1, &m_containers_render_buffer);

    if(m_paused)
    {
        vkCmdExecuteCommands(m_render_command_buffer, 1, &m_gui_render_buffer);

        if(m_active_element)
            vkCmdExecuteCommands(m_render_command_buffer, 1, &m_active_render_command_buffer);
    }

    vkCmdEndRenderPass(m_render_command_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_render_command_buffer))

    VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submit_info =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        1,
        &m_semaphore_image_ready,
        &wait_stage,
        1,
        &m_render_command_buffer,
        1,
        &m_semaphore_render_done
    };


    CHECK_VULKAN(vkResetFences(get_device(), 1, &m_fence_render))

    CHECK_VULKAN(vkQueueSubmit(get_graphics_queue(), 1, &submit_info, m_fence_render))

    VkSwapchainKHR swapchain = get_swapchain();

    VkPresentInfoKHR present_info =
    {
        VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        nullptr,
        1,
        &m_semaphore_render_done,
        1,
        &swapchain,
        &frame_index,
        nullptr
    };

    {

        VkResult res(vkQueuePresentKHR(get_present_queue(), &present_info));

        if(res == VK_ERROR_OUT_OF_DATE_KHR)
        {
            m_surface_changed = true;
        }
        else if(res == VK_SUBOPTIMAL_KHR)
        {
            m_surface_changed = true;
        }
        else if (res != VK_SUCCESS)
        {

            log_message("Error vkQueuePresentKHR(get_present_queue(), &present_info)");

            return res;
        }

    }

    m_refresh = false;

    return VK_SUCCESS;
}

VkResult Lab::create_semaphores()
{
    VkSemaphoreCreateInfo sem_info =
    {
        VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        nullptr,
        0
    };

    CHECK_VULKAN(vkCreateSemaphore(get_device(), &sem_info, nullptr, &m_semaphore_image_ready))
    CHECK_VULKAN(vkCreateSemaphore(get_device(), &sem_info, nullptr, &m_semaphore_render_done))

    return VK_SUCCESS;
}

void Lab::cleanup()
{
    if(m_fence_render)
        vkWaitForFences(get_device(), 1, &m_fence_render, VK_TRUE, ~uint64_t(0));

    if(m_fence_render)
    {
        vkDestroyFence(get_device(), m_fence_render, nullptr);
        ASSIGN_VK_DEF(m_fence_render);
    }
    if(m_command_pool)
    {
        vkDestroyCommandPool(get_device(), m_command_pool, nullptr);
        ASSIGN_VK_DEF(m_command_pool);
    }
    if(m_semaphore_image_ready)
    {
        vkDestroySemaphore(get_device(), m_semaphore_image_ready, nullptr);
        ASSIGN_VK_DEF(m_semaphore_image_ready);
    }
    if(m_semaphore_render_done)
    {
        vkDestroySemaphore(get_device(), m_semaphore_render_done, nullptr);
        ASSIGN_VK_DEF(m_semaphore_render_done);
    }

    m_container_graphics.cleanup();
    m_container_pipeline.cleanup();
    m_containers.clear();
    m_gui_root.cleanup();
    m_render_ressources.cleanup();

    VulkanBase::cleanup();
}

VkResult Lab::create_command_pool()
{
    VkCommandPoolCreateInfo pool_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        nullptr,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        0
    };

    get_graphic_index(pool_info.queueFamilyIndex);

    CHECK_VULKAN(vkCreateCommandPool(get_device(), &pool_info, nullptr, &m_command_pool))

    return VK_SUCCESS;
}

VkResult Lab::create_fence()
{
    VkFenceCreateInfo fence_info =
    {
        VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        nullptr,
        VK_FENCE_CREATE_SIGNALED_BIT
    };

    CHECK_VULKAN(vkCreateFence(get_device(), &fence_info, nullptr, &m_fence_render))

    return VK_SUCCESS;
}

VkResult Lab::record_secondary()
{
    if(m_gui_render_buffer == VK_NULL_HANDLE)
    {
        VkCommandBufferAllocateInfo alloc_info =
        {
            VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            nullptr,
            m_command_pool,
            VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            1
        };

        CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_gui_render_buffer))
    }
    else
        CHECK_VULKAN(vkResetCommandBuffer(m_gui_render_buffer, 0))

        VkCommandBufferInheritanceInfo inh_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        nullptr,
        get_render_pass(),
        0,
        VK_NULL_HANDLE,
        VK_FALSE,
        0,
        0
    };

    VkCommandBufferBeginInfo begin_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
        &inh_info
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_gui_render_buffer, &begin_info))

    vkCmdBindPipeline(m_gui_render_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_container_pipeline.get_pipeline());

    m_gui_root.render(m_gui_render_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_gui_render_buffer))

    m_update_render_commands = false;

    return VK_SUCCESS;
}

VkResult Lab::wait_render_done()
{
    CHECK_VULKAN(vkWaitForFences(get_device(), 1, &m_fence_render, VK_TRUE, ~uint64_t(0)))

    return VK_SUCCESS;
}

VkResult Lab::on_surface_change()
{
    CHECK_VULKAN(wait_render_done())

    VkExtent2D prev_extent = get_swapchain_extent();

    CHECK_VULKAN(recreate_swapchain())
    CHECK_VULKAN(record_secondary())
    CHECK_VULKAN(update_active_element_render_command_buffer())
    CHECK_VULKAN(record_containers_render_buffer())

    if(prev_extent.width != get_swapchain_extent().width || prev_extent.height != get_swapchain_extent().height)
    {
        if(m_paused)
        {
            CHECK_VULKAN(update_gui())

            if(m_active_element)
                m_update_active_elem = true;
        }

        m_refresh = true;
    }

    return VK_SUCCESS;
}

VkResult Lab::update_gui()
{
    m_gui_root.compute_pixel_sizes();

    if(m_gui_update_command_buffer == VK_NULL_HANDLE)
    {
        VkCommandBufferAllocateInfo alloc_info =
        {
            VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            nullptr,
            m_command_pool,
            VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            1
        };

        CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_gui_update_command_buffer))
    }
    else
        CHECK_VULKAN(vkResetCommandBuffer(m_gui_update_command_buffer, 0))

        VkCommandBufferInheritanceInfo inherit_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        nullptr,
        VK_NULL_HANDLE,
        0,
        VK_NULL_HANDLE,
        VK_FALSE,
        0,
        0
    };

    VkCommandBufferBeginInfo begin_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        &inherit_info
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_gui_update_command_buffer, &begin_info))

    m_gui_root.update_gui(m_gui_update_command_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_gui_update_command_buffer))

    m_update_gui = true;

    return VK_SUCCESS;
}

void Lab::add_gui_elements()
{
    Gui::Menu * main_menu = m_gui_root.add_element<Gui::Menu>(Gui::Rect{-1.0f, -1.0f, 2.0f, 2.0f}, 200, 20);
    /** Make sure main menu is first */

    Gui::Menu * resolution_menu = m_gui_root.add_element<Gui::Menu>(Gui::Rect{-1.0f, -1.0f, 2.0f, 2.0f}, 200, 20);
    {

        std::ifstream file("data/resolutions.txt");
        if(!file.is_open())
        {
            Gui::Label * label = m_gui_root.add_element<Gui::Label>(Gui::Element::null_rect, "Could not load resolutions");
            resolution_menu->add_element(label);
        }
        else
        {
            SDL_DisplayMode mode;
            SDL_GetDisplayMode(0, 0, &mode);

            Gui::Label * label = m_gui_root.add_element<Gui::Label>(Gui::Element::null_rect, "Resolution");
            resolution_menu->add_element(label);

            do
            {
                if(file.peek() == '\n')
                    file.ignore();

                uint32_t w, h;
                file >> w;

                while(!isdigit(file.peek()))
                    file.ignore();

                file >> h;

                if(int(w) > mode.w || int(h) > mode.h)
                    continue;

                Gui::ResolutionButton * button = m_gui_root.add_element<Gui::ResolutionButton>(VkExtent2D{w, h});
                resolution_menu->add_element(button);
            }
            while(!file.eof());
        }

        Gui::PopButton * back_button = m_gui_root.add_element<Gui::PopButton>("gfx/back.png", "Back");

        resolution_menu->add_element(back_button);
    }

    resolution_menu->pack();

    Gui::Menu * settings_menu = m_gui_root.add_element<Gui::Menu>(Gui::Rect{-1.0f, -1.0f, 2.0f, 2.0f}, 200, 20);

    {
        Gui::Label * label = m_gui_root.add_element<Gui::Label>(Gui::Element::null_rect, "Settings");

        Gui::FullscreenToggleButton * fullscreen_button = m_gui_root.add_element<Gui::FullscreenToggleButton>("gfx/fullscreen.png", "Toggle fullscreen");

        Gui::PushButton * res_menu_button = m_gui_root.add_element<Gui::PushButton>("gfx/resolution.png", "Resolution", resolution_menu);

        Gui::PopButton * back_button = m_gui_root.add_element<Gui::PopButton>("gfx/back.png", "Back");

        settings_menu->add_element(label);
        settings_menu->add_element(fullscreen_button);
        settings_menu->add_element(res_menu_button);
        settings_menu->add_element(back_button);

        settings_menu->pack();

    }

    {
        Gui::Label * label = m_gui_root.add_element<Gui::Label>(Gui::Element::null_rect, "Paused");

        Gui::PopButton * pop_button = m_gui_root.add_element<Gui::PopButton>("gfx/play.png", "Unpause");

        Gui::PushButton * push_button = m_gui_root.add_element<Gui::PushButton>("gfx/gear.png", "Settings", settings_menu);

        Gui::QuitButton * quit_button = m_gui_root.add_element<Gui::QuitButton>("gfx/X.png", "Quit");

        main_menu->add_element(label);
        main_menu->add_element(pop_button);
        main_menu->add_element(push_button);
        main_menu->add_element(quit_button);

        main_menu->pack();


        m_gui_root.push_main_element(main_menu);
    }
}

VkResult Lab::create_active_element_render_command_buffer()
{
    VkCommandBufferAllocateInfo alloc_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        nullptr,
        m_command_pool,
        VK_COMMAND_BUFFER_LEVEL_SECONDARY,
        1
    };

    CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_active_render_command_buffer))

    VkCommandBufferInheritanceInfo inherit_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        nullptr,
        get_render_pass(),
        0,
        VK_NULL_HANDLE,
        VK_FALSE,
        0,
        0
    };

    VkCommandBufferBeginInfo begin_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
        &inherit_info
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_active_render_command_buffer, &begin_info))

    m_gui_root.render_active_element_box(m_active_render_command_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_active_render_command_buffer))

    return VK_SUCCESS;
}

VkResult Lab::update_active_element_render_command_buffer()
{
    CHECK_VULKAN(vkResetCommandBuffer(m_active_render_command_buffer, 0))

    VkCommandBufferInheritanceInfo inherit_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        nullptr,
        get_render_pass(),
        0,
        VK_NULL_HANDLE,
        VK_FALSE,
        0,
        0
    };

    VkCommandBufferBeginInfo begin_info =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
        &inherit_info
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_active_render_command_buffer, &begin_info))

    m_gui_root.render_active_element_box(m_active_render_command_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_active_render_command_buffer))

    return VK_SUCCESS;
}


void Lab::update_active_element()
{
    const Gui::Element * new_active_element(m_gui_root.get_active_element());

    if(new_active_element != m_active_element)
    {
        m_update_active_elem = new_active_element != nullptr;
        m_refresh = true;
        m_active_element = new_active_element;
    }
}

void Lab::process_event(const Gui::Event& event)
{
    switch(event.type)
    {
    case Gui::Event::Type::Quit:
        m_run = false;
        break;
    case Gui::Event::Type::PopElement:
        m_gui_root.pop_main_element();
        if(!m_gui_root.has_main_element())
        {
            m_paused = false;
        }
        m_refresh = true;
        m_update_render_commands = true;
        update_active_element();
        break;
    case Gui::Event::Type::PushElement:
        m_gui_root.push_main_element(event.pushed_element_event.pushed_element);
        m_refresh = true;
        m_update_render_commands = true;
        update_active_element();
        break;
    case Gui::Event::Type::ToggleFullscreen:
    {
        int fullscreen = SDL_GetWindowFlags(m_window.get_window()) & SDL_WINDOW_FULLSCREEN;

        if(SDL_SetWindowFullscreen(m_window.get_window(), fullscreen ^ SDL_WINDOW_FULLSCREEN))
        {
            log_message("Error SDL_SetWindowFullscreen(m_window.get_window(), fullscreen ^ SDL_WINDOW_FULLSCREEN) :", SDL_GetError());

            m_run = false;
            return;
        }

        m_surface_changed = true;

        break;
    }
    case Gui::Event::Type::SetResolution:
    {
        if(get_swapchain_extent().width == event.set_resolution_event.extent.width &&
                get_swapchain_extent().height == event.set_resolution_event.extent.height)
        {
            return;
        }



        int fullscreen = SDL_GetWindowFlags(m_window.get_window()) & SDL_WINDOW_FULLSCREEN;

        if(fullscreen)
        {
            if(SDL_SetWindowFullscreen(m_window.get_window(), 0))
            {
                log_message("Error SDL_SetWindowFullscreen(m_window.get_window(), 0) :", SDL_GetError());

                m_run = false;
                return;
            }
        }

        m_surface_changed = true;


        SDL_SetWindowSize(m_window.get_window(), int(event.set_resolution_event.extent.width), int(event.set_resolution_event.extent.height));

        break;
    }
    break;
    default:

        log_message("Warning : Unhandled event");

        break;
    }
}

VkResult Lab::switch_pause()
{
    m_paused = !m_paused;

    if(m_paused)
    {
        CHECK_VULKAN(vkWaitForFences(get_device(), 1, &m_fence_render, VK_TRUE, ~uint64_t(0)))

        m_gui_root.clear_main_element_stack();
        m_gui_root.push_main_element(m_gui_root.get_element_at(0));

        CHECK_VULKAN(record_secondary())
        CHECK_VULKAN(update_gui())
        CHECK_VULKAN(update_active_element_render_command_buffer())

        m_active_element = m_gui_root.get_active_element();

        m_update_active_elem = m_active_element != nullptr;
    }

    m_refresh = true;

    return VK_SUCCESS;
}

void Lab::render_container(size_t container_index, VkCommandBuffer cmd_buf) const
{
    const Container & cont = m_containers[container_index];

    Color::LinearRGBA color = {0.0f, 0.0f, 0.0f, 1.0f};
    float ar = get_aspect_ratio();

    const ContainerGraphicsManager::ContainerGraphic * graphic = cont.get_graphic();
    const ContainerInfo * info = &graphic->get_info();

    VkBuffer buffer = m_container_graphics.get_buffer();
    VkDeviceSize zero(0);

    vkCmdPushConstants(cmd_buf, m_container_pipeline.get_pipeline_layout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, 16, &color);
    vkCmdPushConstants(cmd_buf, m_container_pipeline.get_pipeline_layout(), VK_SHADER_STAGE_VERTEX_BIT, 16, 8, &cont.get_position());
    vkCmdPushConstants(cmd_buf, m_container_pipeline.get_pipeline_layout(), VK_SHADER_STAGE_VERTEX_BIT, 24, 4, &ar);

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &buffer, &zero);
    vkCmdBindIndexBuffer(cmd_buf, buffer, graphic->get_outside_index_offset(), VK_INDEX_TYPE_UINT16);


    VkRect2D scissor = {
        {0, 0},
        get_swapchain_extent()
    };

    vkCmdSetScissor(cmd_buf, 0, 1, &scissor);

    vkCmdDrawIndexed(cmd_buf, graphic->get_outside_index_count(), 1, 0, 0, 0);

    vkCmdBindIndexBuffer(cmd_buf, buffer, graphic->get_inside_index_offset(), VK_INDEX_TYPE_UINT16);

    uint32_t inside_index_count(graphic->get_inside_index_count());
    uint32_t scissor_end(get_swapchain_extent().height);

    volume_t volume(0.0f);

    for(const Chemistry::PhasePtr & phase : cont.get_phases())
    {
        /** Set scissor */

        volume += phase->get_volume();
        float fluid_level = cont.get_position().y + info->get_y_coord(info->get_fluid_level(volume));

        scissor.offset.y = uint32_t(float(get_swapchain_extent().height) * (fluid_level * 0.5f + 0.5f));
        scissor.extent.height = scissor_end - scissor.offset.y;

        vkCmdSetScissor(cmd_buf, 0, 1, &scissor);

        scissor_end = scissor.offset.y;

        /** Set color */

        color = phase->get_color();
        vkCmdPushConstants(cmd_buf, m_container_pipeline.get_pipeline_layout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, 16, &color);

        /** Render */

        vkCmdDrawIndexed(cmd_buf, inside_index_count, 1, 0, 0, 0);
    }
}

VkResult Lab::record_containers_render_buffer()
{
    if(m_containers_render_buffer != VK_NULL_HANDLE)
    {
        CHECK_VULKAN(vkResetCommandBuffer(m_containers_render_buffer, 0))
    }
    else
    {
        VkCommandBufferAllocateInfo alloc_info = {
            VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            nullptr,
            m_command_pool,
            VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            1
        };

        CHECK_VULKAN(vkAllocateCommandBuffers(get_device(), &alloc_info, &m_containers_render_buffer))
    }

    VkCommandBufferInheritanceInfo inh_info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        nullptr,
        get_render_pass(),
        0,
        VK_NULL_HANDLE,
        VK_FALSE,
        0,
        0
    };

    VkCommandBufferBeginInfo beg_info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
        &inh_info
    };

    VkViewport viewport = {
        0.0f,
        0.0f,
        float(get_swapchain_extent().width),
        float(get_swapchain_extent().height),
        0.0f,
        1.0f
    };

    CHECK_VULKAN(vkBeginCommandBuffer(m_containers_render_buffer, &beg_info))

    vkCmdBindPipeline(m_containers_render_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_container_pipeline.get_pipeline());
    vkCmdSetViewport(m_containers_render_buffer, 0, 1, &viewport);

    for(size_t container(0);container != m_containers.size();container++)
        render_container(container, m_containers_render_buffer);

    CHECK_VULKAN(vkEndCommandBuffer(m_containers_render_buffer))

    return VK_SUCCESS;
}

#ifdef DEBUG

bool Lab::exec_tests()
{
    m_containers.push_back(Container(&m_container_graphics.at(0)));
    m_containers[0].add_phase(new Chemistry::ConstantColorPhase(2000.0f,{1.0f, 0.0f, 0.0f, 0.5f}));
    m_containers[0].add_phase(new Chemistry::ConstantColorPhase(3800.0f,{0.0f, 0.0f, 1.0f, 0.5f}));

    CHECK_VULKAN_BOOL(record_containers_render_buffer())

    return true;
}

#endif // DEBUG


