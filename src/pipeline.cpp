#include "pipeline.h"

using namespace Vulkan::Pipeline;

Base::Base(VulkanBase * base) : GraphicObject::GraphicObject(base), INIT_VK_DEF(m_pipeline)
{

}

void Base::cleanup()
{
    if(m_pipeline)
    {
        vkDestroyPipeline(get_device(), m_pipeline, nullptr);
        ASSIGN_VK_DEF(m_pipeline);
    }
}

Base::Base(Base&& from) : GraphicObject::GraphicObject(std::move(from)), INIT_VK_EXCH(m_pipeline)
{

}

Base & Base::operator=(Base && rhs)
{
    GraphicObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_pipeline);
    return *this;
}

VkResult Modular::init()
{
    CHECK_VULKAN(create())
    return VK_SUCCESS;
}

VkResult Modular::create()
{
    std::array<VkPipelineShaderStageCreateInfo, 2> stages_infos;
    stages_infos[0] =
    {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,
        0,
        VK_SHADER_STAGE_VERTEX_BIT,
        get_vertex_shader(),
        "main",
        nullptr
    };
    stages_infos[1] =
    {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,
        0,
        VK_SHADER_STAGE_FRAGMENT_BIT,
        get_fragment_shader(),
        "main",
        nullptr
    };

    Custom::Vector<VkVertexInputBindingDescription> vertex_input_bindings = get_vertex_input_bindings_descriptions();
    Custom::Vector<VkVertexInputAttributeDescription> vertex_input_attribs = get_vertex_input_attributes_descriptions();

    VkPipelineVertexInputStateCreateInfo vertex_input_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        nullptr,
        0,
        uint32_t(vertex_input_bindings.get_size()),
        vertex_input_bindings.get_data(),
        uint32_t(vertex_input_attribs.get_size()),
        vertex_input_attribs.get_data(),
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        nullptr,
        0,
        get_primitive_topology(),
        has_primitive_restart()
    };

    Custom::Vector<VkViewport> viewports = get_viewports();
    Custom::Vector<VkRect2D> scissors = get_scissors();

    VkPipelineViewportStateCreateInfo viewport_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        nullptr,
        0,
        uint32_t(viewports.get_size()),
        viewports.get_data(),
        uint32_t(scissors.get_size()),
        scissors.get_data()
    };

    VkPipelineRasterizationStateCreateInfo raster_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        nullptr,
        0,
        has_depth_clamp(),
        VK_FALSE,
        get_polygon_mode(),
        get_cull_mode(),
        get_front_face(),
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisample_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        nullptr,
        0,
        get_rasterization_samples(),
        VK_FALSE,
        0.0f,
        nullptr,
        VK_FALSE,
        VK_FALSE
    };

    VkPipelineColorBlendAttachmentState color_attach_state =
    {
        VK_TRUE,
        VK_BLEND_FACTOR_SRC_ALPHA,
        VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        VK_BLEND_OP_ADD,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        VK_BLEND_OP_ADD,
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo blend_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_FALSE,
        VK_LOGIC_OP_NO_OP,
        1,
        &color_attach_state,
        {1.0f, 1.0f, 1.0f, 1.0f}
    };

    Custom::Vector<VkDynamicState> dynamic_states = get_dynamic_states();

    VkPipelineDynamicStateCreateInfo dynamic_info =
    {
        VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        nullptr,
        0,
        uint32_t(dynamic_states.get_size()),
        dynamic_states.get_data()
    };

    VkGraphicsPipelineCreateInfo pipeline_info =
    {
        VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        nullptr,
        0,
        stages_infos.size(),
        stages_infos.data(),
        &vertex_input_info,
        &input_assembly_info,
        nullptr,
        &viewport_info,
        &raster_info,
        &multisample_info,
        nullptr,
        &blend_info,
        &dynamic_info,
        get_pipeline_layout(),
        get_base()->get_render_pass(),
        0,
        VK_NULL_HANDLE,
        -1
    };

    CHECK_VULKAN(Base::create(pipeline_info))

    return VK_SUCCESS;
}

VkResult Base::load_shader(const char* path, VkShaderModule& module)
{
    std::ifstream module_file(path, std::ios::ate | std::ios::binary);

    if(!module_file.is_open())
    {
        log_message("Error load_shader : could not open ", path);
        return VK_ERROR_UNKNOWN;
    }

    size_t file_size = size_t(module_file.tellg());
    size_t module_size = file_size;

    if(module_size & 3)
    {
        module_size &= ~size_t(3);
        module_size += 4;
    }

    module_file.seekg(0, std::ios::beg);
    module_file.clear();

    Custom::Vector<uint32_t> module_data(module_size >> 2);

    module_file.read(reinterpret_cast<char*>(module_data.get_data()), file_size);

    VkShaderModuleCreateInfo shader_info =
    {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        nullptr,
        0,
        module_size,
        module_data.get_data()
    };

    CHECK_VULKAN(vkCreateShaderModule(get_device(), &shader_info, nullptr, &module))

    return VK_SUCCESS;
}

Custom::Vector<VkVertexInputBindingDescription> Texture::get_vertex_input_bindings_descriptions() const
{
    return Custom::Vector<VkVertexInputBindingDescription> {{
            0,
            sizeof(Vertex::Textured),
            VK_VERTEX_INPUT_RATE_VERTEX
        }
    };
}

Custom::Vector<VkVertexInputAttributeDescription> Texture::get_vertex_input_attributes_descriptions() const
{
    return Custom::Vector<VkVertexInputAttributeDescription>
    {
        {
            0,
            0,
            VK_FORMAT_R32G32_SFLOAT,
            0
        },
        {
            1,
            0,
            VK_FORMAT_R32G32_SFLOAT,
            offsetof(Vertex::Textured, u)
        }
    };
}

std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, 1> Texture::get_descriptor_set_layouts_bindings() const
{
    return std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, 1>
    {
        Custom::Vector<VkDescriptorSetLayoutBinding>{
            {
                0,
                VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                1,
                VK_SHADER_STAGE_FRAGMENT_BIT,
                &m_sampler
            }
        }
    };
}

Texture::Texture(VulkanBase * base) : Complete<1>::Complete(base), INIT_VK_DEF(m_sampler)
{

}

Texture::Texture(Texture && from) : Complete<1>::Complete(std::move(from)), INIT_VK_EXCH(m_sampler)
{
}

Texture & Texture::operator=(Texture && rhs)
{
    Complete<1>::operator=(std::move(rhs));
    ASSIGN_SWAP(m_sampler);
    return *this;
}

Custom::Vector<VkVertexInputBindingDescription> Fill::get_vertex_input_bindings_descriptions() const
{
    return
    {
        {
            0,
            sizeof(Vertex::Basic),
            VK_VERTEX_INPUT_RATE_VERTEX
        }
    };
}

Custom::Vector<VkVertexInputAttributeDescription> Fill::get_vertex_input_attributes_descriptions() const
{
    return
    {
        {
            0,
            0,
            VK_FORMAT_R32G32_SFLOAT,
            0
        }
    };
}

Custom::Vector<VkPushConstantRange> Fill::get_push_constant_ranges() const
{
    return
    {
        {
            VK_SHADER_STAGE_FRAGMENT_BIT,
            0,
            sizeof(Color::LinearRGBA)
        }
    };
}

Line::Line(Line && from) : Modular::Modular(std::move(from)), INIT_COPY(m_parent)
{

}

Line & Line::operator=(Line && rhs)
{
    Modular::operator=(std::move(rhs));
    ASSIGN_COPY(m_parent);
    return *this;
}

Custom::Vector<VkVertexInputBindingDescription> Container::get_vertex_input_bindings_descriptions() const
{
    return {{
            0,
            sizeof(Vertex::Basic),
            VK_VERTEX_INPUT_RATE_VERTEX
        }};
}

Custom::Vector<VkVertexInputAttributeDescription> Container::get_vertex_input_attributes_descriptions() const
{
    return {{
            0,
            0,
            VK_FORMAT_R32G32_SFLOAT,
            0
        }};
}

Custom::Vector<VkPushConstantRange> Container::get_push_constant_ranges() const
{
    return
    {
        { /** Aspect Ratio and position */
            VK_SHADER_STAGE_VERTEX_BIT,
            16,
            12
        },
        { /** Color */
            VK_SHADER_STAGE_FRAGMENT_BIT,
            0,
            16
        }
    };
}

