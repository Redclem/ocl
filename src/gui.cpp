#include "gui.h"

namespace Gui
{

using namespace Vulkan;

Root::Root(VulkanBase * base, EventProcessor * event_proc, const RenderRessources * render_ressources) : MemoryObject::MemoryObject(base), m_render_ressources(render_ressources), INIT_VK_DEF(m_descriptor_pool),
    INIT_VK_DEF(m_shared_buffer), m_textures(), m_texts(), m_elements(), m_main_elements_stack(), m_font(), m_font_data(nullptr),
    m_font_scale(0.0f), m_cursor_pos{0, 0}, m_pixel_height(0.0f), m_pixel_width(0.0f), m_event_proc(event_proc)
{

}

Root::Root(Root&& from) : MemoryObject::MemoryObject(std::move(from)), INIT_COPY(m_render_ressources), INIT_VK_EXCH(m_descriptor_pool),
    INIT_VK_EXCH(m_shared_buffer), INIT_MOVE(m_textures), INIT_MOVE(m_texts), INIT_MOVE(m_elements), INIT_MOVE(m_main_elements_stack),
    INIT_EXCHANGE(m_font, {}), INIT_EXCHANGE(m_font_data, nullptr), INIT_COPY(m_font_scale), INIT_COPY(m_cursor_pos), INIT_COPY(m_pixel_height), INIT_COPY(m_pixel_width), INIT_COPY(m_event_proc)

{

}

Root& Root::operator=(Root&& rhs)
{
    MemoryObject::operator=(std::move(rhs));
    ASSIGN_COPY(m_render_ressources);
    ASSIGN_SWAP(m_descriptor_pool);
    ASSIGN_SWAP(m_shared_buffer);
    ASSIGN_MOVE(m_textures);
    ASSIGN_MOVE(m_texts);
    ASSIGN_MOVE(m_elements);
    ASSIGN_MOVE(m_main_elements_stack);
    ASSIGN_SWAP(m_font);
    ASSIGN_SWAP(m_font_data);
    ASSIGN_COPY(m_font_scale);
    ASSIGN_COPY(m_pixel_height);
    ASSIGN_COPY(m_pixel_width);
    ASSIGN_COPY(m_event_proc);
    return *this;
}

VkResult Root::init(const char * font, float font_hgt)
{
    std::ifstream font_file(font, std::ios::binary | std::ios::ate);
    if(!font_file.is_open())
    {
        log_message("Could not open", font);
        return VK_ERROR_UNKNOWN;
    }

    size_t buffer_size = size_t(font_file.tellg());
    font_file.seekg(0);
    font_file.clear();

    m_font_data = new unsigned char[buffer_size];
    font_file.read(reinterpret_cast<char*>(m_font_data), std::streamsize(buffer_size));

    font_file.close();

    int err(stbtt_InitFont(&m_font, m_font_data, 0));
    if(err == 0)
    {
        log_message("Error init font");
        return VK_ERROR_UNKNOWN;
    }

    m_font_scale = stbtt_ScaleForPixelHeight(&m_font, font_hgt);

    /***********************************************//**
     * Init gui elements and ressources for elements
     **************************************************/

    if(m_elements.empty())
    {
        log_message("Warning : Empty GUI initialized");
        return VK_SUCCESS;
    }

    for(Element * elem : m_elements)
    {
        CHECK_VULKAN(elem->first_initialization())
    }



    CHECK_VULKAN(create_shared_buffer())

    CHECK_VULKAN(create_descriptor_pool())

    CHECK_VULKAN(allocate_memory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT))

    CHECK_VULKAN(create_views())

    CHECK_VULKAN(create_descriptor_sets())

    Vulkan::CommandBuffer cmd_buf(get_base());

    CHECK_VULKAN(cmd_buf.allocate(VK_COMMAND_BUFFER_LEVEL_PRIMARY, m_render_ressources->get_command_pool()))
    CHECK_VULKAN(cmd_buf.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT))

    Custom::Vector<std::unique_ptr<Vulkan::Buffer>> host_buffers;
    host_buffers.push_back(std::make_unique<Vulkan::Buffer, VulkanBase*>(get_base()));

    CHECK_VULKAN(upload_images(cmd_buf.get_command_buffer(), *host_buffers.back()))

    VkDeviceSize buffer_offset(reserved_buffer_size);

    for(Element * elem : m_elements)
    {
        if(host_buffers.back()->get_buffer())
            host_buffers.push_back(std::make_unique<Vulkan::Buffer, VulkanBase*>(get_base()));

        elem->set_buffer_offset(buffer_offset);
        CHECK_VULKAN(elem->second_initialization(*host_buffers.back(), cmd_buf.get_command_buffer()));

        buffer_offset += elem->get_buffer_size();
    }

    CHECK_VULKAN(cmd_buf.end())

    VkCommandBuffer cmd_buf_hdl = cmd_buf.get_command_buffer();

    VkSubmitInfo submit_info =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        0,
        nullptr,
        nullptr,
        1,
        &cmd_buf_hdl,
        0,
        nullptr
    };

    Vulkan::Fence fnc(get_base());

    CHECK_VULKAN(fnc.create())

    CHECK_VULKAN(vkQueueSubmit(get_base()->get_graphics_queue(), 1, &submit_info, fnc.get_fence()))

    CHECK_VULKAN(fnc.wait())

    return VK_SUCCESS;
}

void Root::cleanup()
{
    if(!m_elements.empty())
    {
        for(Element * & elem : m_elements)
        {
            if(elem)
            {
                elem->cleanup();
                delete elem;
                elem = nullptr;
            }
        }

        m_elements.clear();
    }

    if(m_shared_buffer)
    {
        vkDestroyBuffer(get_device(), m_shared_buffer, nullptr);
        ASSIGN_VK_DEF(m_shared_buffer);
    }
    if(!m_textures.empty())
    {
        for(texture_map_t::ValueType & iter : m_textures)
        {
            delete[] iter.first;
            iter.first = nullptr;

            Texture & texture = iter.second;
            vkDestroyImageView(get_device(), texture.view, nullptr);
            ASSIGN_VK_DEF(texture.view);
            vkDestroyImage(get_device(), texture.image, nullptr);
            ASSIGN_VK_DEF(texture.image);
        }
        m_textures.clear();
    }
    if(!m_texts.empty())
    {
        for(texture_map_t::ValueType & iter : m_texts)
        {
            delete[] iter.first;
            iter.first = nullptr;

            Texture & texture = iter.second;
            vkDestroyImageView(get_device(), texture.view, nullptr);
            ASSIGN_VK_DEF(texture.view);
            vkDestroyImage(get_device(), texture.image, nullptr);
            ASSIGN_VK_DEF(texture.image);
        }
        m_texts.clear();
    }
    if(m_shared_buffer)
    {
        vkDestroyBuffer(get_device(), m_shared_buffer, nullptr);
        ASSIGN_VK_DEF(m_shared_buffer);
    }
    if(m_descriptor_pool)
    {
        vkDestroyDescriptorPool(get_device(), m_descriptor_pool, nullptr);
        ASSIGN_VK_DEF(m_descriptor_pool);
    }
    if(m_font_data)
    {
        delete[] m_font_data;
        m_font_data = nullptr;
    }

    MemoryObject::cleanup();
}

VkMemoryRequirements Root::get_memory_requirements() const
{
    VkMemoryRequirements global_req = get_inner_memory_requirements();


    for(Element * element : m_elements)
    {
        add_memory_requirements(global_req, element->get_memory_requirements());
    }

    return global_req;
}

VkMemoryRequirements Root::get_inner_memory_requirements() const
{
    VkMemoryRequirements mem_req = {0, 1, ~uint32_t(0)};

    for(const texture_map_t::ValueType & iter : m_textures)
    {
        const Texture & texture = iter.second;

        VkMemoryRequirements img_req;
        vkGetImageMemoryRequirements(get_device(), texture.image, &img_req);

        add_memory_requirements(mem_req, img_req);
    }

    for(const texture_map_t::ValueType & iter : m_texts)
    {
        const Texture & texture = iter.second;

        VkMemoryRequirements img_req;
        vkGetImageMemoryRequirements(get_device(), texture.image, &img_req);

        add_memory_requirements(mem_req, img_req);
    }

    VkMemoryRequirements buffer_mem_req;

    vkGetBufferMemoryRequirements(get_device(), m_shared_buffer, &buffer_mem_req);

    add_memory_requirements(mem_req, buffer_mem_req);

    return mem_req;
}


VkResult Root::bind_memory()
{
    VkDeviceSize offset(0);

    for(const texture_map_t::ValueType & iter : m_textures)
    {
        const Texture & texture = iter.second;

        VkMemoryRequirements img_req;
        vkGetImageMemoryRequirements(get_device(), texture.image, &img_req);

        offset_match_alignment(offset, img_req.alignment);
        CHECK_VULKAN(vkBindImageMemory(get_device(), texture.image, get_memory(), offset))

        offset += img_req.size;
    }

    for(const texture_map_t::ValueType & iter : m_texts)
    {
        const Texture & texture = iter.second;

        VkMemoryRequirements img_req;
        vkGetImageMemoryRequirements(get_device(), texture.image, &img_req);

        offset_match_alignment(offset, img_req.alignment);

        CHECK_VULKAN(vkBindImageMemory(get_device(), texture.image, get_memory(), offset))

        offset += img_req.size;
    }

    {
        VkMemoryRequirements buffer_mem_req;

        vkGetBufferMemoryRequirements(get_device(), m_shared_buffer, &buffer_mem_req);

        offset_match_alignment(offset, buffer_mem_req.alignment);

        CHECK_VULKAN(vkBindBufferMemory(get_device(), m_shared_buffer, get_memory(), offset))

        offset += buffer_mem_req.size;
    }

    for(Element * elem : m_elements)
    {
        VkMemoryRequirements elem_req = elem->get_memory_requirements();
        offset_match_alignment(offset, elem_req.alignment);

        CHECK_VULKAN(elem->bind_memory(offset))

        offset += elem_req.size;
    }

    return VK_SUCCESS;
}

VkResult Root::create_shared_buffer()
{
    VkDeviceSize buffer_size(reserved_buffer_size);
    for(Element * elem : m_elements)
    {
        buffer_size += elem->get_buffer_size();
    }

    if(buffer_size == 0)
    {
#ifdef DEBUG
        log_message("Warning : Empty buffer for GUI root");
#endif // DEBUG
        return VK_SUCCESS;
    }

    VkBufferCreateInfo buffer_info =
    {
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        nullptr,
        0,
        buffer_size,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr
    };

    CHECK_VULKAN(vkCreateBuffer(get_device(), &buffer_info, nullptr, &m_shared_buffer))

    return VK_SUCCESS;
}

VkResult Root::require_texture(const char* path)
{
    std::pair<texture_map_t::Iterator, bool> result = m_textures.insert(path, {0, 0, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_NULL_HANDLE});

    if(!result.second)
        return VK_SUCCESS;

    /** Modyfing key in map
     * Unsafe, but key comparison behaviour doesn't change here
     * "I know what I'm doing"
     */

    char * str = new char[strlen(path) + 1];
    strcpy(str, path);
    result.first->first = str;

    Texture & texture = result.first->second;

    int w, h;

    if(!stbi_info(path, &w, &h, nullptr))
    {
        log_message("Error stbi_info");
        return VK_ERROR_UNKNOWN;
    }

    texture.width = uint32_t(w);
    texture.height = uint32_t(h);

    VkImageCreateInfo image_info =
    {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        nullptr,
        0,
        VK_IMAGE_TYPE_2D,
        VK_FORMAT_R8G8B8A8_SRGB,
        {uint32_t(w), uint32_t(h), 1},
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        VK_IMAGE_LAYOUT_UNDEFINED
    };

    CHECK_VULKAN(vkCreateImage(get_device(), &image_info, nullptr, &texture.image))

    return VK_SUCCESS;
}

VkResult Root::require_text(const char * text)
{
    std::pair<texture_map_t::Iterator, bool> result = m_texts.insert(text, {0, 0, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_NULL_HANDLE});

    if(!result.second)
        return VK_SUCCESS;

    Texture & texture = result.first->second;

    /** Modyfing key in map
     * Unsafe, but key comparison behaviour doesn't change here
     * "I know what I'm doing"
     */

    char * str = new char[strlen(text) + 1];
    strcpy(str, text);
    result.first->first = str;

    int w, h;

    Graphics::Surface::measure(text, &m_font, m_font_scale, w, h);

    texture.width = w;
    texture.height = h;

    VkImageCreateInfo img_info =
    {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        nullptr,
        0,
        VK_IMAGE_TYPE_2D,
        VK_FORMAT_R8_UNORM,
        {uint32_t(w), uint32_t(h), 1},
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        VK_IMAGE_LAYOUT_UNDEFINED
    };

    CHECK_VULKAN(vkCreateImage(get_device(), &img_info, nullptr, &texture.image))

    return VK_SUCCESS;
}


Element::Element(Element&& from) : INIT_VK_EXCH(m_root), INIT_COPY(m_box), INIT_COPY(m_buffer_offset)
{
    m_root->replace_element(&from, this);
}

Element & Element::operator=(Element && rhs)
{
    CHECK_SELF_ASSIGN;

    get_root()->replace_element(this, &rhs);
    rhs.get_root()->replace_element(&rhs, this);

    ASSIGN_SWAP(m_root);
    ASSIGN_COPY(m_box);
    ASSIGN_COPY(m_buffer_offset);
    return *this;
}

VkResult Element::bind_memory(VkDeviceSize)
{
    return VK_SUCCESS;
}



VkResult Root::create_views()
{
    for(texture_map_t::ValueType & iter : m_textures)
    {
        Texture & texture = iter.second;

        VkImageViewCreateInfo view_info =
        {
            VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            nullptr,
            0,
            texture.image,
            VK_IMAGE_VIEW_TYPE_2D,
            VK_FORMAT_R8G8B8A8_SRGB,
            {VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY},
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
        };

        CHECK_VULKAN(vkCreateImageView(get_device(), &view_info, nullptr, &texture.view))
    }

    for(texture_map_t::ValueType & iter : m_texts)
    {
        Texture & texture = iter.second;

        VkImageViewCreateInfo view_info =
        {
            VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            nullptr,
            0,
            texture.image,
            VK_IMAGE_VIEW_TYPE_2D,
            VK_FORMAT_R8_UNORM,
            {VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_R},
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
        };

        CHECK_VULKAN(vkCreateImageView(get_device(), &view_info, nullptr, &texture.view))
    }

    return VK_SUCCESS;
}

VkResult Root::upload_images(VkCommandBuffer cmd_buf, Vulkan::Buffer & host_buffer)
{
    if(m_textures.get_size() == 0 && m_texts.get_size() == 0)
        return VK_SUCCESS;

    VkDeviceSize upload_size(0);

    Custom::Vector<std::unique_ptr<stbi_uc>> image_datas(m_textures.get_size(), nullptr);
    Custom::Vector<VkExtent2D> image_extents(m_textures.get_size());
    Custom::Vector<Graphics::Surface> text_surfaces(m_texts.get_size());


    size_t index(0);
    for(texture_map_t::ValueType & iter : m_textures)
    {
        image_datas[index] = std::unique_ptr<stbi_uc>(stbi_load(iter.first, reinterpret_cast<int*>(&image_extents[index].width), reinterpret_cast<int*>(&image_extents[index].height), nullptr, 4));
        if(!image_datas[index])
        {
            log_message("Error stbi_load\n");
            return VK_ERROR_UNKNOWN;
        }

        upload_size += image_extents[index].width * image_extents[index].height * 4;

        index++;
    }

    index = 0;
    for(texture_map_t::ValueType & iter : m_texts)
    {
        if(text_surfaces[index].create(iter.first, &m_font, m_font_scale, {0, 0, 0, 0}))
        {
            log_message("Error create surface text : ", SDL_GetError());
            return VK_ERROR_UNKNOWN;
        }

        upload_size += text_surfaces[index].get_surface()->w * text_surfaces[index].get_surface()->h;

        index++;
    }



    CHECK_VULKAN(host_buffer.create(upload_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))

    unsigned char * data;
    size_t data_offset(0);

    CHECK_VULKAN(host_buffer.map_memory(reinterpret_cast<void*&>(data)))

    index = 0;
    for(texture_map_t::ValueType & iter : m_textures)
    {
        memcpy(data + data_offset, image_datas[index].get(), image_extents[index].width * image_extents[index].height * 4);

        VkImageMemoryBarrier barrier =
        {
            VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            nullptr,
            0,
            VK_ACCESS_TRANSFER_WRITE_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            0,
            0,
            iter.second.image,
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
        };

        vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        VkBufferImageCopy region =
        {
            data_offset,
            0,
            0,
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
            {0, 0, 0},
            {image_extents[index].width, image_extents[index].height, 1}
        };

        vkCmdCopyBufferToImage(cmd_buf, host_buffer.get_buffer(), iter.second.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

        barrier.oldLayout = barrier.newLayout;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        barrier.srcAccessMask = barrier.dstAccessMask;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        data_offset += image_extents[index].width * image_extents[index].height * 4;

        index++;
    }

    index = 0;
    for(texture_map_t::ValueType & iter : m_texts)
    {
        SDL_Surface * surface = text_surfaces[index].get_surface();
        for(int line(0); line != surface->h; line++)
        {
            memcpy(data + data_offset + line * surface->w, reinterpret_cast<unsigned char *>(surface->pixels) + line * surface->pitch, surface->w);
        }

        VkImageMemoryBarrier barrier =
        {
            VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            nullptr,
            0,
            VK_ACCESS_TRANSFER_WRITE_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            0,
            0,
            iter.second.image,
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
        };

        vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        VkBufferImageCopy region =
        {
            data_offset,
            0,
            0,
            {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
            {0, 0, 0},
            {uint32_t(surface->w), uint32_t(surface->h), 1}
        };

        vkCmdCopyBufferToImage(cmd_buf, host_buffer.get_buffer(), iter.second.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

        barrier.oldLayout = barrier.newLayout;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        barrier.srcAccessMask = barrier.dstAccessMask;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        data_offset += surface->w * surface->h;

        index++;
    }

    host_buffer.unmap_memory();

    return VK_SUCCESS;
}

VkResult Root::create_descriptor_pool()
{
    uint32_t descr_count(uint32_t(m_textures.get_size() + m_texts.get_size()));

    if(descr_count == 0)
        return VK_SUCCESS;

    for(Element * elem : m_elements)
    {
        descr_count += elem->get_texture_descriptor_set_count();
    }

    std::array<VkDescriptorPoolSize, 1> pool_sizes;
    pool_sizes[0] =
    {
        VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        descr_count
    };

    VkDescriptorPoolCreateInfo pool_info =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        nullptr,
        0,
        descr_count,
        pool_sizes.size(),
        pool_sizes.data()
    };

    CHECK_VULKAN(vkCreateDescriptorPool(get_device(), &pool_info, nullptr, &m_descriptor_pool))

    return VK_SUCCESS;
}

VkResult Label::first_initialization()
{
    CHECK_VULKAN(get_root()->require_text(m_text))
    return VK_SUCCESS;
}

VkResult Label::second_initialization(Vulkan::Buffer&, VkCommandBuffer cmd_buf)
{
    m_text_texture = get_root()->get_text(m_text);

    if(!m_text_texture.is_valid())
    {
        log_message("Error get_root()->get_text(m_text)");
        return VK_ERROR_UNKNOWN;
    }

    {
        float width = float(m_text_texture->width) * get_root()->get_pixel_width();
        float height = float(m_text_texture->height) * get_root()->get_pixel_height();

        float xmin(get_box().x + (get_box().w - width) / 2.0f);
        float xmax(xmin + width);
        float ymin(get_box().y + (get_box().h - height) / 2.0f);
        float ymax(ymin + height);

        TexturedRectangle texture_rect = {{
                {xmin, ymin, 0.0f, 0.0f},
                {xmax, ymin, 1.0f, 0.0f},
                {xmin, ymax, 0.0f, 1.0f},
                {xmax, ymax, 1.0f, 1.0f}
            }
        };

        vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset(), sizeof(TexturedRectangle), &texture_rect);
    }

    return VK_SUCCESS;
}

void Label::update(VkCommandBuffer cmd_buf)
{
    float width = float(m_text_texture->width) * get_root()->get_pixel_width();
    float height = float(m_text_texture->height) * get_root()->get_pixel_height();

    float xmin(get_box().x + (get_box().w - width) / 2.0f);
    float xmax(xmin + width);
    float ymin(get_box().y + (get_box().h - height) / 2.0f);
    float ymax(ymin + height);

    TexturedRectangle texture_rect = {{
            {xmin, ymin, 0.0f, 0.0f},
            {xmax, ymin, 1.0f, 0.0f},
            {xmin, ymax, 0.0f, 1.0f},
            {xmax, ymax, 1.0f, 1.0f}
        }
    };

    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset(), sizeof(TexturedRectangle), &texture_rect);
}


void Label::render(VkCommandBuffer cmd_buf) const
{
    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline());

    get_root()->set_pipeline_viewport_scissor(cmd_buf);


    vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline_layout(), 0, 1, &m_text_texture->descriptor_set, 0, nullptr);

    VkBuffer vertex_buffer(get_root()->get_shared_buffer());
    VkDeviceSize offset(get_buffer_offset());

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &vertex_buffer, &offset);
    vkCmdDraw(cmd_buf, 4, 1, 0, 0);
}

void Root::render(VkCommandBuffer cmd_buf) const
{
    if(m_main_elements_stack.get_size())
        m_main_elements_stack.back()->render(cmd_buf);
}

void Root::compute_pixel_sizes()
{
    m_pixel_height = 2.0f / get_base()->get_swapchain_extent().height;
    m_pixel_width = 2.0f / get_base()->get_swapchain_extent().width;
}

VkResult Root::create_descriptor_sets()
{
    VkDescriptorSetLayout layout = get_texture_pipeline().get_descriptor_set_layouts()[0];

    VkDescriptorSetAllocateInfo alloc_info =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        nullptr,
        m_descriptor_pool,
        1,
        &layout
    };

    VkDescriptorImageInfo img_info =
    {
        VK_NULL_HANDLE,
        VK_NULL_HANDLE,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    };

    VkWriteDescriptorSet write =
    {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        nullptr,
        VK_NULL_HANDLE,
        0,
        0,
        1,
        VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        &img_info,
        nullptr,
        nullptr
    };

    for(texture_map_t::ValueType & value : m_textures)
    {
        CHECK_VULKAN(vkAllocateDescriptorSets(get_device(), &alloc_info, &value.second.descriptor_set))

        write.dstSet = value.second.descriptor_set;
        img_info.imageView = value.second.view;

        vkUpdateDescriptorSets(get_device(), 1, &write, 0, nullptr);
    }

    for(texture_map_t::ValueType & value : m_texts)
    {
        CHECK_VULKAN(vkAllocateDescriptorSets(get_device(), &alloc_info, &value.second.descriptor_set))

        write.dstSet = value.second.descriptor_set;
        img_info.imageView = value.second.view;

        vkUpdateDescriptorSets(get_device(), 1, &write, 0, nullptr);
    }

    return VK_SUCCESS;
}

Root::TextureReference Root::get_texture(const char* path) const
{
    texture_map_t::ConstIterator iter = m_textures.find(path);

    #ifdef DEBUG

    if(iter == m_textures.end())
    {
        log_message("Texture not found :", path);
    }

    #endif // DEBUG

    return (iter != m_textures.end() ? &iter->second : nullptr);
}

Root::TextureReference Root::get_text(const char* text) const
{
    texture_map_t::ConstIterator iter = m_texts.find(text);
    return (iter != m_textures.end() ? &iter->second : nullptr);
}

Label::Label(Label && from) : Element::Element(std::move(from)), INIT_COPY(m_text_texture), INIT_COPY(m_text)
{
}

Label & Label::operator=(Label && rhs)
{
    Element::operator=(std::move(rhs));
    ASSIGN_SWAP(m_text_texture);
    ASSIGN_COPY(m_text);
    return *this;
}

void Root::update_gui(VkCommandBuffer cmd_buf)
{
    for(Element * elem : m_elements)
        elem->update(cmd_buf);
}

Menu::Menu(Root* root, const Rect & box, uint32_t width, uint32_t row_height) : Element(root, box), m_width(width), m_row_height(row_height), m_elements()
{

}

VkResult Menu::second_initialization(Vulkan::Buffer&, VkCommandBuffer cmd_buf)
{
    update(cmd_buf);

    uint32_t indexes[5] = {0, 1, 2, 3, 0};

    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset() + sizeof(BasicRectangle), sizeof(indexes), indexes);

    return VK_SUCCESS;
}

void Menu::update(VkCommandBuffer cmd_buf)
{
    pack();

    float width = float(m_width) * get_root()->get_pixel_width();

    float xmin = (get_box().x + (get_box().w - width) / 2.0f) - 0.5f * get_root()->get_pixel_width();
    float xmax = xmin + width + get_root()->get_pixel_width();

    float height = float(m_elements.get_size() * m_row_height) * get_root()->get_pixel_height();

    float ymin = (get_box().y + (get_box().h - height) / 2.0f) - 0.5f * get_root()->get_pixel_height();
    float ymax = ymin + height + get_root()->get_pixel_height();

    BasicRectangle rect =
    {
        {
            {xmin, ymin},
            {xmax, ymin},
            {xmax, ymax},
            {xmin, ymax}
        }
    };

    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset(), sizeof(rect), &rect);
}

void Menu::render(VkCommandBuffer cmd_buf) const
{
    /** Render background */

    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_fill_pipeline().get_pipeline());

    Color::LinearRGBA color = {1.0f, 1.0f, 1.0f, 1.0f};

    vkCmdPushConstants(cmd_buf, get_root()->get_fill_pipeline().get_pipeline_layout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(color), &color);

    get_root()->set_pipeline_viewport_scissor(cmd_buf);

    VkBuffer vert_buf = get_root()->get_shared_buffer();
    VkDeviceSize offset = get_buffer_offset();

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &vert_buf, &offset);

    vkCmdDraw(cmd_buf, 4, 1, 0, 0);

    /** Render frame */

    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_line_pipeline().get_pipeline());

    color = {0.0f, 0.0f, 0.0f, 1.0f};

    vkCmdPushConstants(cmd_buf, get_root()->get_line_pipeline().get_pipeline_layout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(color), &color);
    vkCmdBindIndexBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset() + sizeof(BasicRectangle), VK_INDEX_TYPE_UINT32);

    vkCmdDrawIndexed(cmd_buf, 5, 1, 0, 0, 0);

    for(Element * elem : m_elements)
        elem->render(cmd_buf);
}

Menu::~Menu()
{
    cleanup();
}

void Menu::cleanup()
{
}

void Menu::pack()
{
    float width = float(m_width) * get_root()->get_pixel_width();
    float height = float(m_row_height) * get_root()->get_pixel_height();

    float x(get_box().x + (get_box().w - width) / 2);
    float y(get_box().y + (get_box().h - height * float(m_elements.get_size())) / 2);

    for(Element * elem : m_elements)
    {
        elem->set_box({x, y, width, height});
        y += height;
    }
}

void Menu::add_element(Element* elem)
{
    m_elements.push_back(elem);
}

Button::Button(Root* root, const char* icon_path, const char* text, const Rect& box) : Element::Element(root, box), m_icon_path(icon_path), m_text(text), m_icon(), m_text_texture()
{

}

Button::Button(Button&& from) : Element::Element(std::move(from)), INIT_COPY(m_icon_path), INIT_COPY(m_text), INIT_MOVE(m_icon), INIT_MOVE(m_text_texture)
{

}

Button & Button::operator=(Button && rhs)
{
    Element::operator=(std::move(rhs));
    ASSIGN_COPY(m_text);
    ASSIGN_COPY(m_icon_path);
    ASSIGN_MOVE(m_icon);
    ASSIGN_MOVE(m_text_texture);
    return *this;
}

VkResult Button::first_initialization()
{
    CHECK_VULKAN(get_root()->require_text(m_text))
    CHECK_VULKAN(get_root()->require_texture(m_icon_path))

    return VK_SUCCESS;
}

VkResult Button::second_initialization(Vulkan::Buffer&, VkCommandBuffer cmd_buf)
{
    m_icon = get_root()->get_texture(m_icon_path);

    if(!m_icon.is_valid())
    {
        log_message("Error get_root()->get_texture(m_icon_path)");
        return VK_ERROR_UNKNOWN;
    }

    m_text_texture = get_root()->get_text(m_text);

    if(!m_text_texture.is_valid())
    {
        log_message("Error get_root()->get_text(m_text)\n");
        return VK_ERROR_UNKNOWN;
    }

    update(cmd_buf);

    return VK_SUCCESS;
}

void Button::update(VkCommandBuffer cmd_buf)
{
    float icon_hgt = float(m_icon->height) * get_root()->get_pixel_height();
    float icon_wdth = float(m_icon->width) * get_root()->get_pixel_width();

    float icon_min_y = get_box().y + (get_box().h - icon_hgt) / 2.0f;
    float icon_max_y = icon_min_y + icon_hgt;

    float icon_max_x = get_box().x + icon_wdth;

    TexturedRectangle icon_rect =
    {
        {
            {get_box().x, icon_min_y, 0.0f, 0.0f },
            {icon_max_x, icon_min_y, 1.0f, 0.0f},
            {get_box().x, icon_max_y, 0.0f, 1.0f},
            {icon_max_x, icon_max_y, 1.0f, 1.0f},
        }
    };

    float text_wdth = float(m_text_texture->width) * get_root()->get_pixel_width();
    float text_hgt = float(m_text_texture->height) * get_root()->get_pixel_height();

    float text_min_x = get_box().x + (get_box().w + icon_wdth - text_wdth) / 2.0f;
    float text_max_x = text_min_x + text_wdth;

    float text_min_y = get_box().y + (get_box().h - text_hgt) / 2.0f;
    float text_max_y = text_min_y + text_hgt;

    TexturedRectangle text_rect =
    {
        {
            {text_min_x, text_min_y, 0.0f, 0.0f},
            {text_max_x, text_min_y, 1.0f, 0.0f},
            {text_min_x, text_max_y, 0.0f, 1.0f},
            {text_max_x, text_max_y, 1.0f, 1.0f}
        }
    };

    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset(), sizeof(TexturedRectangle), &icon_rect);
    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset() + sizeof(TexturedRectangle), sizeof(TexturedRectangle), &text_rect);
}

void Button::render(VkCommandBuffer cmd_buf) const
{
    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline());

    get_root()->set_pipeline_viewport_scissor(cmd_buf);

    vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline_layout(), 0, 1, &m_icon->descriptor_set, 0, nullptr);

    VkBuffer vertex_buf = get_root()->get_shared_buffer();
    VkDeviceSize offset = get_buffer_offset();

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &vertex_buf, &offset);

    vkCmdDraw(cmd_buf, 4, 1, 0, 0);

    vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline_layout(), 0, 1, &m_text_texture->descriptor_set, 0, nullptr);

    vkCmdDraw(cmd_buf, 4, 1, 4, 0);
}

void Root::update_active_element(const SDL_Point& cursor_pos)
{
    m_cursor_pos.x = float(cursor_pos.x) / get_base()->get_swapchain_extent().width * 2.0f - 1.0f;
    m_cursor_pos.y = float(cursor_pos.y) / get_base()->get_swapchain_extent().height * 2.0f - 1.0f;
}

const Element* Menu::get_active_element() const
{
    const Point & mouse_position = get_root()->get_cursor_pos();

    for(Element * elem : m_elements)
    {
        if(point_in_rect(elem->get_box(), mouse_position))
            return elem->get_active_element();
    }

    return nullptr;
}

bool point_in_rect(const Rect& rect, const Point& point)
{
    return point.x >= rect.x && point.x <= rect.x + rect.w && point.y >= rect.y && point.y <= rect.y + rect.h;
}

const Element* Root::get_active_element() const
{
    return m_main_elements_stack.empty() ? nullptr : m_main_elements_stack.back()->get_active_element();
}

void Root::render_active_element_box(VkCommandBuffer cmd_buf) const
{
    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_fill_pipeline().get_pipeline());

    set_pipeline_viewport_scissor(cmd_buf);

    Color::LinearRGBA color = {0.0f, 0.0f, 1.0f, 0.25f};

    vkCmdPushConstants(cmd_buf, get_fill_pipeline().get_pipeline_layout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(color), &color);

    VkDeviceSize offset(0);

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &m_shared_buffer, &offset);

    vkCmdDraw(cmd_buf, 4, 1, 0, 0);
}

void Root::update_active_element_buffer(const Element* active_element, VkCommandBuffer cmd_buf)
{
    const Rect & box = active_element->get_box();

    BasicRectangle rect =
    {
        {
            {box.x, box.y},
            {box.x + box.w, box.y},
            {box.x + box.w, box.y + box.h},
            {box.x, box.y + box.h}
        }
    };

    vkCmdUpdateBuffer(cmd_buf, m_shared_buffer, 0, sizeof(BasicRectangle), &rect);
}

void Root::on_click() const
{
    if(m_main_elements_stack.get_size())
        m_main_elements_stack.back()->on_click();
}

void Element::on_click() const
{

}

void Menu::on_click() const
{
    const Point & clickpos = get_root()->get_cursor_pos();

    for(Element * element : m_elements)
    {
        if(point_in_rect(element->get_box(), clickpos))
        {
            element->on_click();
            return;
        }
    }
}

void QuitButton::on_click() const
{
    Event evnt;
    evnt.type = Event::Type::Quit;
    get_root()->process_event(evnt);
}

void PopButton::on_click() const
{
    Event evnt;
    evnt.type = Event::Type::PopElement;
    get_root()->process_event(evnt);
}

PushButton::PushButton(Root* root, const char* icon_path, const char* text, Element* pushed, const Rect& box) : Button::Button(root, icon_path, text, box), m_pushed(pushed)
{

}

PushButton::PushButton(PushButton&& from) : Button::Button(std::move(from)), INIT_COPY(m_pushed)
{

}

PushButton & PushButton::operator=(PushButton && rhs)
{
    Button::operator=(std::move(rhs));
    ASSIGN_COPY(m_pushed);
    return *this;
}

void PushButton::on_click() const
{
    Event evnt;
    evnt.type = Event::Type::PushElement;
    evnt.pushed_element_event.pushed_element = m_pushed;
    get_root()->process_event(evnt);
}

void FullscreenToggleButton::on_click() const
{
    Event evnt;
    evnt.type = Event::Type::ToggleFullscreen;
    get_root()->process_event(evnt);
}

ResolutionButton::ResolutionButton(Root* root, VkExtent2D resolution, const Rect& box) : Element::Element(root, box), m_resolution(resolution), m_texture()
{

}

ResolutionButton::ResolutionButton(ResolutionButton && from) : Element::Element(std::move(from)), INIT_COPY(m_resolution), INIT_MOVE(m_texture)
{

}

ResolutionButton & ResolutionButton::operator=(ResolutionButton && rhs)
{
    Element::operator=(std::move(rhs));
    ASSIGN_COPY(m_resolution);
    ASSIGN_SWAP(m_texture);
    return *this;
}

VkResult ResolutionButton::first_initialization()
{
    char buffer[256];
    sprintf(buffer, "%ux%u", m_resolution.width, m_resolution.height);
    CHECK_VULKAN(get_root()->require_text(buffer))
    return VK_SUCCESS;
}

VkResult ResolutionButton::second_initialization(Vulkan::Buffer&, VkCommandBuffer cmd_buf)
{
    char buffer[256];
    sprintf(buffer, "%ux%u", m_resolution.width, m_resolution.height);
    m_texture = get_root()->get_text(buffer);
    update(cmd_buf);
    return VK_SUCCESS;
}

void ResolutionButton::update(VkCommandBuffer cmd_buf)
{
    float p_w = get_root()->get_pixel_width();
    float p_h = get_root()->get_pixel_height();

    float x_min = get_box().x + (get_box().w - m_texture->width * p_w) / 2;
    float y_min = get_box().y + (get_box().h - m_texture->height * p_h) / 2;

    TexturedRectangle rect = {{
        {x_min, y_min, 0.0f, 0.0f},
        {x_min + m_texture->width * p_w, y_min, 1.0f, 0.0f},
        {x_min, y_min + m_texture->height * p_h, 0.0f, 1.0f},
        {x_min + m_texture->width * p_w, y_min + m_texture->height * p_h, 1.0f, 1.0f}
    }};

    vkCmdUpdateBuffer(cmd_buf, get_root()->get_shared_buffer(), get_buffer_offset(), sizeof(rect), &rect);
}

void Root::replace_element(Element* to_replace, Element* replacing)
{
    for(Element * & elem : m_elements)
    {
        if(elem == to_replace)
        {
            elem = replacing;
            return;
        }
    }

    log_message("Warning : element at", to_replace, "no found in replace_element");
}

void Root::pop_element()
{
    m_elements.pop_back();
}

void ResolutionButton::render(VkCommandBuffer cmd_buf) const
{
    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline());

    vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, get_root()->get_texture_pipeline().get_pipeline_layout(), 0, 1, &m_texture->descriptor_set, 0, nullptr);

    VkBuffer buf = get_root()->get_shared_buffer();
    VkDeviceSize offset = get_buffer_offset();

    vkCmdBindVertexBuffers(cmd_buf, 0, 1, &buf, &offset);

    vkCmdDraw(cmd_buf, 4, 1, 0, 0);
}

void ResolutionButton::on_click() const
{
    Event evnt;
    evnt.type = Event::Type::SetResolution;
    evnt.set_resolution_event.extent = m_resolution;
    get_root()->process_event(evnt);
}


}
