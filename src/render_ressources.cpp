#include "render_ressources.h"

RenderRessources::RenderRessources(VulkanBase* base) : Vulkan::GraphicObject::GraphicObject(base), INIT_VK_DEF(m_command_pool), INIT_VK_DEF(m_sampler), m_texture_pipeline(base), m_fill_pipeline(base),
    m_line_pipeline(base, &m_fill_pipeline)
{

}

void RenderRessources::cleanup()
{
    if(m_command_pool)
    {
        vkDestroyCommandPool(get_device(), m_command_pool, nullptr);
        ASSIGN_VK_DEF(m_command_pool);
    }
    if(m_sampler)
    {
        vkDestroySampler(get_device(), m_sampler, nullptr);
        ASSIGN_VK_DEF(m_sampler);
    }

    m_texture_pipeline.cleanup();
    m_fill_pipeline.cleanup();
    m_line_pipeline.cleanup();

    Vulkan::GraphicObject::cleanup();
}

RenderRessources::~RenderRessources()
{
    cleanup();
}

RenderRessources::RenderRessources(RenderRessources&& from) : Vulkan::GraphicObject::GraphicObject(std::move(from)), INIT_VK_EXCH(m_command_pool), INIT_VK_EXCH(m_sampler), INIT_MOVE(m_texture_pipeline),
INIT_MOVE(m_fill_pipeline), INIT_MOVE(m_line_pipeline)
{

}

RenderRessources & RenderRessources::operator=(RenderRessources && rhs)
{
    Vulkan::GraphicObject::operator=(std::move(rhs));
    ASSIGN_SWAP(m_command_pool);
    ASSIGN_SWAP(m_sampler);

    ASSIGN_MOVE(m_texture_pipeline);
    ASSIGN_MOVE(m_fill_pipeline);
    ASSIGN_MOVE(m_line_pipeline);

    return *this;
}

VkResult RenderRessources::init_ressources()
{
    CHECK_VULKAN(create_command_pool())
    CHECK_VULKAN(create_sampler())

    m_texture_pipeline.set_sampler(m_sampler);

    CHECK_VULKAN(m_texture_pipeline.init())
    CHECK_VULKAN(m_fill_pipeline.init())
    CHECK_VULKAN(m_line_pipeline.init())

    return VK_SUCCESS;
}

VkResult RenderRessources::create_command_pool()
{
    VkCommandPoolCreateInfo info = {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        nullptr,
        VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
        0
    };

    CHECK_VULKAN(get_base()->get_graphic_index(info.queueFamilyIndex))

    CHECK_VULKAN(vkCreateCommandPool(get_device(), &info, nullptr, &m_command_pool))

    return VK_SUCCESS;
}

VkResult RenderRessources::create_sampler()
{
    VkSamplerCreateInfo info = {
        VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        nullptr,
        0,
        VK_FILTER_LINEAR,
        VK_FILTER_LINEAR,
        VK_SAMPLER_MIPMAP_MODE_NEAREST,
        VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        0.0f,
        VK_FALSE,
        0.0f,
        VK_FALSE,
        VK_COMPARE_OP_ALWAYS,
        0.0f,
        0.0f,
        VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
        VK_FALSE
    };

    CHECK_VULKAN(vkCreateSampler(get_device(), &info, nullptr, &m_sampler))

    return VK_SUCCESS;
}

