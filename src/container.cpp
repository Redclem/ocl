#include "container.h"

Container::Container(Container&& from) : INIT_COPY(m_graphic), INIT_MOVE(m_phases), INIT_COPY(m_position)
{

}

Container::Container(const Container& from) : INIT_COPY(m_graphic), INIT_COPY(m_phases), INIT_COPY(m_position)
{

}
Container& Container::operator=(Container&& rhs)
{
    ASSIGN_COPY(m_graphic);
    ASSIGN_MOVE(m_phases);
    ASSIGN_COPY(m_position);
    return *this;
}

Container& Container::operator=(const Container& rhs)
{
    ASSIGN_COPY(m_graphic);
    ASSIGN_COPY(m_phases);
    ASSIGN_COPY(m_position);
    return *this;
}

Container::Container(const ContainerGraphic* graphic) : m_graphic(graphic), m_phases(), m_position()
{

}

Container::~Container()
{

}
