#include "container_graphics.h"


ContainerGraphicsManager::ContainerGraphicsManager(VulkanBase* vulkan_base) : Buffer(vulkan_base), m_graphics()
{

}

ContainerGraphicsManager::~ContainerGraphicsManager()
{
    cleanup();
}

ContainerGraphicsManager::ContainerGraphicsManager(ContainerGraphicsManager&& from) : Buffer(std::move(from)), INIT_MOVE(m_graphics)
{

}

ContainerGraphicsManager& ContainerGraphicsManager::operator=(ContainerGraphicsManager&& rhs)
{
    Buffer::operator=(std::move(rhs));
    ASSIGN_MOVE(m_graphics);
    return *this;
}

ContainerGraphicsManager::ContainerGraphic::ContainerGraphic() : m_info(), m_buffer_offset(0)
{

}

ContainerGraphicsManager::ContainerGraphic::~ContainerGraphic()
{

}

VkDeviceSize ContainerGraphicsManager::ContainerGraphic::get_required_buffer_size() const
{
    return
        m_info.get_vertices().get_byte_size() +
        m_info.get_outside_indexes().get_byte_size() +
        m_info.get_inside_indexes().get_byte_size();
}

ContainerGraphicsManager::ContainerGraphic::ContainerGraphic(ContainerGraphic&& from) : INIT_MOVE(m_info), INIT_COPY(m_buffer_offset)
{

}

ContainerGraphicsManager::ContainerGraphic::ContainerGraphic(const ContainerGraphic& from) : INIT_COPY(m_info), INIT_COPY(m_buffer_offset)
{

}

ContainerGraphicsManager::ContainerGraphic& ContainerGraphicsManager::ContainerGraphic::operator=(ContainerGraphicsManager::ContainerGraphic&& rhs)
{
    ASSIGN_MOVE(m_info);
    ASSIGN_COPY(m_buffer_offset);
    return *this;
}

ContainerGraphicsManager::ContainerGraphic& ContainerGraphicsManager::ContainerGraphic::operator=(const ContainerGraphicsManager::ContainerGraphic& rhs)
{
    ASSIGN_COPY(m_info);
    ASSIGN_COPY(m_buffer_offset);
    return *this;
}

VkResult ContainerGraphicsManager::create_buffer(VkCommandPool command_pool)
{
    if(m_graphics.get_size() == 0)
    {
        log_message("warning : container graphics manager created with no graphics");
        return VK_SUCCESS;
    }

    VkDeviceSize buffer_size(0);

    for(auto & graphic : m_graphics)
    {
        graphic.set_offset(buffer_size);
        buffer_size += graphic.get_required_buffer_size();
    }

    Vulkan::Buffer cpu_buffer(get_base());

    CHECK_VULKAN(cpu_buffer.create(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))

    void * data;

    CHECK_VULKAN(cpu_buffer.map_memory(data))


    for(ContainerGraphic & graphic : m_graphics)
        graphic.write_data(data);

    cpu_buffer.unmap_memory();

    CHECK_VULKAN(create(buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT))

    Vulkan::CommandBuffer cmd_buf(get_base());

    CHECK_VULKAN(cmd_buf.allocate(VK_COMMAND_BUFFER_LEVEL_PRIMARY, command_pool))

    CHECK_VULKAN(cmd_buf.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT))

    VkBufferCopy copy = {0, 0, buffer_size};

    vkCmdCopyBuffer(cmd_buf.get_command_buffer(), cpu_buffer.get_buffer(), get_buffer(), 1, &copy);

    CHECK_VULKAN(cmd_buf.end())

    Vulkan::Fence fence(get_base());

    CHECK_VULKAN(fence.create())

    VkCommandBuffer cmd_hdl = cmd_buf.get_command_buffer();

    VkSubmitInfo sub_info =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        0,
        nullptr,
        0,
        1,
        &cmd_hdl,
        0,
        nullptr
    };

    CHECK_VULKAN(vkQueueSubmit(get_base()->get_graphics_queue(), 1, &sub_info, fence.get_fence()))

    CHECK_VULKAN(fence.wait())

    return VK_SUCCESS;
}

void ContainerGraphicsManager::ContainerGraphic::write_data(void * location) const
{
    unsigned char * data = reinterpret_cast<unsigned char *>(location) + m_buffer_offset;
    m_info.get_vertices().copy_to(data);
    data += m_info.get_vertices().get_byte_size();
    m_info.get_outside_indexes().copy_to(data);
    data += m_info.get_outside_indexes().get_byte_size();
    m_info.get_inside_indexes().copy_to(data);
}

bool ContainerGraphicsManager::load_containers(const Custom::Vector<const char*>& files)
{
    if(m_graphics.get_size())
    {
        log_message("error : attempt to load containers when already loaded");
        return false;
    }

    m_graphics.resize(files.get_size());

    for(size_t cont(0); cont != m_graphics.get_size(); cont++)
    {
        std::ifstream file(files[cont]);

        if(!m_graphics[cont].load(file))
            return false;
    }

    return true;
}


