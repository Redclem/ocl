#include "warning.h"
#include "lab.h"

#if defined(__WIN32) || defined(WIN32) || defined(__WIN32__)

#ifdef RELEASE
DISABLE_WARNINGS
#include <SDL_main.h>
ENABLE_WARNINGS
#elif DEBUG
#undef main
#endif // RELEASE

#endif // defined

#include <iostream>

int main(int, char *[])
{
    Lab lab;

    if(!lab.init())
    {
        log_message("Error init lab");
        return 1;
    }

    lab.run();

    return 0;
}
