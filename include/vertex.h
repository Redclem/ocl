#ifndef VERTEX_INCLUDED
#define VERTEX_INCLUDED

namespace Vertex
{

struct Textured
{
    float x;
    float y;
    float u;
    float v;
};

struct Basic
{
    float x;
    float y;
};

}

#endif // VERTEX_INCLUDED
