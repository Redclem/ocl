#ifndef CONTAINER_GRAPHICS_H_INCLUDED
#define CONTAINER_GRAPHICS_H_INCLUDED

#include "container_info.h"
#include "VulkanClasses.h"
#include "VulkanBase.h"
#include "copy_move_macros.h"
#include "custom.hpp"
#include "pipeline.h"

#include <vulkan/vulkan.h>

#include <set>
#include <cstring>
#include <iostream>
#include <cassert>

class ContainerGraphicsManager : private Vulkan::Buffer
{
public:

    /** Default constructor */
    ContainerGraphicsManager(VulkanBase * vulkan_base);

    /** Default destructor */
    virtual ~ContainerGraphicsManager() override;

    DECL_MOVE(ContainerGraphicsManager)
    BAN_COPY(ContainerGraphicsManager)

    using Vulkan::Buffer::get_buffer;

    class ContainerGraphic
    {
    public:
        /** Default constructor */
        ContainerGraphic();

        /** Default destructor */
        ~ContainerGraphic();

        /** Set offset in the shared buffer
         * \param offset The offset in the shared buffer
         */
        void set_offset(VkDeviceSize offset) {m_buffer_offset = offset;}

        /** Get offset in the shared buffer
         * \return The offset in the shared buffer
         */
        VkDeviceSize get_offset() const {return m_buffer_offset;}

        /** Get required buffer space
         * \return The required buffer size
         */
        VkDeviceSize get_required_buffer_size() const;

        DECL_MOVE(ContainerGraphic)
        DECL_COPY(ContainerGraphic)

        /** Write data to buffer
         * \param location A pointer to the region recieving the data (buffer offset applied)
         */
        void write_data(void * location) const;

        /** Load from stream
         * \param stream The stream to load from
         * \return true on success, false on failure
         */
        bool load(std::istream & stream)
        {
            return m_info.load(stream);
        }

        /** Display container info to stream
         * \param stream The stream to use to display
         */
        void display(std::ostream & stream) const
        {
            m_info.display(stream);
        }

        /** Get offset of the indexes in the container's data
         * \return The offset of the indexes in the container's data
         */
        VkDeviceSize get_outside_index_offset() const {return m_info.get_vertices().get_byte_size();}

        /** Get the number of outside indexes
         * \return The number of indexes for the draw of the outside
         */
        uint32_t get_outside_index_count() const {return m_info.get_outside_indexes().get_size();}

        /** Get offset of the indexes in the container's data
         * \return The offset of the indexes in the container's data
         */
        VkDeviceSize get_inside_index_offset() const {return m_info.get_vertices().get_byte_size() + m_info.get_outside_indexes().get_byte_size();}

        /** Get the number of outside indexes
         * \return The number of indexes for the draw of the outside
         */
        uint32_t get_inside_index_count() const {return m_info.get_inside_indexes().get_size();}

        /** Get container info
         * \return A const reference to the container info
         */
        const ContainerInfo & get_info() const {return m_info;}

    private:
        ContainerInfo m_info; /**< Member variable m_info : The container's info */
        VkDeviceSize m_buffer_offset; /**< Member variable m_buffer_offset : The offset of the container's data in the shared buffer */
    };

    /** Create buffer for container graphics and upload data
     * \param command_pool The command pool to allocate a command buffer from
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_buffer(VkCommandPool command_pool);

    /** Load containers from files
     * \param files A vector containing the paths to the files
     * \return true on success, false on failure
     */
    bool load_containers(const Custom::Vector<const char *> & files);

    using Buffer::cleanup;

    /** Get graphics at given index
     * \param index The index of the graphics
     * \return The graphics at given index
     */
    const ContainerGraphic & at(size_t index) const {return m_graphics[index];}

    /** Get graphics count
     * \return The number of graphics in the manager
     */
    size_t get_graphics_count() const {return m_graphics.get_size();}

private:
    Custom::Vector<ContainerGraphic> m_graphics; /**< Member variable m_graphics : The vector containing container graphics */
};

#endif // CONTAINER_GRAPHICS_H_INCLUDED
