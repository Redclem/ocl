#ifndef CONTAINER_H_INCLUDED
#define CONTAINER_H_INCLUDED

#include "container_graphics.h"
#include "copy_move_macros.h"
#include "chemistry.h"
#include "VulkanClasses.h"
#include "custom.hpp"
#include "error_macro.h"
#include "color.h"

#include <vulkan/vulkan.h>

#include <vector>

class Container
{
public:
    using ContainerGraphic = ContainerGraphicsManager::ContainerGraphic;
    using PhasePtr = Chemistry::PhasePtr;

    DECL_MOVE(Container)
    DECL_COPY(Container)

    /** Constructor
     * \param graphic A pointer to the ContainerGraphic representing the container (defaults to nullptr)
     */
    Container(const ContainerGraphic * graphic = nullptr);

    /** Destructor */
    ~Container();

    /** Set graphic representation of the container
     * \param graphic A pointer to the ContainerGraphic representing the container
     */
    void set_graphic(const ContainerGraphic * graphic) {m_graphic = graphic;}

    /** Add a phase to the container
     * \param phase The phase to be added
     */
    void add_phase(const PhasePtr & phase)
    {
        m_phases.push_back(phase);
    }
    /** Add a phase to the container
     * \param phase The phase to be added
     */
    void add_phase(PhasePtr && phase)
    {
        m_phases.push_back(std::move(phase));
    }

    /** Access phase at given index
     * \return The phase at given index
     */
    const PhasePtr & phase_at(size_t index) const {return m_phases[index];}

    /** Access phase at given index
     * \return The phase at given index
     */
    PhasePtr & phase_at(size_t index) {return m_phases[index];}

    /** Get phase count
     * \return The number of phases in the container
     */
    size_t get_phase_count() const {return m_phases.size();}

    /** Get position
     * \return A const reference to the position vector
     */
    const Vec::Vec & get_position() const {return m_position;}

    /** Get phases
     * \return A const references to the phase vector
     */
    const std::vector<PhasePtr> & get_phases() const {return m_phases;}

    /** Get container graphic
     * \return A pointer to the container graphic
     */
    const ContainerGraphic * get_graphic() const {return m_graphic;}

private:
    const ContainerGraphic * m_graphic; /** The graphic representation of the container */
    std::vector<PhasePtr> m_phases; /** The phases contained in the container */
    Vec::Vec m_position; /** The container's position */
};

#endif // CONTAINER_H_INCLUDED
