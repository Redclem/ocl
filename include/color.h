#ifndef COLOR_INCLUDED
#define COLOR_INCLUDED

namespace Color
{
    struct LinearRGBA
    {
        float r;
        float g;
        float b;
        float a;
    };
}

#endif // COLOR_INCLUDED
