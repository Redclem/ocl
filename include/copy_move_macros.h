#ifndef COPY_MOVE_MACROS_H_INCLUDED
#define COPY_MOVE_MACROS_H_INCLUDED

#define BAN_COPY(type) type(const type & from) = delete;\
\
type & operator=(const type & rhs) = delete;

#define BAN_MOVE(type) type(type && from) = delete;\
\
type & operator=(type && rhs) = delete;

#define DECL_MOVE(type) type(type && from);\
\
type & operator=(type && rhs);

#define DECL_COPY(type) type(const type & from);\
\
type & operator=(const type & rhs);

#endif // COPY_MOVE_MACROS_H_INCLUDED
