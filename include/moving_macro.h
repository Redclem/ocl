#ifndef MOVING_MACRO_H_INCLUDED
#define MOVING_MACRO_H_INCLUDED

#include <utility>

#define INIT_EXCHANGE(member, value) member(std::exchange<decltype(member), decltype(member)>(from.member, value))
#define INIT_MOVE(member) member(std::move(from.member))
#define INIT_COPY(member) member(from.member)

#define ASSIGN_MOVE(member) member = std::move(rhs.member)
#define ASSIGN_SWAP(member) std::swap(member, rhs.member)
#define ASSIGN_COPY(member) member = rhs.member

#define CHECK_SELF_ASSIGN if(&rhs == this) return *this

#endif // MOVING_MACRO_H_INCLUDED
