#ifndef BASEPIPELINE_H
#define BASEPIPELINE_H

#include "VulkanClasses.h"
#include "VulkanBase.h"
#include "vulkan_macro.h"
#include "moving_macro.h"
#include "copy_move_macros.h"
#include "custom.hpp"
#include "vertex.h"
#include "color.h"

#include <vulkan/vulkan.h>

#include <fstream>
#include <type_traits>
#include <cstddef>

namespace Vulkan
{

namespace Pipeline
{

class Base : public Vulkan::GraphicObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    Base(VulkanBase * base);

    /** Get pipeline
     * \return The pipeline
     */
    VkPipeline get_pipeline() const
    {
        return m_pipeline;
    }

    virtual void cleanup() override;

    BAN_COPY(Base)
    DECL_MOVE(Base)

    /** Initialize pipeline
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult init() = 0;

    /** Load shader module
     * \param path The path to the shader module
     * \param module A VkShaderModule receiving a handle to the shader module
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult load_shader(const char * path, VkShaderModule & module);

protected:

    /** Create pipeline
     * \param info The vkGraphicsPipelineCreateInfo to use to create the pipeline
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create(const VkGraphicsPipelineCreateInfo & info)
    {
        return vkCreateGraphicsPipelines(get_device(), get_base()->get_pipeline_cache(), 1, &info, nullptr, &m_pipeline);
    }

private:
    VkPipeline m_pipeline;
};

class Modular : public Base
{
public:
    using Base::Base;
    using Base::operator=;

    virtual VkResult init();
    virtual VkResult create();

    /** Get vertex shader
     * \return The vertex shader
     */
    virtual VkShaderModule get_vertex_shader() const = 0;

    /** Get fragment shader
     * \return The fragment shader
     */
    virtual VkShaderModule get_fragment_shader() const = 0;

    /** Get pipeline layout
     * \return The pipeline layout
     */
    virtual VkPipelineLayout get_pipeline_layout() const = 0;

    /** Get vertex input bindings descriptions
     * \return The vertex input bindings descriptions
     */
    virtual Custom::Vector<VkVertexInputBindingDescription> get_vertex_input_bindings_descriptions() const = 0;

    /** Get vertex input attriutes descriptions
     * \return The vertex input attriutes descriptions
     */
    virtual Custom::Vector<VkVertexInputAttributeDescription> get_vertex_input_attributes_descriptions() const = 0;

    /** Get the primitive topology
     * \return The primitive topology
     */
    virtual VkPrimitiveTopology get_primitive_topology() const = 0;

    /** Has primitive restart
     * \return VK_TRUE if primitive restart present, VK_FALSE if absent
     */
    virtual VkBool32 has_primitive_restart() const
    {
        return VK_FALSE;
    }

    /** Get viewports
     * \return The viewports
     */
    virtual Custom::Vector<VkViewport> get_viewports() const
    {
        return { {} };
    }

    /** Get scissors
     * \return The scissors
     */
    virtual Custom::Vector<VkRect2D> get_scissors() const
    {
        return { {} };
    }

    /** Has depth clamp
     * \return VK_TRUE if depth clamping present, VK_FALSE if absent
     */
    virtual VkBool32 has_depth_clamp() const
    {
        return VK_FALSE;
    }

    /** Get polygon mode
     * \return The polygon mode
     */
    virtual VkPolygonMode get_polygon_mode() const
    {
        return VK_POLYGON_MODE_FILL;
    }

    /** Get the cull mode
     * \return The cull mode
     */
    virtual VkCullModeFlags get_cull_mode() const
    {
        return VK_CULL_MODE_NONE;
    }

    /** Get the front face
     * \return The front face
     */
    virtual VkFrontFace get_front_face() const
    {
        return VK_FRONT_FACE_COUNTER_CLOCKWISE;
    }

    /** Get the rasterization samples
     * \return The rasterization samples
     */
    virtual VkSampleCountFlagBits get_rasterization_samples() const
    {
        return VK_SAMPLE_COUNT_1_BIT;
    }

    /** Get dynamic states
     * \return The dynamic states
     */
    virtual Custom::Vector<VkDynamicState> get_dynamic_states() const
    {
        return {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
    }
};

template<uint32_t descriptor_set_count>
class Complete : public Modular
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    Complete(VulkanBase * base) : Modular::Modular(base), INIT_VK_DEF(m_vertex_shader), INIT_VK_DEF(m_fragment_shader), m_descriptor_set_layouts{}, INIT_VK_DEF(m_pipeline_layout) {}

    VkShaderModule get_vertex_shader() const
    {
        return m_vertex_shader;
    }
    VkShaderModule get_fragment_shader() const
    {
        return m_fragment_shader;
    }

    VkPipelineLayout get_pipeline_layout() const
    {
        return m_pipeline_layout;
    }

    /** Get descriptor set layouts
     * \return The descriptor set layouts
     */
    const std::array<VkDescriptorSetLayout, descriptor_set_count> & get_descriptor_set_layouts() const
    {
        return m_descriptor_set_layouts;
    }

    /** Get vertex shader path
     * \return The vertex shader path
     */
    virtual const char * get_vertex_shader_path() const = 0;

    /** Get fragment shader path
     * \return The fragment shader path
     */
    virtual const char * get_fragment_shader_path() const = 0;

    /** Create shader modules
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult create_shader_modules()
    {
        CHECK_VULKAN(load_shader(get_vertex_shader_path(), m_vertex_shader))
        CHECK_VULKAN(load_shader(get_fragment_shader_path(), m_fragment_shader))
        return VK_SUCCESS;
    }

    /** Create descriptor set layouts
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult create_descriptor_set_layouts()
    {
        std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, descriptor_set_count> bindings = get_descriptor_set_layouts_bindings();
        std::array<VkDescriptorSetLayoutCreateFlags, descriptor_set_count> flags = get_descriptor_set_layouts_flags();
        for(uint32_t set(0); set != descriptor_set_count; set++)
        {
            VkDescriptorSetLayoutCreateInfo layout_info =
            {
                VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                nullptr,
                flags[set],
                uint32_t(bindings[set].get_size()),
                bindings[set].get_data()
            };

            CHECK_VULKAN(vkCreateDescriptorSetLayout(get_device(), &layout_info, nullptr, m_descriptor_set_layouts.data() + set))
        }
        return VK_SUCCESS;
    }

    /** Create pipeline layout
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult create_pipeline_layout()
    {
        Custom::Vector<VkPushConstantRange> ranges = get_push_constant_ranges();
        VkPipelineLayoutCreateInfo layout_info =
        {
            VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            nullptr,
            0,
            descriptor_set_count,
            m_descriptor_set_layouts.data(),
            uint32_t(ranges.get_size()),
            ranges.get_data()
        };

        CHECK_VULKAN(vkCreatePipelineLayout(get_device(), &layout_info, nullptr, &m_pipeline_layout))

        return VK_SUCCESS;
    }

    /** Get push constant ranges
     * \return The push constant ranges
     */
    virtual Custom::Vector<VkPushConstantRange> get_push_constant_ranges() const
    {
        return {};
    }

    /** Get descriptor set layouts bindings
     * \return The descriptor set layouts bindings
     */
    virtual std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, descriptor_set_count> get_descriptor_set_layouts_bindings() const = 0;

    /** Get descriptor set layouts flags
     * \return The descriptor set layouts flags
     */
    virtual std::array<VkDescriptorSetLayoutCreateFlags, descriptor_set_count> get_descriptor_set_layouts_flags() const
    {
        return {};
    }

    virtual void cleanup() override
    {
        Modular::cleanup();

        if(m_vertex_shader)
        {
            vkDestroyShaderModule(get_device(), m_vertex_shader, nullptr);
            ASSIGN_VK_DEF(m_vertex_shader);
        }

        if(m_fragment_shader)
        {
            vkDestroyShaderModule(get_device(), m_fragment_shader, nullptr);
            ASSIGN_VK_DEF(m_fragment_shader);
        }

        if(m_pipeline_layout)
        {
            vkDestroyPipelineLayout(get_device(), m_pipeline_layout, nullptr);
            ASSIGN_VK_DEF(m_pipeline_layout);
        }

        for(auto & descr : m_descriptor_set_layouts)
        {
            if(descr)
            {
                vkDestroyDescriptorSetLayout(get_device(), descr, nullptr);
                descr = VK_NULL_HANDLE;
            }
        }
    }

    /** Default destructor */
    virtual ~Complete() override
    {
        cleanup();
    }

    virtual VkResult init() override
    {
        CHECK_VULKAN(create_shader_modules())
        CHECK_VULKAN(create_descriptor_set_layouts())
        CHECK_VULKAN(create_pipeline_layout())
        CHECK_VULKAN(Modular::init())
        return VK_SUCCESS;
    }

    /** Move constructor
     * \param from The pipeline to move
     */
    Complete(Complete && from) : Modular::Modular(std::move(from)), INIT_VK_EXCH(m_vertex_shader), INIT_VK_EXCH(m_fragment_shader), INIT_EXCHANGE(m_descriptor_set_layouts, {}), INIT_VK_EXCH(m_pipeline_layout) {}

    /** Move assignement operator
     * \param rhs The pipeline to move
     * \return Itself after assignement
     */
    Complete & operator=(Complete && rhs)
    {
        Modular::operator=(std::move(rhs));
        ASSIGN_SWAP(m_vertex_shader);
        ASSIGN_SWAP(m_fragment_shader);
        ASSIGN_SWAP(m_descriptor_set_layouts);
        ASSIGN_SWAP(m_pipeline_layout);
        return *this;
    }

    BAN_COPY(Complete)

protected:
    /** Create pipeline layout with info
     * \param info The info used to create the pipeline layout
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_pipeline_layout(const VkPipelineLayoutCreateInfo * info)
    {
        return vkCreatePipelineLayout(get_device(), info, nullptr, &m_pipeline_layout);
    }

private:
    VkShaderModule m_vertex_shader;
    VkShaderModule m_fragment_shader;
    std::array<VkDescriptorSetLayout, descriptor_set_count> m_descriptor_set_layouts;
    VkPipelineLayout m_pipeline_layout;
};

class Texture : public Complete<1>
{
public:

    /** Default constructor
     * \param base The VulkanBase to use
     */
    Texture(VulkanBase * base);

    DECL_MOVE(Texture)
    BAN_COPY(Texture)

    virtual Custom::Vector<VkVertexInputBindingDescription> get_vertex_input_bindings_descriptions() const override;
    virtual Custom::Vector<VkVertexInputAttributeDescription> get_vertex_input_attributes_descriptions() const override;

    virtual VkPrimitiveTopology get_primitive_topology() const override
    {
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    }
    virtual const char * get_vertex_shader_path() const override
    {
        return "shaders/texture_vert.spv";
    }
    virtual const char * get_fragment_shader_path() const override
    {
        return "shaders/texture_frag.spv";
    }

    virtual std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, 1> get_descriptor_set_layouts_bindings() const override;

    /** Set sampler
     * \param sampler The sampler
     */
    void set_sampler(VkSampler sampler)
    {
        m_sampler = sampler;
    }
private:
    VkSampler m_sampler;
};

class Fill : public Complete<0>
{
public:

    /** Default constructor
     * \param base The VulkanBase to use
     */
    Fill(VulkanBase * base) : Complete<0>::Complete(base) {}

    virtual Custom::Vector<VkVertexInputBindingDescription> get_vertex_input_bindings_descriptions() const override;
    virtual Custom::Vector<VkVertexInputAttributeDescription> get_vertex_input_attributes_descriptions() const override;

    virtual VkPrimitiveTopology get_primitive_topology() const override
    {
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
    }
    virtual const char * get_vertex_shader_path() const override
    {
        return "shaders/fill_vert.spv";
    }
    virtual const char * get_fragment_shader_path() const override
    {
        return "shaders/fill_frag.spv";
    }

    virtual Custom::Vector<VkPushConstantRange> get_push_constant_ranges() const override;
    virtual std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, 0> get_descriptor_set_layouts_bindings() const override
    {
        return {};
    }
};

class Container : public Complete<0>
{
public:
    using Complete<0>::Complete;
    using Complete<0>::operator=;

    virtual Custom::Vector<VkVertexInputBindingDescription> get_vertex_input_bindings_descriptions() const override;
    virtual Custom::Vector<VkVertexInputAttributeDescription> get_vertex_input_attributes_descriptions() const override;

    virtual VkPrimitiveTopology get_primitive_topology() const override
    {
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    }

    virtual VkBool32 has_primitive_restart() const override
    {
        return VK_TRUE;
    }

    virtual const char * get_vertex_shader_path() const override
    {
        return "shaders/container_vert.spv";
    }

    virtual const char * get_fragment_shader_path() const override
    {
        return "shaders/container_frag.spv";
    }

    virtual Custom::Vector<VkPushConstantRange> get_push_constant_ranges() const override;

    virtual std::array<Custom::Vector<VkDescriptorSetLayoutBinding>, 0> get_descriptor_set_layouts_bindings() const override {return {};}
};

class Line : public Modular
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     * \param parent The Fill pipeline to use as a parent
     */
    Line(VulkanBase * base, const Fill * parent) : Modular(base), m_parent(parent) {}

    virtual Custom::Vector<VkVertexInputBindingDescription> get_vertex_input_bindings_descriptions() const override
    {
        return m_parent->get_vertex_input_bindings_descriptions();
    }
    virtual Custom::Vector<VkVertexInputAttributeDescription> get_vertex_input_attributes_descriptions() const override
    {
        return m_parent->get_vertex_input_attributes_descriptions();
    }

    virtual VkPrimitiveTopology get_primitive_topology() const override
    {
        return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
    }

    virtual VkShaderModule get_vertex_shader() const override
    {
        return m_parent->get_vertex_shader();
    }
    virtual VkShaderModule get_fragment_shader() const override
    {
        return m_parent->get_fragment_shader();
    }

    virtual VkPipelineLayout get_pipeline_layout() const override
    {
        return m_parent->get_pipeline_layout();
    }
    virtual Custom::Vector<VkDynamicState> get_dynamic_states() const override
    {
        return {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
    }

    DECL_MOVE(Line)
    BAN_COPY(Line)

private:
    const Fill * m_parent;
};

}

}


#endif // BASEPIPELINE_H
