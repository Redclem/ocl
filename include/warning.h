#ifndef WARNING_H_INCLUDED
#define WARNING_H_INCLUDED

#ifdef __GNUC__

#define DISABLE_WARNINGS _Pragma("GCC diagnostic push") \
_Pragma("GCC diagnostic ignored \"-Wall\"")\
_Pragma("GCC diagnostic ignored \"-Wsign-compare\"")\
_Pragma("GCC diagnostic ignored \"-Wmissing-declarations\"")\
_Pragma("GCC diagnostic ignored \"-Wfloat-equal\"")\
_Pragma("GCC diagnostic ignored \"-Wunused-but-set-variable\"")\
_Pragma("GCC diagnostic ignored \"-Winline\"")

#define ENABLE_WARNINGS _Pragma("GCC diagnostic pop")

#else
#define DISABLE_WARNINGS __pragma(clang diagnostic push) \
__pragma(clang diagnostic ignored "-Weverything")

#define ENABLE_WARNINGS __pragma(clang diagnostic pop)
#endif // GCC


#endif // WARNING_H_INCLUDED
