#ifndef VULKANCLASSES_INCLUDED
#define VULKANCLASSES_INCLUDED

#include "vulkan_macro.h"
#include "VulkanBase.h"
#include "copy_move_macros.h"
#include "moving_macro.h"

#include <vulkan/vulkan.h>

namespace Vulkan
{

/** Base class for any graphic object */
class GraphicObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    GraphicObject(VulkanBase * base);

    /** Default destructor */
    virtual ~GraphicObject();

    /** Get vulkan device of the base
     * \return The vulkan device of the base
     */
    VkDevice get_device() const {return m_base->get_device();}

    /** Get the base
     * \return The base
     */
    VulkanBase * get_base() const {return m_base;}

    BAN_COPY(GraphicObject)

    DECL_MOVE(GraphicObject)

    /** Cleanup ressources */
    virtual void cleanup() {}

private:
    VulkanBase * m_base;
};

class MemoryObject : public GraphicObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    MemoryObject(VulkanBase * base);

    virtual void cleanup() override;

    BAN_COPY(MemoryObject)

    DECL_MOVE(MemoryObject)

    /** Get object memory
     * \return The object's memory
     */
    VkDeviceMemory get_memory() const {return m_memory;}

    /** Get object memory requirements
     * \return The object's memory requirements
     */
    virtual VkMemoryRequirements get_memory_requirements() const = 0;

    /** Map memory to host memory (Requires memory property VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
     * \param data A variable receiving the position of the memory
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult map_memory(void * & data);

    /** Flush and unmap memory
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult flush_and_unmap_memory();

    /** Unmap memory without flushing */
    void unmap_memory()
    {
        vkUnmapMemory(get_device(), m_memory);
    }

    /** Choose memory type
     * \param requirements The memory requirements of the object using the memory
     * \param mem_props The properties of the memory to allocate
     * \return The index of the chosen memory type, or VulkanBase::no_type if no type is found
     */
    uint32_t choose_memory_type(const VkMemoryRequirements & requirements, VkMemoryPropertyFlags mem_props) {return get_base()->choose_memory_type(requirements, mem_props);}

    /** Add memory requirements
     * \param req0 The VkMemoryRequirements to wich the second will be added
     * \param req1 The VkMemoryRequirements to add to the first
     */
    static void add_memory_requirements(VkMemoryRequirements & req0, const VkMemoryRequirements & req1);

    /** Make an offset match an alignment
     * \param offset The offset that will be modified to match the alignment
     * \param alignment The alignment that the offset must match
     */
    static void offset_match_alignment(VkDeviceSize & offset, VkDeviceSize alignment);

protected:

    /** Allocate and bind object's memory
     * \param props The memory property flags
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult allocate_memory(VkMemoryPropertyFlags props);

    /** Bind memory to the object
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult bind_memory() = 0;

private:
    VkDeviceMemory m_memory;
};

class Buffer : public MemoryObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    Buffer(VulkanBase * base);

    ~Buffer()
    {
        cleanup();
    }

    virtual void cleanup();

    BAN_COPY(Buffer)
    DECL_MOVE(Buffer)

    /** Get buffer
     * \return The buffer
     */
    VkBuffer get_buffer() const {return m_buffer;}

    /** Create buffer
     * \param size The buffer size
     * \param usage The buffer usage flags
     * \param memory_properties The properties of the memory bound to the buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memory_properties);

    virtual VkMemoryRequirements get_memory_requirements() const override;

    virtual VkResult bind_memory() override;

private:
    VkBuffer m_buffer;
};

class CommandBuffer : public GraphicObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    CommandBuffer(VulkanBase * base);

    virtual void cleanup();

    BAN_COPY(CommandBuffer)
    DECL_MOVE(CommandBuffer)

    /** Get command buffer
     * \return The command buffer
     */
    VkCommandBuffer get_command_buffer() const {return m_command_buffer;}

    /** Allocate command buffer
     * \param level The command buffer level
     * \param pool The command pool to allocate from
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult allocate(VkCommandBufferLevel level, VkCommandPool pool);

    /** Begin command buffer
     * \param usage The command buffer usage flags
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult begin(VkCommandBufferUsageFlags usage);

    /** End command buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult end();

private:
    VkCommandBuffer m_command_buffer;
    VkCommandPool m_command_pool;
};

class Fence : public GraphicObject
{
public:
    /** Default constructor
     * \param base The VulkanBase to use
     */
    Fence(VulkanBase * base);

    virtual ~Fence()
    {
        cleanup();
    }

    BAN_COPY(Fence)
    DECL_MOVE(Fence)

    virtual void cleanup();

    /** Get fence
     * \return The fence
     */
    VkFence get_fence() const {return m_fence;}

    /** Create fence
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create();

    /** Wait for fence
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult wait();

private:
    VkFence m_fence;
};

}

#endif // VULKANCLASSES_INCLUDED
