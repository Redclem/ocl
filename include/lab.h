#ifndef LAB_H_INCLUDED
#define LAB_H_INCLUDED

#include "VulkanBase.h"
#include "gui.h"
#include "graphics.h"
#include "custom.hpp"
#include "warning.h"
#include "render_ressources.h"
#include "container_graphics.h"
#include "container.h"

DISABLE_WARNINGS

#include <SDL_vulkan.h>

ENABLE_WARNINGS

#include <string>

class Lab : private VulkanBase, public Gui::EventProcessor
{
public:
    /** Default constructor */
    Lab();

    /** Default destructor */
    ~Lab();

    /** Initialize lab */
    bool init();

    /** Run the lab */
    void run();

    /** Render the lab
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult render();

    /** Get window
     * \return The window
     */
    SDL_Window * get_window() const {return m_window.get_window();}

    /** Create semaphores
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_semaphores();

    /** Cleanup ressources */
    void cleanup();

    DECL_MOVE(Lab)
    BAN_COPY(Lab)

    /** Create fence
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_fence();

    /** Record secondary render buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult record_secondary();

    /** Wait the end of current render operations
     * \return VK_SUCCESS on success,a Vulkan error code on failure
     */
    VkResult wait_render_done();

    /** Execute operations needed on size change
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult on_surface_change();

    /** Update active element */
    void update_active_element();

    /** Update gui (on size change)
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult update_gui();

    /** Add gui elements */
    void add_gui_elements();

    /** Create and record active element render command buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_active_element_render_command_buffer();

    /** Update active element render command buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult update_active_element_render_command_buffer();

    /** Get device
     * \return The device
     */
    VkDevice get_device() const
    {
        return VulkanBase::get_device();
    }

    virtual void process_event(const Gui::Event & event) override;

    /** Switch between paused and unpaused mode
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult switch_pause();

    /** Create command pool
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_command_pool();

    /** Record containers render buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult record_containers_render_buffer();

#ifdef DEBUG

    /** Execute tests (messy code ran at end of init)
     * \return true of success, false on failure (will abort)
     */
    bool exec_tests();

#endif // DEBUG

    /** Render container
     * \param container_index The index of the container to render
     * \param cmd_buf The command buffer to register the render commands into
     */
    void render_container(size_t container_index, VkCommandBuffer cmd_buf) const;

private:
    Gui::Root m_gui_root;

    Graphics::Window m_window;

    Graphics::Surface m_icon;

    VkSemaphore m_semaphore_image_ready;
    VkSemaphore m_semaphore_render_done;

    VkCommandPool m_command_pool;
    VkFence m_fence_render;

    VkCommandBuffer m_render_command_buffer;
    VkCommandBuffer m_gui_render_buffer;
    VkCommandBuffer m_gui_update_command_buffer;
    VkCommandBuffer m_active_render_command_buffer;
    VkCommandBuffer m_containers_render_buffer;

    RenderRessources m_render_ressources;

    ContainerGraphicsManager m_container_graphics;

    std::vector<Container> m_containers;
    Vulkan::Pipeline::Container m_container_pipeline;

    const Gui::Element * m_active_element;
    bool m_run : 1;
    bool m_refresh : 1;
    bool m_update_gui : 1;
    bool m_update_active_elem : 1;
    bool m_paused : 1;
    bool m_update_render_commands : 1;
    bool m_surface_changed : 1;
};

#endif // LAB_H_INCLUDED
