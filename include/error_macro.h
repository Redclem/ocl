#ifndef ERROR_MACRO_H_INCLUDED
#define ERROR_MACRO_H_INCLUDED

#include "log_macro.h"

#define CHECK_SDL(operation) {\
    int result(operation);\
    if(result)\
    {\
        log_message("Error " #operation " :", SDL_GetError());\
        return result;\
    }\
}

#define CHECK_RESULT(operation) {\
    int result(operation);\
    if(result)\
    {\
        log_message("Error " #operation);\
        return result;\
    }\
}

#define CHECK_SDL_BOOL(operation) {\
    int result(operation);\
    if(result)\
    {\
        log_message("Error " #operation " :", SDL_GetError());\
        return false;\
    }\
}

#define CHECK_RESULT_BOOL(operation) {\
    int result(operation);\
    if(result)\
    {\
        log_message("Error " #operation);\
        return false;\
    }\
}

#define CHECK_SDL_NO_RETURN(operation) {\
    if(operation)\
    {\
        log_message("Error " #operation << " : " << SDL_GetError() << '\n';\
        return;\
    }\
}

#define CHECK_RESULT_NO_RETURN(operation) {\
    if(operation)\
    {\
        log_message("Error " #operation << '\n';\
        return;\
    }\
}

#endif // ERROR_MACRO_H_INCLUDED
