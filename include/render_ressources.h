#ifndef RENDER_RESSOURCES_H_INCLUDED
#define RENDER_RESSOURCES_H_INCLUDED

#include "moving_macro.h"
#include "vulkan_macro.h"
#include "VulkanClasses.h"
#include "VulkanBase.h"
#include "pipeline.h"

#include <vulkan/vulkan.h>

class RenderRessources : public Vulkan::GraphicObject
{
public:
    /** Default constructor
     * \param base The base to use
     */
    RenderRessources(VulkanBase * base);

    /** Default destructor */
    virtual ~RenderRessources() override;

    virtual void cleanup() override;

    /** Get command pool
     * \return The command pool
     */
    VkCommandPool get_command_pool() const
    {
        return m_command_pool;
    }

    /** Get sampler
     * \return the sampler
     */
    VkSampler get_sampler() const
    {
        return m_sampler;
    }

    /** Get texture pipeline
     * \return The texture pipeline
     */
    const Vulkan::Pipeline::Texture & get_texture_pipeline() const
    {
        return m_texture_pipeline;
    }

    /** Get fill pipeline
     * \return The fill pipeline
     */
    const Vulkan::Pipeline::Fill & get_fill_pipeline() const
    {
        return m_fill_pipeline;
    }

    /** Get line pipeline
     * \return The line pipeline
     */
    const Vulkan::Pipeline::Line & get_line_pipeline() const
    {
        return m_line_pipeline;
    }

    DECL_MOVE(RenderRessources)
    BAN_COPY(RenderRessources)

    /** Init ressources
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult init_ressources();

    /** Create command pool
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_command_pool();

    /** Create sampler
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_sampler();

private:
    VkCommandPool m_command_pool; /**< Member variable m_command_pool : A transient command pool */
    VkSampler m_sampler;

    Vulkan::Pipeline::Texture m_texture_pipeline;
    Vulkan::Pipeline::Fill m_fill_pipeline;
    Vulkan::Pipeline::Line m_line_pipeline;
};

#endif // RENDER_RESSOURCES_H_INCLUDED
