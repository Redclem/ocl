#ifndef CHEMISTRY_H_INCLUDED
#define CHEMISTRY_H_INCLUDED

#include "color.h"
#include "copy_move_macros.h"
#include "moving_macro.h"
#include "measure_type.h"

namespace Chemistry
{
    /** A chemical phase */
    class Phase
    {
    public:
        /** Constructor */
        Phase() = default;

        /** Get Phase color
         * \return The Phase's color
         */
        virtual Color::LinearRGBA get_color() const = 0;

        /** Get volume
         * \return The Phase's volume in milliliters
         */
        virtual volume_t get_volume() const = 0;

        /** Clone phase
         * \return A pointer to a clone of this
         */
        virtual Phase * clone() const = 0;

        BAN_COPY(Phase)
        BAN_MOVE(Phase)

        virtual ~Phase() {}
    };

    /** A phase with a constant color, use for tests only */
    class ConstantColorPhase : public Phase
    {
    public:
        /** Constructor
         * \param color The phase's color
         * \param volume The phase's volume in milliliters, defaults to 0
         */
        ConstantColorPhase(volume_t volume, Color::LinearRGBA color);

        virtual Color::LinearRGBA get_color() const override {return m_color;}
        virtual volume_t get_volume() const override {return m_volume;}

        virtual ConstantColorPhase * clone() const override {return new ConstantColorPhase(m_volume, m_color);}

    private:
        Color::LinearRGBA m_color; /** The phase's color */
        volume_t m_volume; /** The phase's volume */
    };

    /** A pointer to a phase */
    class PhasePtr
    {
    public:
        /** Default constructor */
        PhasePtr() : m_ptr(nullptr) {}

        /** Construct from ptr (move)
         * \param ptr The pointer to move
         */
        PhasePtr(Phase * && ptr) : m_ptr(ptr) {}

        /** Construct from ptr (copy)
         * \param ptr The pointer to copy
         */
        PhasePtr(const Phase * ptr) : m_ptr(ptr->clone()) {}

        /** Destructor */
        ~PhasePtr()
        {
            delete m_ptr;
        }

        /** Move constructor
         * \param from The PhasePtr to move
         */
        PhasePtr(PhasePtr && from) : INIT_EXCHANGE(m_ptr, nullptr) {}

        /** Copy constructor
         * \param form The PhasePtr to copy
         */
        PhasePtr(const PhasePtr & from) : m_ptr(from->clone()) {}

        /** Move assignement operator
         * \param rhs The PhasePtr to move
         * \return Itself after assignement
         */
        PhasePtr & operator=(PhasePtr && rhs)
        {
            ASSIGN_SWAP(m_ptr);
            return *this;
        }

        /** Copy assignement operator
         * \param rhs The PhasePtr to copy
         * \return Itself after assignement
         */
        PhasePtr & operator=(const PhasePtr & rhs)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = (rhs.m_ptr ? rhs.m_ptr->clone() : nullptr);
            return *this;
        }

        /** Move ptr assignement operator
         * \param rhs The ptr to move
         */
        PhasePtr & operator=(Phase * && ptr)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = ptr;
            return *this;
        }

        /** Copy ptr assignement operator
         * \param rhs The ptr to copy
         */
        PhasePtr & operator=(const Phase * ptr)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = (ptr ? ptr->clone() : nullptr);
            return *this;
        }

        /** Create phase
         * \tparam Type The phase type
         * \tparam Args The type of the args to use to create the phase
         * \param args The args to use to create the phase
         */
        template<typename Type, typename ... Args>
        void create(Args ... args)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = new Type(args...);
        }

        /** Indirection operator
         * \return A reference to the owned phase
         */
        Phase & operator*() {return *m_ptr;}

        /** Indirection operator
         * \return A const reference to the owned phase
         */
        const Phase & operator*() const {return *m_ptr;}

        /** Get pointer
         * \return A pointer to the owned phase
         */
        Phase * get_ptr() {return m_ptr;}

        /** Get pointer
         * \return A const pointer to the owned phase
         */
        const Phase * get_ptr() const {return m_ptr;}

        /** Member of pointer operator
         * \return A pointer to the owned phase
         */
        Phase * operator->() {return m_ptr;}

        /** Member of pointer operator
         * \return A const pointer to the owned phase
         */
        const Phase * operator->() const {return m_ptr;}

        /** Cast to Phase *
         * \return A pointer to the owned phase
         */
        operator Phase *() {return m_ptr;}

        /** Cast to const Phase *
         * \return A const pointer to the owned phase
         */
        operator const Phase *() const {return m_ptr;}

    private:
        Phase * m_ptr; /** A pointer to the phase */
    };
}

#endif // CHEMISTRY_H_INCLUDED
