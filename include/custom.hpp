#ifndef VECTOR_HPP_INCLUDED
#define VECTOR_HPP_INCLUDED

#include "moving_macro.h"

#include <cstddef>
#include <initializer_list>
#include <utility>
#include <functional>
#include <iostream>
#include <cassert>
#include <cstring>

namespace Custom
{

/** Simple vector class */
template<typename Type>
class Vector
{
public:

    /** Get vector size
     * \return The vector's size
     */
    size_t get_size() const
    {
        return m_size;
    }

    /** Get byte size
     * \return The size in bytes of the elements
     */
    size_t get_byte_size() const
    {
        return m_size * sizeof(Type);
    }

    /** Binary copy contents to given location
     * \param dest The location receiving data
     */
    void copy_to(void * dest) const
    {
        memcpy(dest, m_data, get_byte_size());
    }

    /** Get vector data
     * \return A pointer to the vector's data
     */
    Type * get_data()
    {
        return m_data;
    }

    /** Get vector data
     * \return A pointer to the vector's data
     */
    const Type * get_data() const
    {
        return m_data;
    }

    /** Construct empty vector */
    Vector() : m_data(nullptr), m_size(0) {}

    /** Construct with given size
     * \param size The vector's size
     * \param value The value to fill with (default to default constructed)
     */
    template<typename From = Type>
    Vector(size_t size, const From & value) : m_data(size ? new Type[size] : nullptr), m_size(size)
    {
        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = value;
    }

    /** Construct with given size
     * \param size The vector's size
     */
    template<typename From = Type>
    Vector(size_t size) : m_data(size ? new Type[size] : nullptr), m_size(size)
    {
    }

    /** Assign vector content
     * \param size New size for the container
     * \param val The value to fill the container with
     */
    template<typename From = Type>
    void assign(size_t size, const From & value = From())
    {
        resize(size);
        for(size_t iter(0);iter != m_size;++iter)
            m_data[iter] = value;
    }

    /** Construct from initializer list
     * \param list The initializer list
     */
    template<typename From = Type>
    Vector(const std::initializer_list<From> & list) : m_data(list.size() ? new Type[list.size()] : nullptr), m_size(list.size())
    {
        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = *(list.begin() + iter);
    }

    /** Clear vector */
    void clear()
    {
        if(m_data)
        {
            delete[] m_data;
            m_data = nullptr;
        }
        m_size = 0;
    }

    /** Destructor */
    ~Vector()
    {
        clear();
    }

    /** Is vector empty
     * \return true if empty, false if not
     */
    bool empty() const
    {
        return m_size == 0;
    }

    /** Resize vector (can discard content)
     * \param size The new size
     */
    void resize_discard(size_t size)
    {
        if(size == m_size)
            return;

        if(size == 0)
        {
            clear();
            return;
        }

        if(m_data)
        {
            delete[] m_data;
        }

        m_size = size;
        m_data = new Type[m_size];
    }

    /** Resize vector (keeps contents)
     * \param size The new size
     */
    void resize(size_t size)
    {
        if(size == m_size)
            return;

        if(size == 0)
        {
            clear();
            return;
        }

        if(m_size)
        {
            size_t transfer_size = (size > m_size ? m_size : size);
            Type * new_data = new Type[size];

            for(size_t iter(0); iter != transfer_size; ++iter)
            {
                new_data[iter] = std::move(m_data[iter]);
            }

            delete[] m_data;

            m_size = size;
            m_data = new_data;
        }
        else
        {
            m_size = size;
            m_data = new Type[m_size];
        }
    }

    /** Move constructor
     * \param from The vector to move
     */
    Vector(Vector<Type> && from) : INIT_EXCHANGE(m_data, nullptr), INIT_EXCHANGE(m_size, 0) {}

    /** Move assignement operator
     * \param rhs The vector to move
     * \return Itself after assignement
     */
    Vector & operator=(Vector<Type> && rhs)
    {
        CHECK_SELF_ASSIGN;
        ASSIGN_SWAP(m_data);
        ASSIGN_SWAP(m_size);
        return *this;
    }

    /** Copy constructor
     * \param from The vector to copy
     */
    template<typename From = Type>
    Vector(const Vector<From> & from) : m_data(new Type[from.m_size]), INIT_COPY(m_size)
    {
        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = from.m_data[iter];
    }

    /** Copy constructor
     * \param from The vector to copy
     */
    Vector(const Vector & from) : m_data(new Type[from.m_size]), INIT_COPY(m_size)
    {
        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = from.m_data[iter];
    }

    /** Copy assignement operator
     * \param rhs The vector to copy
     */
    template<typename From = Type>
    Vector & operator=(const Vector<From> & rhs)
    {
        CHECK_SELF_ASSIGN;

        resize_discard(rhs.m_size);

        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = rhs.m_data[iter];

        return *this;
    }

    /** Copy assignement operator
     * \param rhs The vector to copy
     */
    Vector & operator=(const Vector & rhs)
    {
        CHECK_SELF_ASSIGN;

        resize_discard(rhs.m_size);

        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = rhs.m_data[iter];

        return *this;
    }

    /** Copy assignement operator
     * \param list The initializer list to copy
     */
    template<typename From = Type>
    Vector & operator=(const std::initializer_list<From> & list)
    {
        resize_discard(list.size());

        for(size_t iter(0); iter != m_size; ++iter)
            m_data[iter] = *(list.begin() + iter);

        return *this;
    }

    /** Add element at back
     * \param elem The element to add
     */
    template<typename From = Type>
    void push_back(const From & elem)
    {
        size_t prev_size = m_size;
        resize(prev_size + 1);
        m_data[prev_size] = elem;
    }

    /** Add element at back
     * \param elem The element to add
     */
    template<typename From = Type>
    void push_back(From && elem)
    {
        size_t prev_size = m_size;
        resize(prev_size + 1);
        m_data[prev_size] = std::move(elem);
    }

    /** Remove last element */
    void pop_back()
    {
        resize(m_size - 1);
    }

    /** Emplace element at end (construct and push back)
     * \param args The arguments used to create the element
     */
    template<typename... Args>
    void emplace_back(Args... args)
    {
        size_t prev_size = m_size;
        resize(prev_size + 1);
        m_data[prev_size] = Type(args...);
    }

    /** Get first element
     * \return A reference to the first element
     */
    Type & front()
    {
        return *m_data;
    }

    /** Get first element
     * \return A const reference to the first element
     */
    const Type & front() const
    {
        return *m_data;
    }

    /** Get last element
     * \return A reference to the last element
     */
    Type & back()
    {
        return m_data[m_size - 1];
    }

    /** Get last element
     * \return A const reference to the last element
     */
    const Type & back() const
    {
        return m_data[m_size - 1];
    }

    /** Subscript operator
     * \param index The index of the element to access
     * \return A reference to the accessed element
     */
    Type & operator[](size_t index)
    {
        return m_data[index];
    }

    /** Subscript operator
     * \param index The index of the element to access
     * \return A const reference to the accessed element
     */
    const Type & operator[](size_t index) const
    {
        return m_data[index];
    }

    /** Get element at given index
     * \param index The index of the element to access
     * \return A reference to the accessed element
     */
    Type & at(size_t index)
    {
        return m_data[index];
    }

    /** Get element at given index
     * \param index The index of the element to access
     * \return A const reference to the accessed element
     */
    const Type & at(size_t index) const
    {
        return m_data[index];
    }

    /** Vector Iterator class */
    class Iterator
    {
    public:

        friend class Vector;

        /** Default constructor */
        Iterator() : m_ptr(nullptr) {}

        /** Addition operator
         * \param rhs The number to add
         * \return The result
         */
        Iterator operator+(ptrdiff_t rhs) const
        {
            return Iterator(m_ptr + rhs);
        }

        /** subtraction operator
         * \param rhs The number to substract
         * \return The result
         */
        Iterator operator-(ptrdiff_t rhs) const
        {
            return Iterator(m_ptr - rhs);
        }

        /** subtraction operator
         * \param rhs The iterator to substract
         * \return The result
         */
        ptrdiff_t operator-(const Iterator & rhs) const
        {
            return m_ptr - rhs.m_ptr;
        }

        /** Prefix incrementation operator
         * \return Itself after assignement
         */
        Iterator & operator++()
        {
            m_ptr++;
            return *this;
        }

        /** Prefix decrementation operator
         * \return Itself after assignement
         */
        Iterator & operator--()
        {
            m_ptr--;
            return *this;
        }

        /** Postfix incrementation operator
         * \return Itself before assignement
         */
        Iterator operator++(int)
        {
            Iterator before(m_ptr);
            m_ptr++;
            return before;
        }

        /** Postfix decrementation operator
         * \return Itself before assignement
         */
        Iterator operator--(int)
        {
            Iterator before(m_ptr);
            m_ptr--;
            return before;
        }

        /** Addition assignement operator
         * \param rhs The number to add
         * \return Itself after addition
         */
        Iterator & operator+=(ptrdiff_t rhs)
        {
            m_ptr += rhs;
            return *this;
        }

        /** subtraction assignement operator
         * \param rhs The number to add
         * \return Itself after subtraction
         */
        Iterator & operator-=(ptrdiff_t rhs)
        {
            m_ptr -= rhs;
            return *this;
        }

        /** Indirection operator
         * \return A reference to the referenced element
         */
        Type & operator*() const
        {
            return *m_ptr;
        }

        /** Member of pointer operator
         * \return The member of the referenced element
         */
        Type * operator->() const
        {
            return m_ptr;
        }

        /** Equality operator
         * \param rhs The iterator to compare to
         * \return true if equal, false if not
         */
        bool operator==(const Iterator & rhs)
        {
            return m_ptr == rhs.m_ptr;
        }

        /** Inquality operator
         * \param rhs The iterator to compare to
         * \return true if inequal, false if not
         */
        bool operator!=(const Iterator & rhs)
        {
            return m_ptr != rhs.m_ptr;
        }

        /** Less than operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly before rhs, false if not
         */
        bool operator<(const Iterator & rhs)
        {
            return m_ptr < rhs.m_ptr;
        }

        /** Greater than operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly after rhs, false if not
         */
        bool operator>(const Iterator & rhs)
        {
            return m_ptr > rhs.m_ptr;
        }

        /** Less or equal to operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly before or equal to rhs, false if not
         */
        bool operator<=(const Iterator & rhs)
        {
            return m_ptr < rhs.m_ptr;
        }

        /** Greater or equal to operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly after or equal to rhs, false if not
         */
        bool operator>=(const Iterator & rhs)
        {
            return m_ptr >= rhs.m_ptr;
        }

    private:
        Type * m_ptr;

        Iterator(Type * ptr) : m_ptr(ptr) {}
    };

    /** Vector Constant Iterator class */
    class ConstIterator
    {
    public:

        friend class Vector;

        /** Construct from Iterator */
        ConstIterator(const Iterator & from) : m_ptr(from.m_ptr) {}

        /** Default constructor */
        ConstIterator() : m_ptr(nullptr) {}

        /** Addition operator
         * \param rhs The number to add
         * \return The result
         */
        ConstIterator operator+(ptrdiff_t rhs) const
        {
            return ConstIterator(m_ptr + rhs);
        }

        /** subtraction operator
         * \param rhs The number to substract
         * \return The result
         */
        ConstIterator operator-(ptrdiff_t rhs) const
        {
            return ConstIterator(m_ptr - rhs);
        }

        /** subtraction operator
         * \param rhs The ConstIterator to substract
         * \return The result
         */
        ptrdiff_t operator-(const ConstIterator & rhs) const
        {
            return m_ptr - rhs.m_ptr;
        }

        /** Prefix incrementation operator
         * \return Itself after assignement
         */
        ConstIterator & operator++()
        {
            m_ptr++;
            return *this;
        }

        /** Prefix decrementation operator
         * \return Itself after assignement
         */
        ConstIterator & operator--()
        {
            m_ptr--;
            return *this;
        }

        /** Postfix incrementation operator
         * \return Itself before assignement
         */
        ConstIterator operator++(int)
        {
            ConstIterator before(m_ptr);
            m_ptr++;
            return before;
        }

        /** Postfix decrementation operator
         * \return Itself before assignement
         */
        ConstIterator operator--(int)
        {
            ConstIterator before(m_ptr);
            m_ptr--;
            return before;
        }

        /** Addition assignement operator
         * \param rhs The number to add
         * \return Itself after addition
         */
        ConstIterator & operator+=(ptrdiff_t rhs)
        {
            m_ptr += rhs;
            return *this;
        }

        /** subtraction assignement operator
         * \param rhs The number to add
         * \return Itself after subtraction
         */
        ConstIterator & operator-=(ptrdiff_t rhs)
        {
            m_ptr -= rhs;
            return *this;
        }

        /** Indirection operator
         * \return A reference to the referenced element
         */
        const Type & operator*() const
        {
            return *m_ptr;
        }

        /** Member of pointer operator
         * \return The member of the referenced element
         */
        const Type * operator->() const
        {
            return m_ptr;
        }

        /** Equality operator
         * \param rhs The iterator to compare to
         * \return true if equal, false if not
         */
        bool operator==(const ConstIterator & rhs)
        {
            return m_ptr == rhs.m_ptr;
        }

        /** Inquality operator
         * \param rhs The iterator to compare to
         * \return true if inequal, false if not
         */
        bool operator!=(const ConstIterator & rhs)
        {
            return m_ptr != rhs.m_ptr;
        }

        /** Less than operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly before rhs, false if not
         */
        bool operator<(const ConstIterator & rhs)
        {
            return m_ptr < rhs.m_ptr;
        }

        /** Greater than operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly after rhs, false if not
         */
        bool operator>(const ConstIterator & rhs)
        {
            return m_ptr > rhs.m_ptr;
        }

        /** Less or equal to operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly before or equal to rhs, false if not
         */
        bool operator<=(const ConstIterator & rhs)
        {
            return m_ptr < rhs.m_ptr;
        }

        /** Greater or equal to operator
         * \param rhs The iterator to compare to
         * \return true if this is strictly after or equal to rhs, false if not
         */
        bool operator>=(const ConstIterator & rhs)
        {
            return m_ptr >= rhs.m_ptr;
        }

    private:
        const Type * m_ptr;

        ConstIterator(const Type * ptr) : m_ptr(ptr) {}
    };

    /** Get iterator to beginning of vector
     * \return An iterator to beginning
     */
    Iterator begin()
    {
        return Iterator(m_data);
    }

    /** Get const iterator to beginning of vector
     * \return A const iterator to beginning
     */
    ConstIterator begin() const
    {
        return ConstIterator(m_data);
    }

    /** Get past-end iterator
     * \return The past-end iterator
     */
    Iterator end()
    {
        return Iterator(m_data + m_size);
    }

    /** Get const past-end iterator
     * \return The const past-end iterator
     */
    ConstIterator end() const
    {
        return ConstIterator(m_data + m_size);
    }

private:
    Type * m_data;
    size_t m_size;
};

/** A map (ordered pair vector) */
template<typename Key, typename Value, class Less = std::less<Key>>
class Map : Vector<std::pair<Key, Value>>
{
public:
    typedef std::pair<Key, Value> ValueType;

    /** Default constructor */
    Map() : Vector<ValueType>::Vector() {}

    /** Copy constructor
     * \param from The map to copy
     */
    Map(const Map & from) : Vector<ValueType>::Vector(from) {}

    /** Move constructor
     * \param from The map to move
     */
    Map(Map && from) : Vector<ValueType>::Vector(std::move(from)) {}

    /** Copy assignement operator
     * \param rhs The map to copy
     * \return Itself after assignement
     */
    Map & operator=(const Map & rhs)
    {
        Vector<ValueType>::operator=(rhs);
        return *this;
    }

    /** Move assignement operator
     * \param rhs The map to move
     * \return Itself after assignement
     */
    Map & operator=(Map && rhs)
    {
        Vector<ValueType>::operator=(std::move(rhs));
        return *this;
    }

    using Vector<ValueType>::get_size;
    using Vector<ValueType>::get_data;
    using Vector<ValueType>::empty;
    using Vector<ValueType>::clear;

    using Vector<ValueType>::begin;
    using Vector<ValueType>::end;

    typedef typename Vector<ValueType>::Iterator Iterator;
    typedef typename Vector<ValueType>::ConstIterator ConstIterator;

    /** Check if map contains given key
     * \param key The key to check
     * \param comp A comparator (less than) object
     * \return true if key contained, false otherwise
     */
    template<typename Comp = Less>
    bool contains(const Key & key, Comp cmp = Comp()) const
    {
        if(get_size() == 0)
            return false;

        ConstIterator search_res = search(key, cmp);

        return !(search_res->first, key) || cmp(key, search_res->first);
    }

    /** Find element with given key
     * \param key The key of the element to find
     * \param cmp A comparator (less than) object
     * \return An iterator to the element with given key or past the end iterator if not found
     */
    template<typename Comp = Less>
    Iterator find(const Key & key, Comp cmp = Comp())
    {
        if(get_size() == 0)
            return end();

        Iterator search_res = search(key, cmp);

        return (cmp(search_res->first, key) || cmp(key, search_res->first)) ? end() : search_res;
    }

    /** Find element with given key
     * \param key The key of the element to find
     * \param comp A comparator (less than) object
     * \return A const iterator to the element with given key or past the end iterator if not found
     */
    template<typename Comp = Less>
    ConstIterator find(const Key & key, Comp cmp = Comp()) const
    {
        if(get_size() == 0)
            return end();

        ConstIterator search_res = search(key, cmp);

        return (cmp(search_res->first, key) || cmp(key, search_res->first)) ? end() : search_res;
    }

    /** Insert element at given key
     * \param key The key at wich to insert the element
     * \param val The value to insert
     * \param comp A comparator (less than) object
     * \return A pair containing a bool describing if the operation succeeded, and an iterator to the previous element or the inserted one
     */
    template<typename From = Value, typename Comp = Less>
    std::pair<Iterator, bool> insert(const Key & key, const From & val = From(), Comp cmp = Comp())
    {
        if(get_size())
        {
            Iterator search_res = search(key, cmp);

            if(cmp(key, search_res->first))
            {
                search_res = create_space(search_res);
                *search_res = {key, val};
                return {search_res, true};
            }
            else if (cmp(search_res->first, key))
            {
                search_res = create_space(++search_res);
                *search_res = {key, val};
                return {search_res, true};
            }

            return {search_res, false};

        }

        Vector<ValueType>::resize(1);
        *begin() = {key, val};
        return {begin(), true};
    }

    /** Insert element at given key
     * \param key The key at wich to insert the element
     * \param val The value to insert
     * \param cmp A comparator (less than) object
     * \return A pair containing a bool describing if the operation succeeded, and an iterator to the previous element or the inserted one
     */
    template<typename Comp = Less>
    std::pair<Iterator, bool> insert(const Key & key, Value && val, Comp cmp = Comp())
    {
        if(get_size())
        {
            Iterator search_res = search(key, cmp);

            if(cmp(key, search_res->first))
            {
                search_res = create_space(search_res);
                *search_res = {key, std::move(val)};

                return {search_res, true};
            }
            else if (cmp(search_res->first, key))
            {
                search_res = create_space(++search_res);
                *search_res = {key, std::move(val)};

                return {search_res, true};
            }

            return {search_res, false};

        }

        Vector<ValueType>::resize(1);
        *begin() = {key, std::move(val)};
        return {begin(), true};
    }

    /** Subscript operator
     * \param key The key of the element to access
     * \return A reference to the element of given key, inserting a default value if non existent
     */
    Value & operator[](const Key & key)
    {
        Less cmp;

        if(get_size() == 0)
        {
            Vector<ValueType>::resize(1);
            *begin() = {key, Value()};

            return begin()->second;
        }

        Iterator search_res = search(key, cmp);

        if(cmp(key, search_res->first))
        {
            search_res = create_space(search_res);
            *search_res = {key, Value()};
        }
        else if(cmp(search_res->first, key))
        {
            search_res = create_space(++search_res);
            *search_res = {key, Value()};
        }

        return Vector<ValueType>::at(search_res).second;
    }

    /** Erase element at specified position
     * \param iter An iterator to the element to erase
     * \return An iterator following the removed element
     */
    Iterator erase(Iterator iter)
    {
        ptrdiff_t pos(iter - begin());

        while(iter + 1 != end())
        {
            *iter = std::move(*(iter + 1));
            ++iter;
        }

        Vector<ValueType>::resize(get_size() - 1);
        return begin() + pos;
    }

    /** Erase element with specified key
     * \param key The key of the element to erase
     */
    void erase(const Key & key)
    {
        Iterator iter = find(key);

        if(iter == end())
            return;

        while(iter + 1 != end())
        {
            *iter = std::move(*(iter + 1));
            ++iter;
        }

        Vector<ValueType>::resize(get_size() - 1);
    }

private:

    template<typename Comp = Less>
    Iterator search(const Key & key, Comp cmp = Comp())
    {
        Iterator search_beg(begin()), search_end(end());

        while((search_end - search_beg) != 1)
        {
            Iterator mid = search_beg + (search_end - search_beg) / 2;

            if(cmp(key, mid->first))
            {
                search_end = mid;
            }
            else
            {
                search_beg = mid;
            }
        }

        return search_beg;
    }

    template<typename Comp = Less>
    ConstIterator search(const Key & key, Comp cmp = Comp()) const
    {
        ConstIterator search_beg(begin()), search_end(end());

        while((search_end - search_beg) != 1)
        {
            ConstIterator mid = search_beg + (search_end - search_beg) / 2;

            if(cmp(key, mid->first))
            {
                search_end = mid;
            }
            else
            {
                search_beg = mid;
            }
        }

        return search_beg;
    }

    /** Create space at given iterator
     * The element currently at iter is placed after the created space
     * \param iter The position to create space at
     * \return an iter to the created space
     */
    Iterator create_space(const Iterator & iter)
    {
        ptrdiff_t index(iter - begin());

        Vector<ValueType>::emplace_back();

        Iterator end_dst(begin() + index);
        Iterator cur_dst(end() - 1);

        while(cur_dst != end_dst)
        {
            *cur_dst = *(cur_dst - 1);
            --cur_dst;
        }

        return begin() + index;
    }
};

}

#endif // VECTOR_HPP_INCLUDED
