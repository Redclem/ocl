#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#include "VulkanClasses.h"
#include "pipeline.h"
#include "graphics.h"
#include "warning.h"
#include "error_macro.h"
#include "moving_macro.h"
#include "vulkan_macro.h"
#include "custom.hpp"
#include "vertex.h"
#include "render_ressources.h"
#include "vec.h"

DISABLE_WARNINGS

#include <stb_image.h>
#include <stb_truetype.h>

ENABLE_WARNINGS

#include <fstream>
#include <cstdio>
#include <cctype>
#include <cassert>
#include <cstring>

namespace Gui
{

/** The box containing an element */
struct Rect
{
    float x;
    float y;
    float w;
    float h;
};

/** A bidimensionnal point */
struct Point
{
    float x;
    float y;
};

/** Is point in rectangle
 * \param rect The rectangle
 * \param point The point
 * \return true if point is in rect, false otherwise
 */
bool point_in_rect(const Rect & rect, const Point & point);


/** Forward declaration of Element */
class Element;

/** An baic rectangle */
struct BasicRectangle
{
    Vertex::Basic vertices[4];
};

/** A textured rectangle */
struct TexturedRectangle
{
    Vertex::Textured vertices[4];
};

struct Event
{
    /** Event type */
    enum class Type
    {
        Quit,
        PopElement,
        PushElement,
        ToggleFullscreen,
        SetResolution
    };

    Type type;

    /**************************//**
     * Specialized event structs
     *****************************/

    struct PushElementEvent
    {
        Element * pushed_element;
    };

    struct SetResolutionEvent
    {
        VkExtent2D extent;
    };

    /**************************//**
     * Union of specialized structs
     *****************************/

    union
    {
        PushElementEvent pushed_element_event;
        SetResolutionEvent set_resolution_event;
    };
};

/** An abstract class for an object capable of processing events */
class EventProcessor
{
public:
    EventProcessor() {}
    virtual ~EventProcessor() {}
    virtual void process_event(const Event & event) = 0;
private:
};

/** The root of a gui, containing ressources and settings */
class Root : public Vulkan::MemoryObject
{
public:

    /***************************************************//**
     * Member classes
     ******************************************************/

    /** A texture : image and image view */
    struct Texture
    {
        uint32_t width;
        uint32_t height;
        VkImage image;
        VkImageView view;
        VkDescriptorSet descriptor_set;
    };

    /** A reference to a texture */
    class TextureReference
    {
    public:
        friend class Root;

        /** Default constructor */
        TextureReference() : m_texture(nullptr) {}

        /** Copy constructor
         * \param from The reference to copy
         */
        TextureReference(const TextureReference & from) : INIT_COPY(m_texture) {}

        /** Copy assignement operator
         * \param rhs The reference to copy
         * \return Itself after assignement
         */
        TextureReference & operator=(const TextureReference & rhs)
        {
            ASSIGN_COPY(m_texture);
            return *this;
        }

        /** Member of pointer operator
         * \return A pointer to the underlying texture
         */
        const Texture * operator->() const
        {
            return m_texture;
        }

        /** Is reference valid
         * \return true if value, false if not
         */
        bool is_valid() const {return m_texture != nullptr;}

    private:

        TextureReference(const Texture * texture) : m_texture(texture) {}

        const Texture * m_texture;
    };

    /***************************************************//**
    * Constants
    *******************************************************/

    static constexpr VkDeviceSize reserved_buffer_size = sizeof(BasicRectangle); /**< Constant reserved_buffer_size : Size of the buffer region reserved for the root */

    /***************************************************//**
     * Member functions
     ******************************************************/



    /** Default constructor
     * \param base The VulkanBase to use
     * \param event_proc The event processor
     * \param render_ressources The render ressources
     */
    Root(VulkanBase * base, EventProcessor * event_proc, const RenderRessources * render_ressources);

    Root(const Root& from) = delete;
    Root & operator=(const Root & rhs) = delete;

    /** Move constructor
    * \param from The object to move
    */
    Root(Root && from);

    /** Move assignement operator
    * \param rhs The object to move
    * \return Itself after assignement
    */
    Root & operator=(Root && rhs);

    /** Initialize root ressources
     * \param font The path to the font file
     * \param font_hgt The font pixel height
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult init(const char * font, float font_hgt);

    virtual void cleanup() override;

    /** Default destructor */
    virtual ~Root()
    {
        cleanup();
    }

    /** Get font
     * \return The font
     */
    const stbtt_fontinfo * get_font() const
    {
        return &m_font;
    }

    /** Get font scale
     * \return The font scale
     */
    float get_font_scale() const
    {
        return m_font_scale;
    }

    /** Get cursor position
     * \return The current cursor position
     */
    const Point get_cursor_pos() const
    {
        return m_cursor_pos;
    }

    /** On mouse movement
     * \param cursor_pos The new cursor position
     * \param cmd_buf The command buffer in wich to register update commands
     */
    void update_active_element(const SDL_Point & cursor_pos);

    /** Get texture pipeline
     * \return The texture pipeline
     */
    const Vulkan::Pipeline::Texture & get_texture_pipeline() const
    {
        return m_render_ressources->get_texture_pipeline();
    }

    /** Get fill pipeline
     * \return The fill pipeline
     */
    const Vulkan::Pipeline::Fill & get_fill_pipeline() const
    {
        return m_render_ressources->get_fill_pipeline();
    }

    /** Get line pipeline
     * \return The texture pipeline
     */
    const Vulkan::Pipeline::Line & get_line_pipeline() const
    {
        return m_render_ressources->get_line_pipeline();
    }

    virtual VkMemoryRequirements get_memory_requirements() const override;

    /** Get inner memory requirements
     * \return The inner memory requirements
     */
    VkMemoryRequirements get_inner_memory_requirements() const;

    /** Upload images
     * \param cmd_buf The command buffer to register the upload commands into
     * \param host_buffer A buffer to use to store host-side data
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult upload_images(VkCommandBuffer cmd_buf, Vulkan::Buffer & host_buffer);

    /** Create image views
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_views();

    virtual VkResult bind_memory() override;

    /** Create shared buffer
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_shared_buffer();

    /** Require a texture with the given path to be loaded
     * \param path The path of the texture to be loaded
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult require_texture(const char * path);

    /** Create descriptor pool
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_descriptor_pool();

    /** Get descriptor pool
     * \return The descriptor pool
     */
    VkDescriptorPool get_descriptor_pool() const
    {
        return m_descriptor_pool;
    }

    /** Get the pixel height
     * \return The pixel height
     */
    float get_pixel_height() const
    {
        return m_pixel_height;
    }

    /** Get the pixel width
     * \return The pixel width
     */
    float get_pixel_width() const
    {
        return m_pixel_width;
    }

    /** Construct & add element
     * \tparam ElemType The type of the element
     * \param Args the args used to build the element
     * \return A pointer to the created element
     */
    template<class ElemType, typename ...Args>
    ElemType * add_element(Args... args)
    {
        ElemType * elem = new ElemType(this, args...);

        m_elements.push_back(elem);

        return elem;
    }

    /** Get shared buffer
     * \return The shared buffer
     */
    VkBuffer get_shared_buffer() const
    {
        return m_shared_buffer;
    }

    /** Record render commands
     * \param cmd_buf The command buffer receiving the render commands
     */
    virtual void render(VkCommandBuffer cmd_buf) const;

    /** Create semaphores
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_semaphores();

    /** Set pipeline viewport and scissor state
     * \param cmd_buf The command buffer in wich to register the commands to update the states
     */
    void set_pipeline_viewport_scissor(VkCommandBuffer cmd_buf) const
    {
        get_base()->set_viewport_scissor(cmd_buf);
    }

    /** Update pixel sizes */
    void compute_pixel_sizes();

    /** Require text
     * \param text The text to require
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult require_text(const char * text);

    /** Create textures descriptor sets
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_descriptor_sets();

    /** Get the descriptor set of a required texture
     * \param path The texture's path
     * \return A reference to the required text
     */
    TextureReference get_texture(const char * path) const;

    /** Get the descriptor set of a required text
     * \param text The test
     * \return A reference to the required text texture
     */
    TextureReference get_text(const char * text) const;

    /** Update GUI (execute on size change)
     * \param cmd_buf The command buffer to register the update commands into
     */
    void update_gui(VkCommandBuffer cmd_buf);

    /** String comparator class */
    class StringCompare
    {
    public:
        /** Call operator : Compare strings
         * \param str0 The first string to compare
         * \param str1 The second string to compare
         * \return true if str0 < str1, false else
         */
        bool operator()(const char * str0, const char * str1)
        {
            return strcmp(str0, str1) < 0;
        }
    };

    /** Push main element
     * \param element The element to push
     */
    void push_main_element(Element * element)
    {
        m_main_elements_stack.push_back(element);
    }

    /** Pop main element */
    void pop_main_element()
    {
        m_main_elements_stack.pop_back();
    }

    /** Get active element
     * \return The active element
     */
    const Element * get_active_element() const;

    /** Render active element box
     * \param cmd_buf The command buffer to register the render commands into
     */
    void render_active_element_box(VkCommandBuffer cmd_buf) const;

    /** Update active element vertices
     * \param active_element The active element
     * \param cmd_buf The command buffer to register update commands into
     */
    void update_active_element_buffer(const Element * active_element, VkCommandBuffer cmd_buf);

    /** Process event
     * \param event The event to process
     */
    void process_event(const Event & evnt)
    {
        m_event_proc->process_event(evnt);
    }

    /** Click callback */
    void on_click() const;

    /** Clear the main element stack */
    void clear_main_element_stack()
    {
        m_main_elements_stack.clear();
    }

    /** Get element at given index in root
     * \param idx The element index
     * \return The element at given index in root
     */
    Element * get_element_at(size_t index)
    {
        return m_elements[index];
    }

    /** Does the root have a main element
     * \return true if root has main element, false otherwise
     */
    bool has_main_element() const
    {
        return !m_main_elements_stack.empty();
    }

    /** Replace an element with another
     * Used when moving elements
     */

    void replace_element(Element * to_replace, Element * replacing);

private:

    void pop_element();

private:

    const RenderRessources * m_render_ressources;

    VkDescriptorPool m_descriptor_pool;

    VkBuffer m_shared_buffer;

    typedef Custom::Map<const char *, Texture, StringCompare> texture_map_t;

    texture_map_t m_textures;
    texture_map_t m_texts;

    Custom::Vector<Element*> m_elements;

    Custom::Vector<Element*> m_main_elements_stack;

    stbtt_fontinfo m_font;
    unsigned char * m_font_data;
    float m_font_scale;
    Point m_cursor_pos;

    float m_pixel_height;
    float m_pixel_width;

    EventProcessor * m_event_proc;
};

class Element
{
public:

    static constexpr Rect null_rect = {0.0f, 0.0f, 0.0f, 0.0f};

    /** Default constructor
     * \param root The root to use
     * \param box The element's box
     */
    Element(Root * root, const Rect & box = null_rect) : m_root(root), m_box(box), m_buffer_offset(0)
    {
    }

    /** First initialization : Create elements (without allocating memory)
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult first_initialization() = 0;

    /** \brief Get element memory requirements
     * Get element ressources memory requirements
     * Element must have been throught first initialization
     * \return The element's memory requirements
     */
    virtual VkMemoryRequirements get_memory_requirements() const
    {
        return VkMemoryRequirements{0, 1, ~uint32_t(0)};
    }

    /** Get required buffer size
     * \return The required buffer size
     */
    virtual VkDeviceSize get_buffer_size() const
    {
        return 0;
    }

    /** \b Bind memory
     * Bind memory to element ressources
     * \warning Element must have been through first initialization
     * \param offset An offset specifying the region of memory in the root's memory dedicated to this object
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult bind_memory(VkDeviceSize offset);

    /** Second initialization : Upload and create remaining ressources (image views)
     * \warning Element must have been through memory binding
     * \param host_buffer A host-side buffer to use to store data to be uploaded, cleared after command buffer submission
     * \param cmd_buf A command buffer to record the uploading commands into
     * \param buffer_offset An offet into the root's shared buffer dedicated to this object
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    virtual VkResult second_initialization(Vulkan::Buffer & host_buffer, VkCommandBuffer cmd_buf) = 0;

    /** Get the root
     * \return The root
     */
    Root * get_root() const
    {
        return m_root;
    }

    /** Get vulkan base
     * \return The vulkan base
     */
    VulkanBase * get_base() const
    {
        return m_root->get_base();
    }

    /** Get device
     * \return The device
     */
    VkDevice get_device() const
    {
        return m_root->get_base()->get_device();
    }

    /** Cleanup element ressources */
    virtual void cleanup() {}

    /** Destructor */
    virtual ~Element()
    {
        cleanup();
    }

    /** Get required texture descriptor sets count
     * \return The required texture descriptor sets count
     */
    virtual uint32_t get_texture_descriptor_set_count() const
    {
        return 0;
    }

    DECL_MOVE(Element)
    BAN_COPY(Element)

    /** Get box
     * \return The element's box
     */
    const Rect & get_box() const
    {
        return m_box;
    }

    /** Get box
     * \return The element's box
     */
    Rect & get_box()
    {
        return m_box;
    }

    /** Set box
     * \param box The new box
     */
    void set_box(const Rect & box)
    {
        m_box = box;
    }

    /** Set buffer offset
     * \param offset The new buffer offet
     */
    void set_buffer_offset(VkDeviceSize offset)
    {
        m_buffer_offset = offset;
    }

    /** Get buffer offset
     * \return The buffer offet
     */
    VkDeviceSize get_buffer_offset() const
    {
        return m_buffer_offset;
    }

    /** Render element
     * \param cmd_buffer The command buffer to register commands into
     */
    virtual void render(VkCommandBuffer cmd_buf) const = 0;

    /** Update element (on size change)
     * \param cmd_buf The command buffer to register update commands into
     */
    virtual void update(VkCommandBuffer cmd_buf) = 0;

    /** Click callback */
    virtual void on_click() const;

    /** \brief Get active element
     * Get the element wich is hovered by the mouse.
     * Returns itself or a contained element or nullptr
     * \return The active element
     */
    virtual const Element * get_active_element() const
    {
        return nullptr;
    }

private:
    Root * m_root;
    Rect m_box;
    VkDeviceSize m_buffer_offset;
};

class Label : public Element
{
public:
    /** Default constructor
     * \param root The gui root
     * \param box The element's box
     */
    Label(Root * root, const Rect & box = null_rect, const char * text = nullptr) : Element::Element(root, box), m_text_texture(), m_text(text) {}

    virtual VkResult first_initialization() override;

    virtual VkDeviceSize get_buffer_size() const override
    {
        return sizeof(TexturedRectangle);
    }
    virtual VkResult second_initialization(Vulkan::Buffer & host_buffer, VkCommandBuffer cmd_buf) override;

    virtual void cleanup() override
    {
    }

    /** Set text
     * \param text The new text
     */
    void set_text(const char * text)
    {
        m_text = text;
    }

    /** Get text
     * \return The text
     */
    const char * get_text()
    {
        return m_text;
    }

    virtual void render(VkCommandBuffer cmd_buf) const override;

    virtual void update(VkCommandBuffer cmd_buf) override;

    DECL_MOVE(Label)
    BAN_COPY(Label)

private:
    Root::TextureReference m_text_texture;
    const char * m_text;
};

/** A gui menu */
class Menu : public Element
{
public:
    /** Default constructor
     * \param root The gui root
     * \param box The menu's box
     * \param width The menu's width
     * \param row_height The menu rows' height
     */
    Menu(Root * root, const Rect & box, uint32_t width, uint32_t row_height);

    virtual ~Menu() override;

    virtual void cleanup() override;

    virtual VkResult first_initialization() override
    {
        return VK_SUCCESS;
    }
    virtual VkDeviceSize get_buffer_size() const override
    {
        return sizeof(BasicRectangle) + 5 * sizeof(uint32_t);
    }
    virtual VkResult second_initialization(Vulkan::Buffer & host_buffer, VkCommandBuffer cmd_buf) override;

    BAN_COPY(Menu)

    virtual void update(VkCommandBuffer cmd_buf) override;

    virtual void render(VkCommandBuffer cmd_buf) const override;

    /** \brief Pack menu
     * Set the menu's elements position accordingly to the menu's position
     */
    void pack();

    /** Add an element
     * Add pointer to element
     * Menu does not own the element
     * \param elem The element to add
     */
    void add_element(Element * elem);

    virtual const Element * get_active_element() const override;

    virtual void on_click() const override;

private:
    float m_width;
    float m_row_height;
    Custom::Vector<Gui::Element*> m_elements;
};

/** A gui button */
class Button : public Element
{
public:
    /** Default constructor
     * \param root The gui root
     * \param icon_path The icon's path
     * \param text The button's text
     * \param box The button's box
     */
    Button(Root * root, const char * icon_path, const char * text, const Rect & box = null_rect);

    virtual ~Button() override
    {
        cleanup();
    }

    virtual VkDeviceSize get_buffer_size() const override
    {
        return sizeof(TexturedRectangle) * 2;
    }

    virtual VkResult first_initialization() override;

    virtual VkResult second_initialization(Vulkan::Buffer & host_buffer, VkCommandBuffer cmd_buf) override;

    virtual void update(VkCommandBuffer cmd_buf) override;

    virtual void render(VkCommandBuffer cmd_buf) const override;

    DECL_MOVE(Button)
    BAN_COPY(Button)

    virtual const Element * get_active_element() const override
    {
        return this;
    }

private:
    const char * m_icon_path;
    const char * m_text;
    Root::TextureReference m_icon;
    Root::TextureReference m_text_texture;
};

/** Quit button */
class QuitButton : public Button
{
public:
    using Button::Button;

    virtual void on_click() const override;
};

class PopButton : public Button
{
public:
    using Button::Button;

    virtual void on_click() const override;
};

class FullscreenToggleButton : public Button
{
public:
    using Button::Button;

    virtual void on_click() const override;
};

class PushButton : public Button
{
public:

    /** Default constructor
     * \param root The gui root
     * \param icon_path The button's icon's path
     * \param text The button's text
     * \param pushed The element to push
     * \param box The element's box
     */
    PushButton(Root * root, const char * icon_path, const char * text, Element * pushed, const Rect & box = null_rect);

    BAN_COPY(PushButton)
    DECL_MOVE(PushButton)

    virtual void on_click() const override;
private:

    Element * m_pushed;
};

class ResolutionButton : public Element
{
public:
    /** Default constructor
     * \param root The gui root
     * \param resolution The button's resolution
     * \param box The button's box
     */
    ResolutionButton(Root * root, VkExtent2D resolution, const Rect & box = null_rect);

    virtual VkResult first_initialization() override;
    virtual VkResult second_initialization(Vulkan::Buffer & host_buffer, VkCommandBuffer cmd_buf);

    virtual void update(VkCommandBuffer cmd_buf);

    virtual VkDeviceSize get_buffer_size() const {return sizeof(TexturedRectangle);}

    virtual void render(VkCommandBuffer cmd_buf) const;

    virtual const Element * get_active_element() const override {return this;}

    virtual void on_click() const override;

    BAN_COPY(ResolutionButton)
    DECL_MOVE(ResolutionButton)

private:
    VkExtent2D m_resolution;
    Root::TextureReference m_texture;
};

}

#endif // GUI_H_INCLUDED
