#ifndef VULKANBASE_H
#define VULKANBASE_H

#include "version.h"
#include "warning.h"
#include "vulkan_macro.h"
#include "custom.hpp"
#include "moving_macro.h"

DISABLE_WARNINGS

#include <vulkan/vulkan.h>

#include <SDL_vulkan.h>
#include <SDL.h>

ENABLE_WARNINGS

#include <array>
#include <cstdlib>
#include <fstream>

class VulkanBase {
public:

    DISABLE_WARNINGS

    static constexpr uint32_t api_version = VK_API_VERSION_1_2; /**< Version of the api */

    ENABLE_WARNINGS

#ifdef DEBUG
    static constexpr std::array<const char *, 1> layers
    {
        "VK_LAYER_KHRONOS_validation"
    };
#else
    static constexpr std::array<const char *, 0> layers {}; /**< Enabled Vulkan layers */

#endif // DEBUG

#ifdef DEBUG
    static constexpr std::array<const char *,1> instance_extensions
    {
        "VK_EXT_debug_utils"
    };

#else

    static constexpr std::array<const char *, 0> instance_extensions{}; /**< Enabled Vulkan instance extensions */
#endif // DEBUG

    static constexpr std::array<const char *, 1> device_extensions =
    {
        "VK_KHR_swapchain"
    }; /**< Enabled Vulkan device extensions */



    static constexpr uint32_t no_family = uint32_t(~0); /**< A queue family index representing an absent queue family */
    static constexpr uint32_t no_type = uint32_t(~0); /**< A memory type index representing an absent memory type */

    static constexpr const char * cache_location = "pipeline_cache";

    /** Default constructor */
    VulkanBase();
    /** Default destructor */
    ~VulkanBase();

    /** Cleanup ressources */
    void cleanup();

    /** Initialize
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult init();

    /** Create instance
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_instance();

    /** Get instance
     * \return The instance
     */
    VkInstance get_instance() const
    {
        return m_instance;
    }

    /** Move constructor
    * \param from The object to move
    */
    VulkanBase(VulkanBase && from);

    /** Move assignement operator
    * \param rhs The object to move
    * \return Itself after assignement
    */
    VulkanBase & operator=(VulkanBase && rhs);

    VulkanBase(const VulkanBase& from) = delete;
    VulkanBase & operator=(const VulkanBase & rhs) = delete;

    /** Choose the physical device
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult choose_physical_device();

    /** Get physical device
     * \return The physical device
     */
    VkPhysicalDevice get_physical_device() const
    {
        return m_physical_device;
    }

    /** Get device
     * \return The device
     */
    VkDevice get_device() const
    {
        return m_device;
    }

    /** Get graphics queue
     * \return The graphics queue
     */
    VkQueue get_graphics_queue() const
    {
        return m_graphics_queue;
    }

    /** Get present queue
     * \return The present queue
     */
    VkQueue get_present_queue() const
    {
        return m_present_queue;
    }

    /** Check if physical device is suitable
     * \param physical_device The device to check
     * \param suitable A bool receiving true if suitable, false if not
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult physical_device_suitable(VkPhysicalDevice physical_device, bool & suitable) const;

    /** Get physical device queue family indexes
     * \param physical_device The physical device to get the indexes from
     * \param graphic A uint32_t receiving the graphic queue family index or no_family if no graphic queues are present
     * \param present A uint32_t receiving the present queue family index or no_family if no graphic queues are present
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult get_physical_device_indexes(VkPhysicalDevice physical_device, uint32_t & graphic, uint32_t & present) const;

    /** Get physical device queue family indexes
     * \param graphic A uint32_t receiving the graphic queue family index or no_family if no graphic queues are present
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult get_graphic_index(uint32_t & graphic) const;

    /** Create device
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_device();

    /** Set the window using the ressources
     * \param window The window
     */
    void set_window(SDL_Window * window)
    {
        m_window = window;
    }

    /** Create Vulkan surface
     * \return true on success, false otherwise
     */
    bool create_surface();

    /** Get surface
     * \return The surface
     */
    VkSurfaceKHR get_surface() const
    {
        return m_surface;
    }

    /** Get render pass
     * \return The render pass
     */
    VkRenderPass get_render_pass() const
    {
        return m_render_pass;
    }

    /**
     * \brief Create swapchain
     * Creates or recreate swapchain and images views.
     * Previous swapchain is used for creating the new one and destroyed.
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_swapchain();

    /** Choose surface format
     * \param format A VkSurfaceFormatKHR receiving the chosen surface format
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult choose_surface_format(VkSurfaceFormatKHR & format);

    /** Choose present mode
     * \param present_mode A VkPresentModeKHR receiving the chosen present mode
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult choose_present_mode(VkPresentModeKHR & present_mode);

    /** Find memory type matching requirements
     * \param requirements The VkMemoryRequirements describing the memory requirements
     * \param mem_props The memory properties of the type to choose
     * \return The chosen memory type's index, or no_type if not found
     */
    uint32_t choose_memory_type(const VkMemoryRequirements & requirements, VkMemoryPropertyFlags mem_props);

    /** Recreate swapchain and its ressources
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult recreate_swapchain();

    /** Cleanup swapchain dependent ressources */
    void cleanup_swapchain_ressources();

    /** Create framebuffers
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_framebuffers();

    /** Create render pass
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_render_pass();

#ifdef DEBUG

    /** Create debug utils messenger
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_debug_utils_messenger();

    /** Debug utils messenger callback
     * \param messageSeverity The message severity
     * \param messageTypes The message type
     * \param pCallbackData A pointer to a structure containing callback related information
     * \param pUserData The provided user data
     * \return Always VK_FALSE
     */
    static VkBool32 VKAPI_PTR debug_utils_messenger_callback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageTypes,
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
            void* pUserData);

    /** Populate a VkDebugUtilsMessengerCreateInfoEXT structure
     * \param info The structure to populate
     */
    void populate_debug_utils_messenger_info(VkDebugUtilsMessengerCreateInfoEXT & info);

#endif

    /** Get the pipeline cache
     * \return The pipeline cache
     */
    VkPipelineCache get_pipeline_cache() const
    {
        return m_pipeline_cache;
    }

    /** Create the pipeline cache
     * \return VK_SUCCESS on success, a Vulkan error code on failure
     */
    VkResult create_pipeline_cache();

    /** Save pipeline cache */
    void save_pipeline_cache() const;

    /** Get the swapchain's extent
     * \return The swapchain's extent
     */
    const VkExtent2D & get_swapchain_extent() const
    {
        return m_swapchain_extent;
    }

    /** Get swapchain
     * \return The swapchain
     */
    VkSwapchainKHR get_swapchain() const
    {
        return m_swapchain;
    }

    /** Get framebuffers
     * \return The framebuffers
     */
    const Custom::Vector<VkFramebuffer> & get_framebuffers() const
    {
        return m_framebuffers;
    }

    /** Set viewport and scissor dynamic state in given command buffer
     * \param cmd_buf The command buffer
     */
    void set_viewport_scissor(VkCommandBuffer cmd_buf) const;

    /** Get aspect ratio
     * \return The aspect ratio
     */
    float get_aspect_ratio() const {return m_aspect_ratio;}

protected:

private:
    SDL_Window * m_window;

    VkInstance m_instance;

#ifdef DEBUG
    VkDebugUtilsMessengerEXT m_debug_utils_messenger;
#endif // DEBUG

    VkPhysicalDevice m_physical_device;

    VkSurfaceKHR m_surface;

    VkDevice m_device;
    VkQueue m_graphics_queue;
    VkQueue m_present_queue;

    VkSwapchainKHR m_swapchain;
    VkExtent2D m_swapchain_extent;

    Custom::Vector<VkImageView> m_swapchain_image_views;
    Custom::Vector<VkFramebuffer> m_framebuffers;

    VkRenderPass m_render_pass;

    VkPipelineCache m_pipeline_cache;

    VkFormat m_swapchain_format;

    float m_aspect_ratio;
};

#endif // VULKANBASE_H
