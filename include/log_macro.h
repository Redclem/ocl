#ifndef LOG_MACRO_H_INCLUDED
#define LOG_MACRO_H_INCLUDED

#include <iostream>
#include <ctime>

#ifdef DEBUG

namespace Log
{
    template<typename Type>
    void log(Type arg)
    {
        std::cout << arg << '\n';
    }

    template<typename First, typename ... Args>
    void log(First first, Args  ... args)
    {
        std::cout << first << ' ';
        log(args...);
    }
}

#define log_message(...) Log::log("Log message at line", __LINE__, "in file", __FILE__, "in function", __func__ ,"at time", time(nullptr), ":\n", __VA_ARGS__)

#else
#define log_message(...)
#endif // DEBUG

#endif // LOG_MACRO_H_INCLUDED
