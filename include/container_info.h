#ifndef CONTAINER_MESH_INCLUDED
#define CONTAINER_MESH_INCLUDED

#include "copy_move_macros.h"
#include "custom.hpp"
#include "vertex.h"
#include "log_macro.h"
#include "measure_type.h"
#include "vec.h"

#include <iostream>
#include <string>
#include <memory>
#include <stdexcept>
#include <map>

/** A class containing info about a container */
class ContainerInfo
{
public:
    /** Default constructor */
    ContainerInfo();

    /** Load info from stream
     * \param stream The stream to read from
     * \return true on successful load, false on failure
     */
    bool load(std::istream & stream);

    /** Destructor */
    ~ContainerInfo();

    typedef Vertex::Basic vertex_t; /**< Member type vertex_t : The type of the container vertices */
    typedef uint16_t index_t; /**< Member type index_t : The type of the container indexes */

    static constexpr index_t primitive_restart = ~index_t(0); /** Constant expression primitive_restart : Index value restarting the primitive assembly */

    DECL_MOVE(ContainerInfo)
    DECL_COPY(ContainerInfo)

    /** A volume contained in the container */
    class VolumeInfo
    {
    public:
        /** Default constructor
         * \param height The volume's height
         */
        VolumeInfo(height_t height) : m_height(height) {}

        /** Empty destructor */
        virtual ~VolumeInfo() {}

        /** Get maximal capacity
         * \return The maximal capacity (in milliliters)
         */
        virtual volume_t get_capacity() const = 0;

        /** Get fluid level with given volume
         * \param volume The volume in milliliters
         * \return The fluid level in centimeters
         */
        virtual height_t get_fluid_level(volume_t volume) const = 0;

        /** Allocate clone of this object
         * \return A pointer to a clone of this object
         */
        virtual VolumeInfo * clone() const = 0;

        /** Get volume height
         * \return The volume height in centimeters
         */
        height_t get_height() const {return m_height;}

        /** Write volume to stream
         * \param stream The stream to write the volume to
         */
        virtual void display(std::ostream & stream) const = 0;

    private:

        height_t m_height; /**< Member variable m_height : Volume height in centimeters */
    };

    /** A volume w/ constant section (amount of liquid linear to the level of the liquid, e.g. cylinder or square) */
    class Linear : public VolumeInfo
    {
    public:

        /** Default constructor
         * \param height The volume's height
         * \param section The volume's section
         */
        Linear(height_t height, surface_t section) : VolumeInfo::VolumeInfo(height), m_section(section) {}

        virtual volume_t get_capacity() const {return m_section * get_height();}

        virtual height_t get_fluid_level(volume_t volume) const {return volume / m_section;}

        virtual VolumeInfo * clone() const {return new Linear(get_height(), m_section);}

        virtual void display(std::ostream & stream) const {stream << get_height() << " linear " << m_section;}

    private:
        surface_t m_section; /** The section of the volume, in square centimeters */
    };

    /** Object holding a pointer to a VolumeInfo */
    class VolumePtr
    {
    public:
        /** Empty constructor */
        VolumePtr() : m_ptr(nullptr) {}

        /** Construct from pointer
         * \param ptr The pointer to construct from
         */
        VolumePtr(VolumeInfo * && ptr) : m_ptr(ptr) {}

        /** Construct from pointer
         * \param ptr The pointer to construct from
         */
        VolumePtr(VolumeInfo const * & ptr) : m_ptr(ptr->clone()) {}

        /** Copy constructor
         * \param from The object to copy
         */
        VolumePtr(const VolumePtr & from) : m_ptr(from.m_ptr->clone()) {}

        /** Move constructor
         * \param from The object to move
         */
        VolumePtr(VolumePtr && from) : INIT_EXCHANGE(m_ptr, nullptr) {}

        /** Copy assignement operator
         * \param rhs The object to copy
         * \return Itself after assignement
         */
        VolumePtr & operator=(const VolumePtr & rhs)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = (rhs.m_ptr ? rhs.m_ptr->clone() : nullptr);
            return *this;
        }

        /** Move assignement operator
         * \param rhs The object to move
         * \return Itself after assignement
         */
        VolumePtr & operator=(VolumePtr && rhs)
        {
            ASSIGN_SWAP(m_ptr);
            return *this;
        }

        /** Pointer assignement operator
         * \param pointer The pointer to assign from
         * \return Itself after assignement
         */
        VolumePtr & operator=(VolumeInfo const * & rhs)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = rhs->clone();
            return *this;
        }

        /** Pointer assignement operator
         * \param pointer The pointer to assign from
         * \return Itself after assignement
         */
        VolumePtr & operator=(VolumeInfo * && rhs)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = rhs;
            return *this;
        }

        /** Destroy owned volume */
        void destroy()
        {
            if(m_ptr)
            {
                delete m_ptr;
                m_ptr = nullptr;
            }
        }

        /** Destructor */
        ~VolumePtr()
        {
            if(m_ptr) delete m_ptr;
        }

        /** Member of pointer operator
         * \return A pointer to the owned volume
         */
        VolumeInfo * operator->()
        {
            return m_ptr;
        }

        /** Member of pointer operator (const)
         * \return A const pointer to the owned volume
         */
        const VolumeInfo * operator->() const
        {
            return m_ptr;
        }

        /** Create volume object
         * \tparam Volume The class of the volume to create
         * \tparam Args The type of the args used to construct the object
         * \param args The args used to construct the object
         */
        template<class Volume, typename ... Args>
        void create(Args ... args)
        {
            if(m_ptr) delete m_ptr;
            m_ptr = new Volume(args...);
        }

        /** Indirection operator
         * \return A reference to the owned volume
         */
        VolumeInfo & operator*()
        {
            return *m_ptr;
        }

        /** Indirection operator
         * \return A const reference to the owned volume
         */
        const VolumeInfo & operator*() const
        {
            return *m_ptr;
        }

        /** Get pointer to owned volume
         * \return A pointer to the owned volume
         */
        VolumeInfo * get_ptr()
        {
            return m_ptr;
        }

        /** Get const pointer to owned volume
         * \return A const pointer to the owned volume
         */
        const VolumeInfo * get_ptr() const
        {
            return m_ptr;
        }

        /** Cast to VolumeInfo *
         * \return A pointer to the owned volume
         */
        operator VolumeInfo *()
        {
            return m_ptr;
        }

        /** Cast to const VolumeInfo *
         * \return A const pointer to the owned volume
         */
        operator const VolumeInfo *() const
        {
            return m_ptr;
        }

    private:
        VolumeInfo * m_ptr;
    };

    /** Clear container information */
    void clear();

    /** Write contents to stream
     * \param stream The stream to write to
     */
    void display(std::ostream & stream) const;

    /** Get capacity
     * \return The capacity of the container in milliliters
     */
    volume_t get_capacity() const;

    /** Get outside indexes
     * \return A const reference to the vector containing the outside indexes
     */
    const Custom::Vector<index_t> & get_outside_indexes() const {return m_outside_indexes;}

    /** Get inside indexes
     * \return A const reference to the vector containing the inside indexes
     */
    const Custom::Vector<index_t> & get_inside_indexes() const {return m_inside_indexes;}

    /** Get vertices
     * \return A const reference to the vector containing the vertices
     */
    const Custom::Vector<vertex_t> & get_vertices() const {return m_vertices;}

    /** Compute y coordinate from fluid level
     * \param level The fluid's level in centimeters
     * \return The y coordinate of the fluid
     */
    height_t get_y_coord(height_t level) const {return (level / m_height) * (m_top - m_bottom) + m_bottom;}

    /** Get the level of the fluid in the container
     * \param volume The volume of fluid in the container in milliliters
     * \return The level of the fluid in the container in centimeters
     */
    height_t get_fluid_level(volume_t volume) const;

    /** Get bottom
     * \return The y coordinate of the bottom of the container
     */
    float get_bottom() const {return m_bottom;}

    /** Get maximums
     * \return A const reference to the maximums
     */
    const Vec::Vec & get_maximums() const {return m_maximums;}

    /** Get minimums
     * \return A const reference to the minimums
     */
    const Vec::Vec & get_minimums() const {return m_minimums;}

    /** Get center
     * \return The coordinates of the center
     */
    Vec::Vec get_center() const {return {(m_maximums.x + m_minimums.x) * 0.5f, (m_maximums.y + m_minimums.y) * 0.5f};}

private:


    /** Compute the y coordinate of the container's bottom and top */
    void compute_extremums();

    /** Automatically generate inside vertices and indexes
     * outside vertices must be in counter-clockwise order for this to work correctly
     * \param outline A vector containing the indexes of the outline
     * \param width the width of the container wall (outside - inside distance)
     */
    void autogen_inside(const Custom::Vector<index_t> & outline, float width);

    Custom::Vector<vertex_t> m_vertices; /**< Member variable m_vertices : The vertices of the container */
    Custom::Vector<index_t> m_outside_indexes; /**< Member variable m_indexes : The indexes making up the outside of the container */
    Custom::Vector<index_t> m_inside_indexes; /**< Member variable m_indexes : The indexes making up the inside of the container */

    Custom::Vector<VolumePtr> m_volumes; /**< Member variable m_volumes : The volumes making up the container */

    height_t m_height; /**< Member variable m_height : The height of the inside of the container in centimeters */
    float m_bottom; /**< Member variable m_bottom : The y coordinate of the container's inner side bottom */
    float m_top; /**< Member variable m_bottom : The y coordinate of the container's inner side top */

    Vec::Vec m_maximums;
    Vec::Vec m_minimums;
};

#endif // CONTAINER_MESH_INCLUDED
