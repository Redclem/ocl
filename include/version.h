#ifndef VERSION_H_INCLUDED
#define VERSION_H_INCLUDED

#include <cstdint>

#include <vulkan/vulkan.h>

namespace Version
{
    constexpr uint32_t major = 0; /**< Version minor */
    constexpr uint32_t minor = 0; /**< Version major */
    constexpr uint32_t patch = 0; /**< Version patch */

    constexpr uint32_t version = VK_MAKE_VERSION(major, minor, patch); /**< Vulkan compatible version number */
}

#endif // VERSION_H_INCLUDED
