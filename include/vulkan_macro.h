#ifndef VULKAN_MACRO_H_INCLUDED
#define VULKAN_MACRO_H_INCLUDED

#include "log_macro.h"

#define CHECK_VULKAN(operation) {\
    VkResult vulkan_operation_result(operation);\
    if(vulkan_operation_result != VK_SUCCESS)\
    {\
        log_message("Error " #operation " :", vulkan_operation_result);\
        return vulkan_operation_result;\
    }\
}

#define CHECK_VULKAN_BOOL(operation) {\
    VkResult vulkan_operation_result(operation);\
    if(vulkan_operation_result != VK_SUCCESS)\
    {\
        log_message("Error " #operation " :", vulkan_operation_result);\
        return false;\
    }\
}

#define CHECK_VULKAN_NO_RETURN(operation) {\
    VkResult vulkan_operation_result(operation);\
    if(vulkan_operation_result != VK_SUCCESS)\
    {\
        log_message("Error " #operation " :", vulkan_operation_result);\
        return;\
    }\
}

#define INIT_VK_DEF(member) member(VK_NULL_HANDLE)//reinterpret_cast<decltype(member)>(VK_NULL_HANDLE))
#define INIT_VK_EXCH(member) member(std::exchange<decltype(member), decltype(member)>(from.member, VK_NULL_HANDLE))//reinterpret_cast<decltype(member)>(VK_NULL_HANDLE)))

#define ASSIGN_VK_DEF(member) member = VK_NULL_HANDLE//reinterpret_cast<decltype(member)>(VK_NULL_HANDLE)

#define DEF_VK(type) VK_NULL_HANDLE//reinterpret_cast<type>(VK_NULL_HANDLE)

#endif // VULKAN_MACRO_H_INCLUDED
