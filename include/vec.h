#ifndef VEC_H_INCLUDED
#define VEC_H_INCLUDED

#include <cmath>

namespace Vec
{
struct Vec
{
    float x;
    float y;

    /** Null constructor */
    Vec() : x(0.0f), y(0.0f) {}

    /** Coordinate constructor
     * \param _x The x coordinate
     * \param _y The y coordinate
     */
    Vec(float _x, float _y) : x(_x), y(_y) {}

    /** Addition assignement opeator
     * \param rhs The vector to add to this
     * \return Itself after assignement
     */
    Vec & operator+=(const Vec & rhs)
    {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    /** Multiplication assignement operator
     * \param rhs The float to multiply with
     * \return Itself after multiplying
     */
    Vec & operator*=(float rhs)
    {
        x *= rhs;
        y *= rhs;
        return *this;
    }

    /** Normalize this vector
     * \return Itself after assignement
     */
    Vec & normalize()
    {
        float norm = sqrt(x * x + y * y);
        x /= norm;
        y /= norm;
        return *this;
    }

    /** Get norm
     * \return The vector's norm
     */
    float norm()
    {
        return sqrt(x * x + y * y);
    }

    /** Dot product
     * \param b The second vector
     * \return The dot product of this and b
     */
    float dot(const Vec & b)
    {
        return x * b.x + y * b.y;
    }

    /** Addition operator
     * \param b The second vector
     * \return The sum of this and b
     */
    Vec operator+(const Vec & b)
    {
        return Vec(x + b.x, y + b.y);
    }

    /** Scalar division operator
     * \param scalar The scalar to divide by
     * \return The quotient of this and b
     */
    Vec operator/(float scalar)
    {
        return Vec(x / scalar, y / scalar);
    }
};
}

#endif // VEC_H_INCLUDED
