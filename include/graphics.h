#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include "warning.h"
#include "error_macro.h"
#include "moving_macro.h"
#include "copy_move_macros.h"
#include "vulkan_macro.h"

#include "custom.hpp"
#include "VulkanBase.h"
#include "VulkanClasses.h"

DISABLE_WARNINGS

#include <SDL.h>

#include <vulkan/vulkan.h>

#include <stb_image.h>
#include <stb_truetype.h>

ENABLE_WARNINGS

#include <stdexcept>
#include <map>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <memory>

namespace Graphics
{

/** Is a point in a rectangle
 * \param point The point
 * \param rect The rectangle
 * \return true if point in rectangle, false otherwise
 */
bool point_in_rect(const SDL_Point & point, const SDL_Rect & rect);


class Window
{
public:

    /** Default constructor */
    Window();

    /** Create window (and renderer)
     * \param name The window's name in UTF-8
     * \param width The window's width
     * \param height The window's height
     * \return 0 on success, -1 on failure
     */
    int create_window(const char * name, int width, int height);

    /** Wait until window receives quit signal */
    void wait_quit();

    /** Default destructor */
    ~Window();

    /** Get handle to window
     * \return A handle to the window
     */
    SDL_Window * get_window() const
    {
        return m_window;
    }

    /** Set icon
     * \param surf The surface to use as an icon
     */
    void set_icon(SDL_Surface * surf)
    {
        SDL_SetWindowIcon(m_window, surf);
    }

    /** Get window size
     * \param w A reference to the variable receiving the width
     * \param h A reference to the variable receiving the height
     */
    void get_window_size(int & w, int & h)
    {
        SDL_GetWindowSize(m_window, &w, &h);
    }

    /** Toggle fullscreen
     * \return 0 on success, a negative error code on failure
     */
    int toggle_fullscreen();

    /** Set resolution
     * \param w The new width
     * \param h The new height
     * \return 0 on success, a negative error code on failure
     */
    int set_resolution(int w, int h);

    DECL_MOVE(Window)
    DECL_COPY(Window)

private:
    SDL_Window * m_window;
};

class Surface
{
public:
    /** Default constructor */
    Surface();

    /** Default destructor */
    ~Surface();

    /** Get handle to surface
     * \return A handle to the surface
     */
    const SDL_Surface * get_surface() const
    {
        return m_surface;
    }

    /** Get handle to surface
     * \return A handle to the surface
     */
    SDL_Surface * get_surface()
    {
        return m_surface;
    }

    /** Create empty surface
     * \param w The surface width
     * \param h The surface height
     * \param depth The surface depth
     * \param format The surface format
     * \return 0 on success, -1 on failure
     */
    int create(int w, int h, int depth, Uint32 format);

    /** Create from file
     * \param path The file to load from
     * \return 0 on success, -1 on failure
     */
    int create(const char * path);

    /** Create from text
     * \param text The input text
     * \param font A pointer to the font info
     * \param scale The font scale
     * \param color The text color (alpha ignored)
     * \return 0 on success, a negative error code on failure
     */
    int create(const char * text, const stbtt_fontinfo * font, float scale, SDL_Color color);

    /** Get the size of a surface containing given text
     * \param text The text
     * \param font The font
     * \param scale The font's scale
     * \param w An int receiving the text width
     * \param h An int receiving the text height
     */
    static void measure(const char * text, const stbtt_fontinfo * font, float scale, int & w, int & h);

    /** Is the surface valid
     * \return true if valid, false otherwise
     */
    bool valid() const
    {
        return m_surface;
    }

    /** Move constructor
    * \param from The object to move
    */
    Surface(Surface && from);

    /** Move assignement operator
    * \param rhs The object to move
    * \return Itself after assignement
    */
    Surface & operator=(Surface && rhs);

    Surface(const Surface& from) = delete;
    Surface & operator=(const Surface & rhs) = delete;


private:
    SDL_Surface * m_surface;
};

}

#endif // GRAPHICS_H_INCLUDED
