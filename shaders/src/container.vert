#version 450

layout(location = 0) in vec2 i_pos;

layout(push_constant) uniform PushConstant
{
	layout(offset=16)
	vec2 pos;	
	float ar;
} push_constant;

void main()
{
	gl_Position = vec4(i_pos + push_constant.pos, 0.0, 1.0);
	gl_Position.x /= push_constant.ar;
}
