
"""
GLSL to SPIR-V compiler script

Author : Redclem, https://gitlab.com/Redclem

Sources are taken from the execution directory, binairies go to the parent directory
Store source files hashes in specified file to skip unchanged files
"""


from os import listdir, system
from os.path import exists
import hashlib

# Target vulkan environment
target_env = "vulkan1.0"

# Source files extensions
sources_extensions = (".vert", ".frag")

# Hash storage file
hash_path = "hashes"

shaders = [name for name in listdir() if name.endswith(sources_extensions)]

hashes = {}

if exists(hash_path):
    with open(hash_path, "r") as hash_file:
        hash_dat = hash_file.read()

    for line in hash_dat.split("\n"):
        if line == "":
            continue
        name_and_hash = line.split(":")
        hashes[name_and_hash[0]] = name_and_hash[1]

for shader in shaders:
    name_parts = shader.split(".")
    shader_dst_exec = ("../{}_{}.spv".format(name_parts[0], name_parts[1]))

    hasher = hashlib.sha1()
    with open(shader, "rb") as shader_f:
        hasher.update(shader_f.read())

    if shader in hashes.keys():
        if hashes[shader] == hasher.hexdigest():
            print("Skipping {}".format(shader))
            continue

    hashes[shader] = hasher.hexdigest()
    
    system("glslangValidator {} -o {} --target-env {}".format(shader, shader_dst_exec, target_env))

with open(hash_path, "w") as hash_file:
    for key, value in hashes.items():
        hash_file.write("{}:{}\n".format(key, value))

input("Done. Press any key...")
