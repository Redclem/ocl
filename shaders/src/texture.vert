#version 450

layout(location = 0) in vec2 i_position;
layout(location = 1) in vec2 i_texture_coord;

layout(location = 0) out vec2 o_texture_coord;

void main()
{
    o_texture_coord = i_texture_coord;
    gl_Position = vec4(i_position, 0.0, 1.0);
}
