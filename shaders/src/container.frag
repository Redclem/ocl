#version 450

layout(push_constant) uniform PushConstant{
	vec4 color;
} push_constant;

layout(location = 0) out vec4 o_color;

void main()
{	
	o_color = push_constant.color;
}
